" enable filetype detection:
filetype on
filetype plugin on
filetype indent on " file type based indentation

syntax on
" Support True Colors in terminal
:set termguicolors
" iceberg PaperColor monokai gruvbox
colorscheme monokai
set background=dark
" set term=xterm-256
set nocompatible " not compatible with vi
"set nocp
"set t_Co=256
"set t_Sb=m
"set t_Sf=m

set encoding=utf-8
set number
set colorcolumn=80

"for Cygwin remove \n\r under windows
set fileformat=unix
set fileformats=unix,dos
"set nobinary

set tabstop=2
set shiftwidth=2
set et
set mouse=a
set smarttab
set smartindent
set nowrap
"//procedure foo(integer, boolean){{{ ...}}}
set foldmethod=marker

set splitbelow splitright


" Navigate with <Ctrl>-hjkl in Insert mode
inoremap <C-h> <C-o>h
inoremap <C-j> <C-o>j
inoremap <C-k> <C-o>k
inoremap <C-l> <C-o>l
" , + m - copy the full path of current opened file to clipboard
nnoremap <leader>m :let @+=expand("%:p")<CR> :let @*=expand("%:p")<CR>:echo @*<CR>
nnoremap gd :YcmCompleter GoToDefinition<CR>

" use: call FormatJson() Dont Work!
function! FormatJson()
python3 << EOF
import vim
import json
try:
    buf = vim.current.buffer
    json_content = '\n'.join(buf[:])
    content = json.loads(json_content)
    sorted_content = json.dumps(content, indent=4, sort_keys=True)
    buf[:] = sorted_content.split('\n')
except Exception, e:
    print e
EOF
endfunction

" Select range and: '<,'>FormatJson
" need installed nodejs xargs
"command! -range FormatJson <line1>,<line2>!xargs -0 -I {} node -e 'console.log(JSON.stringify({}, null, 2));'


"manual loading
"set rtp+=~/.vim/plugged/youcompleteme

" ==== PLUGINS ====
" config for vim-plug
call plug#begin()

Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'easymotion/vim-easymotion'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'valloric/youcompleteme'
Plug 'RRethy/vim-hexokinase', { 'do': 'make hexokinase' }
"Show Colors of hex rgb ect
"Plug 'BourgeoisBear/clrzr'
call plug#end()

"let g:ycm_keep_logfile = 1
"let g:ycm_log_level = 'debug'
"
"filetype plugin indent on

" -->> Hexokinase:Config Colors Previewer
" https://github.com/RRethy/vim-hexokinase
" for hexokinase
" Method of highlighters for hexoklinase
"let g:Hexokinase_highlighters = [ 'foreground' ]
let g:Hexokinase_highlighters = [ 'backgroundfull' ]

"let g:Hexokinase_highlighters = [ 'sign_column' ]
let g:Hexokinase_refreshEvents = [ 'InsertLeave' ]
" Patterns to match for all filetypes
" Can be a comma separated string or a list of strings
" Default value:
"let g:Hexokinase_optInPatterns = 'full_hex,rgb,rgba,hsl,hsla,colour_names'

" Choose which filetypes to scrape automatically (by default ALL filetypes are scraped):
" Sample value, to keep default behaviour don't define this variable
let g:Hexokinase_ftEnabled = ['css', 'html', 'javascript']
" HexokinaseTurnOff

" --<< Hexokinase


" set a map leader for more key combos
" pe/vim-fugitive'u
let g:mapleader = ','

"mappings
"File manager Open FileManager via Ctrl-N
map <C-n> :NERDTreeToggle<CR>
"map <C-i> :!~/tools/mpv-play<CR><CR>


" Fuzzy search
"map <C-p> :Telescope find_files<CR>

" Read-only rtf through unrtf
"autocmd BufReadPre *.rtf silent set ro
"autocmd BufReadPost *.rtf silent %!unrtf --text

"use nasm syntax insted default asm
autocmd BufNew,BufRead *.asm set ft=nasm

" for js/coffee/jade files, 4 spaces
"autocmd Filetype javascript setlocal ts=4 sw=4 sts=0 expandtab
"autocmd Filetype java setlocal ts=4 sw=4 sts=0 expandtab
autocmd BufRead,BufNewFile   *.c,*.h,*.java set ts=4 sw=4 sts=0 expandtab "noic cin noexpandtab

"augroup filetype_c
"    autocmd!
"    :autocmd FileType c setlocal tabstop=4 shiftwidth=4 expandtab "softtabstop=4
"    :autocmd FileType c nnoremap <buffer> <localleader>c I/*<space><esc><s-a><space>*/<esc>
"augroup end

highlight ExtraWhitespace ctermbg=red guibg=#5f0000
match ExtraWhitespace /\s\+$/
au BufWinEnter * match ExtraWhitespace /\s\+$/
au InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
au InsertLeave * match ExtraWhitespace /\s\+$/
au BufWinLeave * call clearmatches()

" enable filetype detection:
filetype on
filetype plugin on
filetype indent on " file type based indentation

"function! hiToFile()
"  redir! > /home/swarg/vim.output
"  hi
"  redir END
"endfunction
"
" Show syntax highlighting groups for word under cursor
"echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
nmap <leader>z :call <SID>SynStack()<CR>
function! <SID>SynStack()
  if !exists("*synstack")
    return
  endif
  let s0 = map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
  let l:s = synID(line('.'), col('.'), 1)
  echo s0 synIDattr(l:s, 'name') . ' -> ' . synIDattr(synIDtrans(l:s), 'name')
endfunc


