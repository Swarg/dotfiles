Dotfiles
========

Files in .dotfiles are user-specific configuration for applications.
This includes configurations for awesomewm, neovim, terminal and gtk theme,
and settings for a lot of other applications.
Symlinks are placed in the home directory pointing to the files inside this
repository, so it's easy to track any changes using git.


Installation
============
```bash
git clone https://gitlab.com/Swarg/dotfiles.git
cd dotfiles
# Pick by one like:
stow nvim
# TODO one cmd for all
```

BashRC and Vi/VimRC for root(Manual installation):

```bash
cd dotfiles/root
sudo cp .bashrc /root/.bashrc

# vi/vim
sudo cp .vimrc /root/.vimrc && sudo mkdir -p /root/.vim/ && \
  sudo cp -r ../vim/.vim/colors/ /root/.vim/colors
```

Stow
====

The folder structure for the configurations is setup so it can be used with
`GNU Stow <https://www.gnu.org/software/stow/>`_. For example::
```bash
   cd ~/.dotfiles/
   stow nvim
```

To symlink all files within nvim to your home folder.

# Content

- alacritty         -- fast terminal(experimental)
- awesome           -- current used tile window manager
- bash              -- aliases
- darkai            -- gtk color theme (tweaked Adwaita/xfce4)
- dmenu-for-search  -- search-sources: yt-channels, sites for dmenu-scripts
- dmenu-scripts     -- called by shortcut configured in awesome
- i3                -- experimental, not finished
- lightdm           -- gtk-greeter for login
- vim               -- experimental, not finished (has nice monokai theme)
- nvim              -- currently used
- vifm              -- filemanager with vim-like control and features
- x                 -- .Xresources(xterm cfg, X appereance: nice font, dpi)
- xfce4             -- terminal and xfcong:keydoard-shortcuts, xsettings
- zathura           -- pdf reader with nice darkmode
- java-swing        -- attempt to create common used dark theme for java-swing apps
- tools             -- cli helpers
- mpv               -- a media player


# Install Hack Nerd Font

https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/Hack

copy this files::

Hack Bold Italic Nerd Font Complete.ttf
Hack Bold Nerd Font Complete.ttf
Hack Italic Nerd Font Complete.ttf
Hack Regular Nerd Font Complete.ttf

to `~/.local/share/fonts`  or to  `/usr/share/fonts`

Then run::
```bash
  fc-cache -f -v
```

to check is font installed::
```bash
  fc-list | grep "Hack"
```

If you need to manually configure the fonts in xfce4-terminal
(for nvim)

~/.config/xfce4/terminal/terminalrc::
  FontName=Hack Nerd Font 10


Create a List of all Installed Packages
=======================================

The following command will store the list of all installed packages on your
Debian-based system to a file called pkgs_list.txt::

```bash
sudo dpkg-query -f '${binary:Package}\n' -W > pckgs_list.txt
```

Then you have the list, you can install the same packages with::
```bash
sudo xargs -a packages_list.txt apt install
```


Create a new config section
===========================

```sh
mkdir -p ~/.dotfiles/mpv/.config
mv ~/.config/mpv/ ~/.dotfiles/mpv/.config
cd ~/.dotfiles/
stow mpv
```

Check
```sh
ls -l ~/.config/
lrwxrwxrwx  ...  mpv -> ../.dotfiles/mpv/.config/mpv
```


More
====

[Condigure sleep-in-inactive systemd-logind](./docs/sleep-inactive.md)

[Darkai README](darkai/.local/share/themes/Darkai/README.md)
