#!/usr/bin/env bash
echo "sites_list"

declare -A sites_list
sites_list[debian]="debian.org"
sites_list[awesomewm-apidoc]="https://awesomewm.org/doc/api"
sites_list[stolayrov_info]="stolyarov.info"
sites_list[nerdfornts]=nerdfonts.com/cheat-sheet
sites_list[lua-users]=lua-users.org
sites_list[mono]=mono-project.com
