#:!/usr/bin/env bash

# Goal: Quick search in specified yt channel
# Quick Access:
#   via Awesome ShortKey:  Super-p,s, input:"ytchannel"

declare -A yt_channels
yt_channels[dmitry-ketov]="DmitryKetov"
yt_channels[uneex]="unx7784"
yt_channels[jugru]="JUGru"

yt_channels[comp-sci-center-ru]="CompscicenterRu"
yt_channels[lectory_fpmi]="lectory_fpmi"
yt_channels[mipt_study]="mipt_study"
yt_channels[frtk_mfti]="user-lw6iw3my4h"
yt_channels[cpp-amalov]="vividbw"
yt_channels[cpp-kvladimirov]="tilir"

yt_channels[distrotube]="DistroTube"
yt_channels[chrisatmachine]="chrisatmachine"
yt_channels[itvdn]="ITVDN"
yt_channels[dotnext]="DotNextConf"
yt_channels[highload]="HighLoadChannel"
yt_channels[slurm_io]="slurm_io"
#yt_channels[]=""
#https://www.youtube.com/@

