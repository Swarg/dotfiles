" enable filetype detection:
filetype on
filetype plugin on
filetype indent on " file type based indentation

syntax on
" Support True Colors in terminal
:set termguicolors
" iceberg PaperColor monokai gruvbox
colorscheme monokai
set background=dark
" set term=xterm-256
set nocompatible " not compatible with vi
"set nocp
"set t_Co=256
"set t_Sb=m
"set t_Sf=m

set encoding=utf-8
set number
set colorcolumn=80

"for Cygwin remove \n\r under windows
set fileformat=unix
set fileformats=unix,dos
"set nobinary

set tabstop=2
set shiftwidth=2
set et
set mouse=a
set smarttab
set smartindent
set nowrap
"//procedure foo(integer, boolean){{{ ...}}}
set foldmethod=marker

set splitbelow splitright


" Navigate with <Ctrl>-hjkl in Insert mode
inoremap <C-h> <C-o>h
inoremap <C-j> <C-o>j
inoremap <C-k> <C-o>k
inoremap <C-l> <C-o>l
" , + m - copy the full path of current opened file to clipboard
nnoremap <leader>m :let @+=expand("%:p")<CR> :let @*=expand("%:p")<CR>:echo @*<CR>


"manual loading
"set rtp+=~/.vim/plugged/youcompleteme


"let g:ycm_keep_logfile = 1
"let g:ycm_log_level = 'debug'
"
"filetype plugin indent on


" set a map leader for more key combos
" pe/vim-fugitive'u
let g:mapleader = ' '

" Fuzzy search
"map <C-p> :Telescope find_files<CR>

" Read-only rtf through unrtf
"autocmd BufReadPre *.rtf silent set ro
"autocmd BufReadPost *.rtf silent %!unrtf --text

"use nasm syntax insted default asm
autocmd BufNew,BufRead *.asm set ft=nasm

" for js/coffee/jade files, 4 spaces
"autocmd Filetype javascript setlocal ts=4 sw=4 sts=0 expandtab
"autocmd Filetype java setlocal ts=4 sw=4 sts=0 expandtab
autocmd BufRead,BufNewFile   *.c,*.h,*.java set ts=4 sw=4 sts=0 expandtab "noic cin noexpandtab

"augroup filetype_c
"    autocmd!
"    :autocmd FileType c setlocal tabstop=4 shiftwidth=4 expandtab "softtabstop=4
"    :autocmd FileType c nnoremap <buffer> <localleader>c I/*<space><esc><s-a><space>*/<esc>
"augroup end

highlight ExtraWhitespace ctermbg=red guibg=#5f0000
match ExtraWhitespace /\s\+$/
au BufWinEnter * match ExtraWhitespace /\s\+$/
au InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
au InsertLeave * match ExtraWhitespace /\s\+$/
au BufWinLeave * call clearmatches()

" enable filetype detection:
filetype on
filetype plugin on
filetype indent on " file type based indentation

