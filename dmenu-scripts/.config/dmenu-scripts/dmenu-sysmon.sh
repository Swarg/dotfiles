#!/bin/bash
# dmenu scrit for launching system monitorings

declare -a options=(" top
 htop
 iftop
 glance
 iftop
 iotop
 iptraf-ng
 s-tui
 quite ")

choice=$(echo -e "${options[@]}" | dmenu -l -i -p 'SysMons: ')

if [ "$choice" == " quit " ]; then
  echo "Quit"
elif [ "$choice" == " top " ]; then
  exec sh -e top
elif [ "$choice" == " htop " ]; then
  exec sh -e htop
elif [ "$choice" == " iftop " ]; then
  exec sh -e iftop
fi


