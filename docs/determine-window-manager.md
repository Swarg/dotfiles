## Determine Window Manager from CLI

### Determine which window managers are installed

To view the default display manager:

Debian/Ubuntu
```sh
cat /etc/X11/default-display-manager
```
Output Example for this dotfiles:
`/usr/sbin/lightdm`

RedHat & Fedora:
```sh
/etc/sysconfig/desktop
```

OpenSuse:
/etc/sysconfig/displaymanager

```sh
grep 'ExecStart=' /etc/systemd/system/display-manager.service
```
ExecStart=/usr/sbin/lightdm



Display the name and status of the active display manager service on your machine.
```sh
sudo systemctl status display-manager
```
```
 Loaded: loaded (/lib/systemd/system/lightdm.service; enabled; vendor preset: enabled)
     Active: active (running) ...
     CGroup: /system.slice/lightdm.service
             ├─946 /usr/sbin/lightdm
             └─986 /usr/lib/xorg/Xorg :0 -seat seat0 -auth /var/run/lightdm/root/:0 -nolisten tcp vt7 -novtswitch
```
```sh
sudo systemctl --property=Id,Description show display-manager.service
```
Output:
```
Id=lightdm.service
Description=Light Display Manager
```

The display manager name should be in DESKTOP_SESSION
```sh
echo $DESKTOP_SESSION
```
awesome

```sh
echo $XDG_SESSION_DESKTOP
```
awesome

GDMSESSION - returns the Window Manager
```sh
echo $GDMSESSION
```
awesome

```sh
#!/bin/bash

windowManagerName () {
    local window=$(
        xprop -root -notype
    )

    local identifier=$(
        echo "${window}" |
        awk '$1=="_NET_SUPPORTING_WM_CHECK:"{print $5}'
    )

    local attributes=$(
        xprop -id "${identifier}" -notype -f _NET_WM_NAME 8t
    )

    local name=$(
        echo "${attributes}" |
        grep "_NET_WM_NAME = " |
        cut --delimiter=' ' --fields=3 |
        cut --delimiter='"' --fields=2
    )

    echo "${name}"
}
```
Oneline:
```sh
id=$(xprop -root -notype | awk '$1=="_NET_SUPPORTING_WM_CHECK:"{print $5}'); xprop -id "${id}" -notype -f _NET_WM_NAME 8t | grep "_NET_WM_NAME = " | cut --delimiter=' ' --fields=3 | cut --delimiter='"' --fields=2
```
say: `awesome`



# Way to find all window managers (Debian)
limit the search to window managers provided by a Debian package,
there's an easy way.
All window managers in Debian provide the
`x-window-manager` [alternative](https://wiki.debian.org/DebianAlternatives)
```sh
update-alternatives --list x-window-manager
```
Output Example:
```
/usr/bin/awesome
/usr/bin/i3
/usr/bin/xfwm4
```

```sh
ls -l /etc/alternatives/x-window-manager
... /etc/alternatives/x-window-manager -> /usr/bin/xfwm4
```


```sh
dpkg -l | grep -i 'window.*manager' | sed -ne '/^ii /!d;s/^ii *//;/^lib/d;p'
```
