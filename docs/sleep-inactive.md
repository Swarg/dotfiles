## Configure auto sleep in inactive

[Login manager configuration files]
(https://www.freedesktop.org/software/systemd/man/logind.conf.html)

You can configure what the laptop should do on certain events like:
  - closing the lid or
  - pressing the power button
in `/etc/systemd/logind.conf`.
 If you want it to, for example, suspend and hibernate after a specific amount
of time you set that time in `/etc/systemd/sleep.conf` by uncommenting
`HibernateDelaySec=....` Then you set `HandleLidSwitch=suspend-then-hibernate`
which will make it first suspend and hibernate after the time you specified.

If you want to enable sleep when inactive, you can uncomment `IdleActionSec`
and `IdleAction` in `/etc/systemd/logind.conf` and configure it to your liking.

After edit the config use:
```sh
sudo systemctl restart systemd-logind
```

### Example: setup idle time to 2 hours:
/etc/systemd/logind.conf:
```
[Login]
IdleActionSec=120min
```


### Force way to whole system

Disable suspend and hibernation:
```sh
sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target
```

To re-enable hibernate and suspend use the following command:

```sh
sudo systemctl unmask sleep.target suspend.target hibernate.target hybrid-sleep.target
```

## FAQ:

Q: would like to have the closing of my laptop lid ignored on a Debian machine.
A:If you just want to prevent suspending when the lid is closed you can
set the following options in `/etc/systemd/logind.conf`:
```
[Login]
HandleLidSwitch=ignore
HandleLidSwitchDocked=ignore
```
restart the service or reboot your machine:
```sh
systemctl restart systemd-logind.service
```

Q: /etc/systemd/logind.conf is being ignored
A: This might help someone.
I was having the same issue because `/etc/systemd/logind.conf` was a symlink.
After copying the original file instead of symlinking it logind.conf
isn't ignored anymore


