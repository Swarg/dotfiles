-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
--local lain = require("lain")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- Load Debian menu entries
local debian = require("debian.menu")
local has_fdo, freedesktop = pcall(require, "freedesktop")

local key = awful.key
local aclient = awful.client
local abutton = awful.button
local ascreen = awful.screen

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
  naughty.notify({
    preset = naughty.config.presets.critical,
    title = "Oops, there were errors during startup!",
    text = awesome.startup_errors
  })
end

-- Handle runtime errors after startup
do
  local in_error = false
  awesome.connect_signal("debug::error", function(err)
    -- Make sure we don't go into an endless error loop
    if in_error then return end
    in_error = true

    naughty.notify({
      preset = naughty.config.presets.critical,
      title = "Oops, an error happened!",
      text = tostring(err)
    })
    in_error = false
  end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
-- ~/.config/awesome/mytheme.lua
--beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")
local home = os.getenv("HOME")
-- . multicolor  powerarrow  powerarrow-blue
local themename = "." --multicolor"
local themepath = home .."/.config/awesome/themes/".. themename .."/theme.lua"
beautiful.init(themepath)
--gears.filesystem.get_configuration_dir() .. "theme.lua")


FontName = "Hack Nerd Font"
-- This is used later as the default terminal and editor to run.
terminal = "x-terminal-emulator"
--terminal = "alarcitty"
editor = os.getenv("EDITOR") or "nvim"
editor_cmd = terminal .. " -e " .. editor
htop_cmd = terminal .. " -e " .. "htop"
-- terminal .. " -e " .. "nmtui"   -- text ui for control network

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
mk = "Mod4"
alt = "Mod1"
ctrl = "Control"
shift = "Shift"


-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
  awful.layout.suit.tile,
  awful.layout.suit.floating,
  -- awful.layout.suit.tile.left,
  -- awful.layout.suit.tile.bottom,
  -- awful.layout.suit.tile.top,
  -- awful.layout.suit.fair,
  -- awful.layout.suit.fair.horizontal,
  -- awful.layout.suit.spiral.dwindle,
  awful.layout.suit.max,
  --awful.layout.suit.max.fullscreen,
  awful.layout.suit.spiral,
  awful.layout.suit.magnifier,
  --awful.layout.suit.corner.nw,
  -- awful.layout.suit.corner.ne,
  -- awful.layout.suit.corner.sw,
  -- awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
  { "hotkeys",       function() hotkeys_popup.show_help(nil, ascreen.focused()) end },
  { "manual",        terminal .. " -e man awesome" },
  { "edit config",   editor_cmd .. " " .. awesome.conffile },
  { "restart",       awesome.restart },
  { "quit (logout)", function() awesome.quit() end },
}

local menu_awesome = { "awesome", myawesomemenu, beautiful.awesome_icon }
local menu_terminal = { "open terminal", terminal }

if has_fdo then
  mymainmenu = freedesktop.menu.build({
    before = { menu_awesome },
    after = { menu_terminal }
  })
else
  mymainmenu = awful.menu({
    items = {
      menu_awesome,
      { "Debian", debian.menu.Debian_menu.Debian },
      menu_terminal,
    }
  })
end

-- No awesome icon in tray
--mylauncher = awful.widget.launcher(
-- { image = beautiful.awesome_icon, menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock()

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
  awful.button({}, 1, function(t) t:view_only() end),
  awful.button({ mk }, 1, function(t)
    if client.focus then
      client.focus:move_to_tag(t)
    end
  end),
  awful.button({}, 3, awful.tag.viewtoggle),
  awful.button({ mk }, 3, function(t)
    if client.focus then
      client.focus:toggle_tag(t)
    end
  end),
  awful.button({}, 4, function(t) awful.tag.viewnext(t.screen) end),
  awful.button({}, 5, function(t) awful.tag.viewprev(t.screen) end)
)

local tasklist_buttons = gears.table.join(
  awful.button({}, 1,
    function(c)
      if c == client.focus then
        c.minimized = true
      else
        c:emit_signal("request::activate", "tasklist", { raise = true })
      end
    end
  ),
  awful.button({}, 3,
    function() awful.menu.client_list({ theme = { width = 250 } }) end),
  awful.button({}, 4, function() aclient.focus.byidx(1) end),
  awful.button({}, 5, function() aclient.focus.byidx( -1) end)
)

local function set_wallpaper(s)
  -- Wallpaper
  if beautiful.wallpaper then
    local wallpaper = beautiful.wallpaper
    -- If wallpaper is a function, call it with the screen
    if type(wallpaper) == "function" then
      wallpaper = wallpaper(s)
    end
    gears.wallpaper.maximized(wallpaper, s, true)
  end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
  -- Wallpaper
  set_wallpaper(s)

  -- Each screen has its own tag table.
  awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" },
    s, awful.layout.layouts[1])

  -- Create a promptbox for each screen
  s.mypromptbox = awful.widget.prompt()
  -- Create an imagebox widget which will contain an icon indicating which layout we're using.
  -- We need one layoutbox per screen.
  s.mylayoutbox = awful.widget.layoutbox(s)
  s.mylayoutbox:buttons(gears.table.join(
    awful.button({}, 1, function() awful.layout.inc(1) end),
    awful.button({}, 3, function() awful.layout.inc( -1) end),
    awful.button({}, 4, function() awful.layout.inc(1) end),
    awful.button({}, 5, function() awful.layout.inc( -1) end)))
  -- Create a taglist widget
  s.mytaglist = awful.widget.taglist {
    screen  = s,
    -- filter  = awful.widget.taglist.filter.all,
    -- Show only not empty or selected workspaces
    filter  = function(t) return t.selected or #t:clients() > 0 end,
    buttons = taglist_buttons
  }

  -- Create a tasklist widget
  s.mytasklist = awful.widget.tasklist {
    screen  = s,
    filter  = awful.widget.tasklist.filter.currenttags,
    buttons = tasklist_buttons
  }

  -- Create the wibox
  s.mywibox = awful.wibar({ position = "top", height = 22, screen = s })

  -- Add widgets to the wibox
  s.mywibox:setup {
    layout = wibox.layout.align.horizontal,
    { -- Left widgets
      layout = wibox.layout.fixed.horizontal,
      mylauncher,
      s.mytaglist,
      s.mypromptbox,
    },
    s.mytasklist, -- Middle widget
    { -- Right widgets
      layout = wibox.layout.fixed.horizontal,
      mykeyboardlayout,
      wibox.widget.systray(),
      mytextclock,
      s.mylayoutbox,
    },
  }
end
)
--awful.screen.connect_for_each_screen(function(s) beautiful.at_screen_connect(s) end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
  awful.button({}, 3, function() mymainmenu:toggle() end),
  awful.button({}, 4, awful.tag.viewnext),
  awful.button({}, 5, awful.tag.viewprev)
))
-- }}}

-- Wrapper
function bk(k0, k1, act, dsc, gr)
  return awful.key(k0, k1, act, { description = dsc, group = gr })
end

function bydirection(d)
  awful.client.focus.global_bydirection(d)
  if client.focus then
    client.focus:raise()
  end
end

-- {{{ Key bindings
globalkeys = gears.table.join(
  bk({ mk }, "s", hotkeys_popup.show_help, "show help", "awesome"),
  bk({ mk }, "w", function() mymainmenu:show() end, "show main menu", "awesome"),
  bk({ mk }, "Page_Down", awful.tag.viewprev, "view previous", "tag"), -- Left
  bk({ mk }, "Page_Up", awful.tag.viewnext, "view next", "tag"), -- Right
  bk({ mk }, "Escape", awful.tag.history.restore, "go back", "tag"),

  -- Tag browsing(Workspaces) ALT+TAB (ALT+SHIFT+TAB)
  bk({ mk }, "Tab", awful.tag.viewnext, "view next", "tag"),
  bk({ mk, shift }, "Tab", awful.tag.viewprev, "view previous", "tag"),

  -- Non-empty tag browsing CTRL+TAB (CTRL+SHIFT+TAB)
  --bk({ ctrl }, "Tab", function() lain.util.tag_view_nonempty(-1) end, "view  previous nonempty", "tag"),
  --bk({ ctrl, shift }, "Tab", function() lain.util.tag_view_nonempty(1) end, "view  previous nonempty", "tag"),

  -- Navigations
  --bk({ mk }, "l", function() aclient.focus.byidx(1) end, "focus next by index", "client"), -- j
  --bk({ mk }, "h", function() aclient.focus.byidx(-1) end, "focus previous by index", "client"), -- k

  -- By direction client focus
  bk({ mk }, "h", function() bydirection("left") end, "Focus left", "client"),
  bk({ mk }, "j", function() bydirection("down") end, "Focus down", "client"),
  bk({ mk }, "k", function() bydirection("up") end, "Focus up", "client"),
  bk({ mk }, "l", function() bydirection("right") end, "Focus right", "client"),
  -- Resize look clientkeys
  --bk({ mk, }, "Right", function() awful.tag.incmwfact(0.05) end, "increase master width factor", "layout"), --l
  --bk({ mk, }, "Left", function() awful.tag.incmwfact( -0.05) end, "decrease master width factor", "layout"), --h


  -- Layout manipulation
  --bk({ mk, "Shift" }, "j", function() aclient.swap.byidx(1) end, "swap with next client by index", "client"),
  --bk({ mk, "Shift" }, "k", function() aclient.swap.byidx( -1) end, "swap with previous client by index", "client"),

  --bk({ mk, "Control" }, "j", function() ascreen.focus_relative(1) end, "focus the next screen", "screen"),
  --bk({ mk, "Control" }, "k", function() ascreen.focus_relative( -1) end, "focus the previous screen", "screen"),
  --
  bk({ mk }, "u", aclient.urgent.jumpto, "jump to urgent client", "client"),
  bk({ mk }, "Tab",
    function()
      aclient.focus.history.previous()
      if client.focus then
        client.focus:raise()
      end
    end, "go back", "client"),

  -- Standard program
  bk({ mk, }, "Return", function() awful.spawn(terminal) end, "open a terminal", "launcher"),
  bk({ mk, ctrl }, "r", awesome.restart, "reload awesome", "awesome"),
  bk({ mk, shift }, "q", awesome.quit, "quit awesome", "awesome"),
  bk({ mk, }, "Right", function() awful.tag.incmwfact(0.05) end, "increase master width factor", "layout"), --l
  bk({ mk, }, "Left", function() awful.tag.incmwfact( -0.05) end, "decrease master width factor", "layout"), --h
  bk({ mk, shift }, "Left", function() awful.tag.incnmaster(1, nil, true) end, "increase the number of master client",
    "layout"), --h
  bk({ mk, "Shift" }, "Right", function() awful.tag.incnmaster( -1, nil, true) end,
    "decrease the number of master client", "layout"), --l
  bk({ mk, "Control" }, "Left", function() awful.tag.incncol(1, nil, true) end, "increase the number of columns",
    "layout"),
  bk({ mk, "Control" }, "Right", function() awful.tag.incncol( -1, nil, true) end, "decrease the number of columns",
    "layout"),
  -- Layout
  bk({ mk, }, "space", function() awful.layout.inc(1) end, "select next", "layout"),
  bk({ mk, "Shift" }, "space", function() awful.layout.inc( -1) end, "select previous", "layout"),


  -- Dmenu (Prompt)
  bk({ mk }, "d", --function() awful.util.spawn("dmenu_run") end,
    function()
      awful.spawn(string.format("dmenu_run -i -nb '%s' -nf '%s' -sb '%s' -sf '%s' -fn '%s:bold:pixelsize=16'",
        beautiful.bg_normal, beautiful.fg_normal, beautiful.bg_focus, beautiful.fg_focus, FontName))
    end,
    "run dmenu", "launcher"),

  -- Menubar bk({ mk }, "p", function() menubar.show() end, "show the menubar", "launcher"),
  -- dmenu scripts (Super + p followed by KEY)
  awful.key({ mk }, "p", function()
    local dms = "~/.config/dmenu-scripts/"
    local shell = awful.spawn.with_shell
    local grabber
    grabber = awful.keygrabber.run(
      function(_, k, event)
        if event == "release" then return end

            if k == "e" then shell(dms .. "dm-confedit")
        elseif k == "c" then shell(dms .. "dm-pickcolor")
        elseif k == "n" then shell(dms .. "dm-note")
        elseif k == "t" then shell(dms .. "dm-translate")
        elseif k == "s" then shell(dms .. "dm-websearch")
        elseif k == "q" then shell(dms .. "dm-logout")
        elseif k == "k" then shell(dms .. "dm-kill")
        -- TODO elseif k == "u" then shell(dms .. "dm-usbmount")
        -- TODO elseif k == "u" then shell(dms .. "dm-music")
        end
        awful.keygrabber.stop(grabber)
      end
    )
  end,
    { description = "followed by KEY", group = "tools" }
  ),



  --[[  bk({ mk }, "d", function()
      awful.util.spawn(string.format("./.dmenu/dmenu-edit-configs.sh '%s' '%s' '%s' '%s' '%s:bold:pixelsize=16'",
      beautiful.bg_normal, beautiful.fg_normal, beautiful.bg_focus, beautiful.fg_focus, FontName))
    end,
    "edit config files", "launcher"),]]
  --TODO sysmon surfraw

  -- Firefox
  bk({ mk }, "b", function() awful.util.spawn("firefox") end, "run firefox", "apps"),
  bk({ mk }, "t", function() awful.util.spawn(htop_cmd) end, "run htop", "apps"),
  bk({ mk }, "y", function() awful.spawn.with_shell("flameshot gui -p ~/Pictures/screenshots") end, "screenshot", "apps"),
  bk({ mk, "Shift" }, "t", function() awful.spawn.with_shell("flameshot full -p ~/Pictures/screenshots") end,
    "screenshot", "apps"),

  --bk({ mk }, "n", function() awful.util.spawn("neovim") end, "run neovim", "gui apps" ),
  --bk({ mk }, "e", function() awful.util.spawn("thunar") end, "run thunar", "gui apps" ),


  -- Copy primary to clipboard (terminals to gtk)
  bk({ mk }, "c", function() awful.spawn.with_shell("xsel | xsel -i -b") end, "cb copy terminal to gtk", "hotkeys"),
  -- Copy clipboard to primary (gtk to terminals)
  bk({ mk }, "v", function() awful.spawn.with_shell("xsel -b | xsel") end, "cb copy gtk to terminal", "hotkeys"),
  bk({ mk }, "x",
    function()
      awful.prompt.run {
        prompt       = "Run Lua code: ",
        textbox      = ascreen.focused().mypromptbox.widget,
        exe_callback = awful.util.eval,
        history_path = awful.util.get_cache_dir() .. "/history_eval"
      }
    end, "lua execute prompt", "awesome"),

  bk({ mk, "Shift" }, "b",
    function() mouse.screen.mywibox.visible = not mouse.screen.mywibox.visible end,
    "Hide/Show wibox (bar)", "awesome"), --(bar - main panel with workspaces, time, ect)

  --bk({ mk }, "F12", function() awful.spawn { "xlock" } end, "xlock", "awesome"),  -- dont work
  bk({ mk }, "F10", function() awful.spawn.with_shell("cal -m | xmessage -timeout 10 -file -") end, "msg", "awesome"),
  bk({ mk }, "`", function() awful.spawn.with_shell("~/tools/translate1") end, "translate", "tools")
)

clientkeys = gears.table.join(
-- Resize Super+Shift + hjkl
  bk({ mk, shift }, "h", function() awful.tag.incmwfact( -0.01) end, "resize [", "client"),
  bk({ mk, shift }, "l", function() awful.tag.incmwfact(0.01) end, "resize ]", "client"),
  bk({ mk, shift }, "j", function() awful.client.incwfact(0.04) end, "resize ^", "client"),
  bk({ mk, shift }, "k", function() awful.client.incwfact( -0.04) end, "resize v", "client"),
  -- Move for floating (not fork for tailing mod)
  bk({ mk, alt }, "h", function(c) c:relative_move( -40, 0, -40, 0) end, "move [", "client"),
  bk({ mk, alt }, "j", function(c) c:relative_move(0, 40, 0, 0) end, "move v", "client"),
  bk({ mk, alt }, "k", function(c) c:relative_move(0, -40, 0, 0) end, "move ^", "client"),
  bk({ mk, alt }, "l", function(c) c:relative_move(40, 0, 0, 0) end, "move ]", "client"),
  --[[
  bk({ mk, "Shift"   }, "h", function (c) c:relative_move(-20,   0,   0,   0) end, "resize l", "client"),
  bk({ mk, "Shift"   }, "j", function (c) c:relative_move(  0,  20,   0,   0) end, "resize d", "client"),
  bk({ mk, "Shift"   }, "k", function (c) c:relative_move(  0, -20,   0,   0) end, "resize k", "client"),
  bk({ mk, "Shift"   }, "l", function (c) c:relative_move( 20,   0,   0,   0) end, "resize r", "client"),]]

  bk({ mk, }, "f", function(c)
    c.fullscreen = not c.fullscreen
    c:raise()
  end, "toggle fullscreen", "client"),
  bk({ mk, shift }, "f", aclient.floating.toggle, "toggle floating", "client"),
  -- TODO change gaps on the fly mk+g, mk+shift+g

  bk({ mk, }, "q", function(c) c:kill() end, "close", "client"),
  bk({ mk, ctrl }, "Return", function(c) c:swap(aclient.getmaster()) end, "move to master", "client"),
  bk({ mk, }, "o", function(c) c:move_to_screen() end, "move to screen", "client"),
  bk({ mk, "Shift" }, "t", function(c) c.ontop = not c.ontop end, "toggle keep on top", "client"),

  -- The client currently has the input focus, so it cannot be
  -- minimized, since minimized clients can't have the focus.
  bk({ mk, }, "n", function(c) c.minimized = true end, "minimize", "client"),
  bk({ mk, "Control" }, "n",
    function()
      local c = aclient.restore()
      -- Focus restored client
      if c then
        c:emit_signal(
          "request::activate", "key.unminimize", { raise = true }
        )
      end
    end, "restore minimized", "client"),

  bk({ mk, }, "m", function(c)
    c.maximized = not c.maximized
    c:raise()
  end, "(un)maximize", "client"),
  key({ mk, "Control" }, "m",
    function(c)
      c.maximized_vertical = not c.maximized_vertical
      c:raise()
    end,
    { description = "(un)maximize vertically", group = "client" }),
  key({ mk, "Shift" }, "m",
    function(c)
      c.maximized_horizontal = not c.maximized_horizontal
      c:raise()
    end,
    { description = "(un)maximize horizontally", group = "client" })
)
--https://awesomewm.org/apidoc/documentation/90-FAQ.md.html

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 10 do
  globalkeys = gears.table.join(globalkeys,
    -- View tag only.
    key({ mk }, "#" .. i + 9,
      function()
        local tag = ascreen.focused().tags[i]
        if tag then
          tag:view_only()
        end
      end,
      { description = "view tag #" .. i, group = "tag" }),

    -- Toggle tag display.
    key({ mk, "Control" }, "#" .. i + 9,
      function()
        local tag = ascreen.focused().tags[i]
        if tag then
          awful.tag.viewtoggle(tag)
        end
      end,
      { description = "toggle tag #" .. i, group = "tag" }),

    -- Move  client to tag.
    key({ mk, "Shift" }, "#" .. i + 9,
      function()
        if client.focus then
          local tag = client.focus.screen.tags[i]
          if tag then
            client.focus:move_to_tag(tag)
          end
        end
      end,
      { description = "move focused client to tag #" .. i, group = "tag" }),

    -- Toggle tag on focused client.
    key({ mk, "Control", "Shift" }, "#" .. i + 9,
      function()
        if aclient.focus then
          local tag = client.focus.screen.tags[i]
          if tag then
            client.focus:toggle_tag(tag)
          end
        end
      end,
      { description = "toggle focused client on tag #" .. i, group = "tag" })
  )
end

clientbuttons = gears.table.join(
  awful.button({}, 1, function(c)
    c:emit_signal("request::activate", "mouse_click", { raise = true })
  end),
  awful.button({ mk }, 1, function(c)
    c:emit_signal("request::activate", "mouse_click", { raise = true })
    awful.mouse.client.move(c)
  end),
  awful.button({ mk }, 3, function(c)
    c:emit_signal("request::activate", "mouse_click", { raise = true })
    awful.mouse.client.resize(c)
  end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new client (through the "manage" signal).
awful.rules.rules = {
  -- All client will match this rule.
  { rule = {},
    properties = {
      border_width = beautiful.border_width,
      border_color = beautiful.border_normal,
      focus = aclient.focus.filter,
      raise = true,
      keys = clientkeys,
      buttons = clientbuttons,
      screen = ascreen.preferred,
      placement = awful.placement.no_overlap + awful.placement.no_offscreen,
      -- fix random gaps for term (Character-baset window, WM_NORMAL_HINTS)
      size_hints_honor = false,
    }
  },

  -- Floating client.
  { rule_any = {
    instance = {
      "DTA", -- Firefox addon DownThemAll.
      "copyq", -- Includes session name in class.
      "pinentry",
      "gxmessage",
      "About"
    },
    class = {
      "Arandr",
      "Blueman-manager",
      "Gpick",
      "Kruler",
      "MessageWin", -- kalarm.
      "Sxiv",
      "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
      "Wpa_gui",
      "veromix",
      "xtightvncviewer",
    },

    -- Note that the name property shown in xprop might be set slightly after creation of the client
    -- and the name shown there might not match defined rules here.
    name = {
      "Event Tester", -- xev.
    },
    role = {
      "AlarmWindow", -- Thunderbird's calendar.
      "ConfigManager", -- Thunderbird's about:config.
      "pop-up", -- e.g. Google Chrome's (detached) Developer Tools.
      "Preferences",
      "setup",
    }
  }, properties = { floating = true } },

  -- Add titlebars to normal client and dialogs
  { rule_any = { type = { "normal", "dialog" } },
    properties = { titlebars_enabled = false }
  },

  -- Set Firefox to always map on the tag named "2" on screen 1.
  -- { rule = { class = "Firefox" },
  --   properties = { screen = 1, tag = "2" } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function(c)
  -- Set the windows at the slave,
  -- i.e. put it at the end of others instead of setting it master.
  if not awesome.startup then aclient.setslave(c) end

  if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
    -- Prevent client from being unreachable after screen count changes.
    awful.placement.no_offscreen(c)
  end
end)

-- Make all Floating Windows always in top
--client.connect_signal("property::floating", function(c)
--  if c.floating then c.ontop = true else c.ontop = false end
--end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
  -- buttons for the titlebar
  local buttons = gears.table.join(
    awful.button({}, 1, function()
      c:emit_signal("request::activate", "titlebar", { raise = true })
      awful.mouse.client.move(c)
    end),
    awful.button({}, 3, function()
      c:emit_signal("request::activate", "titlebar", { raise = true })
      awful.mouse.client.resize(c)
    end)
  )

  awful.titlebar(c):setup {
    { -- Left
      awful.titlebar.widget.iconwidget(c),
      buttons = buttons,
      layout  = wibox.layout.fixed.horizontal
    },
    { -- Middle
      { -- Title
        align  = "center",
        widget = awful.titlebar.widget.titlewidget(c)
      },
      buttons = buttons,
      layout  = wibox.layout.flex.horizontal
    },
    { -- Right
      awful.titlebar.widget.floatingbutton(c),
      awful.titlebar.widget.maximizedbutton(c),
      awful.titlebar.widget.stickybutton(c),
      awful.titlebar.widget.ontopbutton(c),
      awful.titlebar.widget.closebutton(c),
      layout = wibox.layout.fixed.horizontal()
    },
    layout = wibox.layout.align.horizontal
  }
end)

-- Enable sloppy focus, so that focus follows mouse.
-- client.connect_signal("mouse::enter", function(c)
--     c:emit_signal("request::activate", "mouse_enter", {raise = false})
-- end)


-- No border for maximized clients
function border_adjust(c)
  if c.maximized then -- no borders if only 1 client visible
    c.border_width = 0
  elseif #awful.screen.focused().clients > 1 then
    c.border_width = beautiful.border_width
    c.border_color = beautiful.border_focus
  end
end

--client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("focus", border_adjust)
--client.connect_signal("property::maximized", border_adjust)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

-- Autostart
awful.spawn.with_shell("nm-applet")
-- Change keyboard language layout
awful.spawn.with_shell("setxkbmap -layout 'us,ru' -option 'grp:alt_shift_toggle'")


--awful.spawn.with_shell("xargs xwallpaper --stretch < ~/.cache/wall")
--awful.spawn.with_shell("~/.fehbg") -- set last saved feh wallpaper
--awful.spawn.with_shell("feh --randomize --bg-fill /usr/share/backgrounds/dtos-backgrounds/*") -- feh sets random wallpaper
--awful.spawn.with_shell("nitrogen --restore") -- if you prefer nitrogen to feh/xwallpaper
