local topbar               = {}

local gears                = require("gears")
local awful                = require("awful")
local wibox                = require("wibox")
local beautiful            = require("beautiful")
local gmatch, lines, floor = string.gmatch, io.lines, math.floor

-- on Debian pkg: awesome-extra
--local vicious              = require("vicious")
local status_ok, vicious = pcall(require, "vicious")
-- if not status_ok then return end

local theme                = {}
--theme.dir                  = os.getenv("HOME") .. "/.config/awesome/themes/powerarrow-blue"
--theme.widget_music         = theme.dir .. "/icons/note.png"
--theme.widget_mem           = theme.dir .. "/icons/mem.png"
--theme.widget_cpu           = theme.dir .. "/icons/cpu.png"

--local imem         = wibox.widget.imagebox(theme.widget_mem)
--local icpu         = wibox.widget.imagebox(theme.widget_cpu)


-- [    ] ================ [ Memory ] ================ [    ] --

-- Parsing Current Memory State from /proc/meminfo
local mem_update           = function()
  local mem_now = {}
  for line in lines("/proc/meminfo") do
    for k, v in gmatch(line, "([%a]+):[%s]+([%d]+).+") do
      if k == "MemTotal" then
        mem_now.total = floor(v / 1024 + 0.5)
      elseif k == "MemFree" then
        mem_now.free = floor(v / 1024 + 0.5)
      elseif k == "Buffers" then
        mem_now.buf = floor(v / 1024 + 0.5)
      elseif k == "Cached" then
        mem_now.cache = floor(v / 1024 + 0.5)
      elseif k == "SwapTotal" then
        mem_now.swap = floor(v / 1024 + 0.5)
      elseif k == "SwapFree" then
        mem_now.swapf = floor(v / 1024 + 0.5)
      elseif k == "SReclaimable" then
        mem_now.srec = floor(v / 1024 + 0.5)
      end
    end
  end
  local m = mem_now
  mem_now.used = m.total - m.free - m.buf - m.cache - m.srec
  mem_now.swapused = m.swap - m.swapf
  --mem_now.perc = math.floor(mem_now.used / mem_now.total * 100)
  return mem_now
end

-- Memory Widget Simple Text Used / Total Gb
topbar.memwval = function()
  local w = wibox.widget {
    {
      id     = "mytb",
      text   = "0",
      used   = 0,
      total  = 0,
      widget = wibox.widget.textbox,
    },
    layout     = wibox.layout.stack,
    -- TODO how call this from callback?
    set_update = function(self, val) --              
      self.mytb.text = string.format("  %0.1f / %0.1f Gb",
        self.mytb.used, self.mytb.total)
    end,
  }

  gears.timer {
    timeout   = 10,
    call_now  = true,
    autostart = true,
    callback  = function()
      local mi = mem_update()
      w.mytb.used = mi.used / 1024
      w.mytb.total = mi.total / 1024
      w.update = "x" -- TODO learn lua syntax and call as method not a setter
    end
  }
  return w
end

-- Graph Memory Usage implemented by vicious lib
topbar.memwgraph = function()
  if (vicious == nil) then
    return wibox.widget {
      markup = '<i>M</i><b>?</b>',
      align  = 'center',
      valign = 'center',
      widget = wibox.widget.textbox
    }
  end
  local memw = wibox.widget.graph()
  memw:set_width(20)
  memw:set_color(gears.color.create_solid_pattern("#ae81ff"))
  vicious.cache(vicious.widgets.mem)
  memw.opacity = "1"
  vicious.register(memw, vicious.widgets.mem, "$1", 10)
  beautiful.graph_bg = "#00000000"
  return memw
end

-- [ END ] ================ [ Memory ] ================ [ END ]--


--[[
local cpu     = lain.widget.cpu({
  settings = function()
    local pref = " "
    local cpu_usage = cpu_now.usage
    if (cpu_usage < 10) then
      pref = "  "
    end
    --widget:set_markup(markup.font(theme.font, pref .. cpu_usage .. "% "))
    widget:set_markup(pref .. cpu_usage .. "% ")
  end
})]] --[[
local cbg      = wibox.container.background
local wcm      = wibox.container.margin
local hrz      = wibox.layout.align.horizontal
local ww       = wibox.widget]]

topbar.cpuwgraph = function()
  if (vicious == nil) then
    return wibox.widget {
      markup = '<i>M</i><b>?</b>',
      align  = 'center',
      valign = 'center',
      widget = wibox.widget.textbox
    }
  end
  --[[cpuicon = wibox.widget {
        image  = "/usr/share/icons/oxygen/base/22x22/devices/cpu.png",
        resize = false,
        widget = wibox.widget.imagebox
    }]]
  local cpuwidget = wibox.widget.graph()
  cpuwidget:set_width(24)
  cpuwidget:set_color(gears.color.create_solid_pattern("#a1efe0"))
  vicious.cache(vicious.widgets.cpu)
  cpuwidget.opacity = "1"
  vicious.register(cpuwidget, vicious.widgets.cpu, "$1", 10)
  return cpuwidget
end

-- TODO
topbar.temp = function()
  return awful.widget.watch('sensors', 20, function(widget, stdout)
    for line in stdout:gmatch("[^\r\n]+") do
      if line:match("edge") then
        widget:set_text(line)
        break
      end
    end
  end)
end

-- Number of running screens
topbar.screensw = function()
  return awful.widget.watch('screen -ls', 30, function(widget, stdout)
    local i = 0
    for line in stdout:gmatch("[^\r\n]+") do
      if line:match("[^\t]+[09]+") then
        i = i + 1
      end
    end
    -- https://www.nerdfonts.com/cheat-sheet
    -- TODO nf-md-leaf_circle_outline 
    widget:set_text(" " .. i .. " ")
  end)
end

-- TODO select OS by uname -u
topbar.os = function()
  return awful.widget.watch('', 3600, function(widget, stdout)
    widget:set_text(" ")
  end)
end


-- apt list --upgradable  Count of upgradable packages and systemlogo
topbar.updatable_pkgs = function()
  local updwdg = awful.widget.watch('bash -c "apt list --upgradable | tail -n +2 | wc -l"', 3600,
    function(widget, stdout)
      widget:set_text("  " .. stdout .. "  ") -- 
    end)
  return updwdg
end


-- Tools
topbar.term = "xterm"
--
topbar.run_upgrade_pkg = function()
  --TODO not only Debian-based os
  local shell = awful.spawn.with_shell
  --xfce4-terminal -e "bash -c 'echo 123; exec $SHELL'"
  local cmd = "sudo apt-get update && sudo apt-get upgrade"
  shell(topbar.term .. " -e \"bash -c 'echo " .. string.gsub(cmd, "&&", "+") ..
    " && " .. cmd .. "&& " ..
    "notify-send log-file /var/log/apt/history.log; exec $SHELL'\"")

  -- /var/log/apt/
end

topbar.theme = theme
return topbar
