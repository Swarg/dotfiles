local M = {}

-- Setup keybindings for my own dmenu scripts
-- Adding this custom keybindings to show_help window

local awful = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup.widget")
local gears_debug = require("gears.debug") -- aka inspect / vim.inspect
require("awful.hotkeys_popup.keys")

-- See Refs: /usr/share/awesome/lib/awful/hotkeys_popup/widget.lua

-- Empty name to pass app-name matching (Show always) here must be app name
local dmscripts_rule = { name = { "" } }
for group_name, group_data in pairs({
    ["launcher Super-p +"] = { color = "#009F00", rule_any = dmscripts_rule }
}) do
    hotkeys_popup.add_group_rules(group_name, group_data)
end

-- Show Compos for quick run one of the my own dmenu-script
-- See ref: /usr/share/awesome/lib/awful/hotkeys_popup/keys/firefox.lua
local dmscripts_keys = {
  ["launcher Super-p +"] = {
    {
      modifiers = {},-- "Super-p + " },
      keys = {
        h = "dm-hub",
        e = "dm-confedit",
        b = "dm-books",
        c = "dm-pickcolor",
        n = "dm-note",
        t = "dm-translate",
        s = "dm-websearch",
        q = "dm-logout",
        k = "dm-kill",
        u = "dm-music",
        a = "dm-screen",
      }
    },
  }
}
-- add my own keybindings to help window called in Super-s (see rc.lua)
-- Aditional dingings fro Super-p +
function M.set_keybinginds()
  hotkeys_popup.add_hotkeys(dmscripts_keys)
end


function M.dmenu_run(font_name)
  local b = require("beautiful")
  awful.spawn(string.format(
    "dmenu_run -i -nb '%s' -nf '%s' -sb '%s' -sf '%s' -fn '%s:bold:pixelsize=16'",
    b.bg_normal, b.fg_normal, b.bg_focus, b.fg_focus, font_name))
end


-- Function triggered by type Super-p
-- here is the keystroke listener that runs the desired script
function M.run_desired_script()
  local shell = awful.spawn.with_shell
  local dms = "~/.config/dmenu-scripts/"
  local grabber
  grabber = awful.keygrabber.run(
    function(_, k, event)
      if event == "release" then return end
          if k == "h" then shell(dms .. "dm-hub")
      elseif k == "e" then shell(dms .. "dm-confedit")
      elseif k == "b" then shell(dms .. "dm-books")
      elseif k == "a" then shell(dms .. "dm-screen")
      elseif k == "s" then shell(dms .. "dm-websearch")
      elseif k == "t" then shell(dms .. "dm-translate")
      elseif k == "c" then shell(dms .. "dm-pickcolor")
      elseif k == "n" then shell(dms .. "dm-note")      -- ~/todo.txt
      elseif k == "k" then shell(dms .. "dm-kill")
      elseif k == "u" then shell(dms .. "dm-music")
      elseif k == "q" then shell(dms .. "dm-logout")
      elseif k == "p" then shell(dms .. "dm-pass")
      -- elseif k == "d" then dump2file()
      -- TODO elseif k == "u" then shell(dms .. "dm-usbmount")
      end
      awful.keygrabber.stop(grabber)
    end
  )
end

-- TODO dofile(/tmp/runme.lua)
-- [Debug] Alternative for Super+x (Lua execute promt)
local function dump2file()
  local file = io.open("/tmp/awesome", "w")
  if file then
    file:write(gears_debug.dump_return(hotkeys_popup, "hotkeys"))
    file:close()
  end
end
return M
