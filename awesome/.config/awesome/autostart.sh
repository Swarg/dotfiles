#!/bin/sh

# Description: Autostart script for Awesome-WM
# Usage:
#   add this line to ~/.config/awesome/rc.lua
#   awful.spawn.with_shell("autostart.sh")
# Dependencies:
# -- pulseaudio,pasystray,
# -- network-manager-gnome(nm-applet)
# -- x11-xkb-utils (setxkbmap)
# Author: Swarg

start() {
  [ -z "$(pidof -x $1)" ] && ${2:-$1} &
}

# xterm font size and theme color
[ -f "$HOME/.Xresources" ] && xrdb -merge "$HOME/.Xresources"
# xterm*faceName: Hack Nerd Font
# ! xterm*faceName: Monospace
# xterm*faceSize: 10
# XTerm*Background: #272822
# XTerm*Foreground: #9b9884

# Audio
start pulseaudio start-pulseaudio-x11
start pasystray

# PulseAudio Control in Systray  (from pakage:pasystray)
# systemctl --user restart pulseaudio.socket
# pasystray

# NetWork
start nm-applet

# Change keyboard language layout
setxkbmap -layout 'us,ru' -option 'grp:alt_shift_toggle'

# set screensaver and dpms to 30 minutes
run xset s 1800 1800
run xset dpms 1800 1800 1800
