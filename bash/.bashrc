# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt



# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias lsh='ls --color=auto -lh'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    #alias grep='grep --color=auto'
    #alias fgrep='fgrep --color=auto'
    #alias egrep='egrep --color=auto'
fi


# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'

# take current user PATH for root
alias sudop='sudo env PATH=$PATH'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# echo $XDG_SESSION_TYPE # x11 wayland

# Create new directory and cd to it
mkcdir () {
    mkdir -p -- "$1" && cd -P -- "$1"
}

alias n=nvim
alias nr='nvim README.md'
alias dic=sdcv
alias term=/usr/bin/x-terminal-emulator
alias tet=task-extractor
alias gdf="git diff"
# alias cd-data='cd /media/$USER/Data/'
alias cd.="cd ~/.dotfiles"

# easy way to deal with nested empty directories aka (java packages)
function supercd() { cd $(dirname $(find "$@" -type f | head));}

DIR_DEV0="${HOME}/dev"
NOW_YEAR="$(date +%Y)"
DIR_SRC0="/d/Dev/src"
DIR_DOC0="/d/Dev/doc"

DIR_DEV="${HOME}/dev/$(date +%Y)"
alias nh="nvim ${DIR_DEV}/howtos/README.md"
alias nMf="nvim Makefile"
alias nbg="nvim build.gradle"
alias cddev="cd $DIR_DEV/"
alias cddeva="cd $DIR_DEV/asm"
alias cddevc="cd $DIR_DEV/c"
alias cddevb="cd $DIR_DEV/bash"
alias cddeve="cd $DIR_DEV/elixir"
alias cddevn="cd $DIR_DEV/nvim"
alias cddevl="cd $DIR_DEV/lua"
alias cddevj="cd $DIR_DEV/java"
alias cddevp="cd $DIR_DEV/php"
alias cddevr="cd $DIR_DEV/rust"
alias cddevd="cd $DIR_DEV/docker"
alias cddevdb="cd $DIR_DEV/db"
alias cddevh="cd $DIR_DEV/howtos"
alias cddevL="cd $DIR_DEV/learn"
alias cddevgo="cd $DIR_DEV/golang"
alias cddevpy="cd $DIR_DEV/py"
alias cddevjq="cd $DIR_DEV/jq"
alias cddevjs="cd $DIR_DEV/js"
alias cddevcs="cd $DIR_DEV/cs"
alias cddevts="cd $DIR_DEV/ts"
alias cddevw="cd $DIR_DEV/web"

alias cdsrc="cd $DIR_SRC0/"
alias cdsrcc="cd $DIR_SRC0/c"
alias cdsrcl="cd $DIR_SRC0/lua"
alias cdsrcL="cd $DIR_SRC0/linux"
alias cdsrce="cd $DIR_SRC0/elixir"
alias cdsrcd="cd $DIR_SRC0/docker"
alias cdsrco="cd $DIR_SRC0/lua/openresty"

alias cddoc="cd $DIR_DOC0/"

alias grepc="grep --exclude-dir='.git' -rIn "
alias s-l="screen -ls"
alias s-r="screen -r"
alias s-o="screen"

#
alias em="/usr/local/bin/exercism"
alias emt="BATS_RUN_SKIPPED=true /usr/local/bin/exercism test"
alias mit="mix test"
alias bats.="BATS_RUN_SKIPPED=true /usr/local/bin/bats *.bats"

alias eclipse="~/.local/share/eclipse/latest/eclipse"

# for ~/tools/2fa
completion_2fa() {
  local STORAGE="/$HOME/.ssh/keys/.2fa/"
	local SERVICES # 2fa services name
	services=$(find "$STORAGE" -type d -printf "%f " | sed 's/.2fa\///')
	complete -W "$services" 2fa
	complete -W "$services" ~/.2fa/decrypt.key.sh
}
complete -F completion_2fa 2fa


# For quick open new termx with current directory
# [TODO] hot-key to spawn termx-window and cd commant of current xterm
alias dup='x-terminal-emulator -e "sh -c \"cd $(pwd); $SHELL\""'
alias reopen='x-terminal-emulator -e "sh -c \"cd $(pwd); $SHELL\""; exit'
# Copy pwd from cli to gtk-clipboard
alias pwdc="pwd | tr -d '\n' | xsel"
# Jump to Directory in gtk-clibpoard
alias cd.c="cd $(xsel -b)"

# ---- GIT ----
alias gst='git status'
alias gco='git checkout'

# -- Show Current Git branch (Only in project root) --
parse_git_branch() {
  if [ -d ".git" ]; then
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
  fi
}
export PS1="\[\e]0;\u@\h: \w\a\]${debian_chroot:+\($debian_chroot\)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$(parse_git_branch)\[\033[00m\]\$ "
# export PS1="\u@\h \[\033[32m\]\w\[\033[33m\]\$(parse_git_branch)\[\033[00m\] $ "
# \u@\h \[\033[32m\] - user, host name and its displaying color
# \w\[\033[33m\] - current working directory and its displaying color
# \$(parse_git_branch)\[\033[00m\] - git branch name and its displaying color
# --  --
# another version of the same thing:
# export PS1="\w \$(__git_ps1 '(%s)') \$ "
# https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh


# My Tools, bins for current users(Ansible)
PATH="$PATH:$HOME/tools/:$HOME/.local/bin/"

# PostgreSQL
PATH="$PATH:/usr/lib/postgresql/13/bin/"

# AskPassHelper: For nvim to save file with root permissions
# com -bar ww exe 'w !sudo tee >/dev/null %:p:S' | setl nomod
# sudo: a terminal is required to read the password;
# either use the -S option to read from standard input or configure an askpass helper
export SUDO_ASKPASS='/usr/lib/ssh/x11-ssh-askpass'

# Java
# To switch JDK use ~/tools/set-java [8|17|21]
export JAVA_HOME=/usr/lib/jvm/java
export JDK8=/usr/lib/jvm/java8
export JDK17=/usr/lib/jvm/java17
export JDK21=/usr/lib/jvm/java21
export GROOVY_HOME=/opt/groovy
export MICRONAUT_HOME=~/.micronaut
export PATH="$PATH:$JAVA_HOME/bin:$GROOVY_HOME/bin:$MICRONAUT_HOME/bin"



# OLD create soft link instead adding to PATH
# update-alternatives --install /usr/bin/java java /opt/jdk/jdk1.8.0_05/bin/java 100

# Gradle
# ~/tools/set-gradle <VER>
export GRADLE_HOME=/opt/gradle/
export PATH="$PATH:$GRADLE_HOME/bin"

# Maven
export MVN_HOME="/opt/maven"
export PATH="$PATH:$MVN_HOME/bin"
# $MVN_HOME/conf/settings.xml < here defined default localMavenRepo


# Ant
export PATH="$PATH:/opt/ant/apache-ant-1.10.12/bin"

# for gammu 3g modem
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# GoLang
export PATH="$PATH:/usr/local/go/bin"
# workspace for go-projects
# export GOPATH="TODO"

# pnpm
alias p=pnpm
export PNPM_HOME="$HOME/.local/share/pnpm"
case ":$PATH:" in
  *":$PNPM_HOME:"*) ;;
  *) export PATH="$PNPM_HOME:$PATH" ;;
esac
# pnpm end

# Node Version Manager
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion


# Dotnet available 6
export DOTNET_ROOT="/lib/dotnet/6/"
DOTNET_TOOLS="$HOME/.dotnet/tools/"
export PATH="$PATH:$DOTNET_ROOT:$DOTNET_TOOLS"
# telemetry off
export DOTNET_CLI_TELEMETRY_OPTOUT=1

# COMPOSER - PHP Package Manager. This path for cli tools installed via cmd:
# composer global require maintainer/app_name
PHP_COMPOSER_BIN="$HOME/.config/composer/vendor/bin/"
export PATH="$PATH:$PHP_COMPOSER_BIN"

NVIM_MASION_BIN="$HOME/.local/share/nvim/mason/bin/"
export PATH="$PATH:$NVIM_MASION_BIN"

# Rust
. "$HOME/.cargo/env"

# Elixir
. "$HOME/.asdf/asdf.sh"
. "$HOME/.asdf/completions/asdf.bash"
export ERL_AFLAGS="-kernel shell_history enabled"


# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac
