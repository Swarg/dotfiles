# Darkai

# install in xfce4
```bash
lxappearance
```
or
```
xfconf-query -c xsettings -p /Net/ThemeName -s "Darkai"
```

The prev way to install my own changed css-file to default Adwaita-dark:
Apply Adwaita-dark theme and change file in path:
```
/usr/share/themes/Adwaita-dark/gtk-3.0/gtk.css
```
from
```
@import url("resource:///org/gtk/libgtk/theme/Adwaita/gtk-contained-dark.css");
```
to
```
@import url("file:///home/YOURNAME/.local/share/themes/Darkai/gtk-3.0/gtk.css");
```

https://wiki.xfce.org/howto/xfwm4_theme
https://docs.xfce.org/xfce/xfconf/xfconf-query
