#!/bin/bash
# Goals:
#   - fast switching between a miltiple nginx site configuration
#   - easy management of nginx configs and sites
#       - fix permissions for directories
#       - install config
#       - local deployment of site from specified dir into your www-path
#       - fast change site_root in specified(current) config file

EDITOR=vim
AVAILABLE="/etc/nginx/sites-available"
ENABLED="/etc/nginx/sites-enabled"
LOGDIR="/var/log/nginx/"

WS_USER="${WS_USER:=www-data}"
WS_PERMS="${WS_PERMS:=744}"
# for directory recomended use 755, for files 644. Usage Example:
# find . -type d -exec chmod 755 {} \;
# find . -type f -exec chmod 644 {} \;

# For local deployment with command 'deploy'
WWW="${WWW:=/var/www/html}"
SUB_DIR="${SUB_DIR:=}"
SITE_CONFIG="${SITE_CONFIG:=}"

VARS="EDITOR, AVAILABLE, ENABLED, LOGDIR, WS_USER, WS_PERMS, WWW, SUB_DIR, SITE_CONFIG"

USAGE_FOR_CONFNAME="on|off|cat|edit|path|grep|bak|set-site-root"

if [ "$1" == "help" -o "$1" == "--help" -o "$1" == "" ]; then
  CMD=$(basename "$0")
  echo "$CMD -- tool for easy management of nginx configs and sites"
  echo "$CMD reload                  -- nginx reload configs"
  echo "$CMD fix-perms <path/to/dir> -- set owner and permissions for webserv"
  echo "$CMD install <new-config>    -- copy a <new-config> file to $AVAILABLE"
  echo "$CMD deploy <copied-site>    -- local deployment See $CMD deploy --help"
  echo "$CMD ls                      -- show enabled and available site-configs"
  echo ""
  echo "Actions with specified site-config (by name or by index from 1):"
  echo "<sc> - is site-config base filename at $ENABLED,"
  echo "  you can also refer to the site-config not by the name"
  echo "  but by a number in the list of enabled configs ($CMD ls)(Index from 1)"
  echo "$CMD <sc> on     -- create link from available to enabled"
  echo "$CMD <sc> off    -- remove link from enabled"
  echo "$CMD <sc> st     -- status. is exists in available and enabled"
  echo "$CMD <sc> cat    -- print all site-config into output"
  echo "$CMD <sc> edit   -- open config in editor (defaul is vim)"
  echo "$CMD <sc> grep   -- search with grep inside config-file"
  echo "$CMD <sc> path   -- show full path to config file"
  echo "$CMD <sc> bak    -- create backup copy of the config file"
  echo "$CMD <sc> set-site-root -- update only site_root in specified config file"
  exit 0
fi

if [ "$1" == "ls" -o "$1" == "st" ]; then
  echo "ls $AVAILABLE"
  ls $AVAILABLE/
  echo ""
  echo "ls $ENABLED"
  ls $ENABLED
  exit 0
fi

if [ "$1" == "reload" ]; then
    echo "sudo nginx -t && sudo nginx -s reload"
    sudo nginx -t && sudo nginx -s reload # check config  and reload
    exit 0
fi

if [ "$1" == "vars" ]; then
  echo $VARS_LIST
  exit 0
fi

# copy site config to /etc/nginx/sites-available
if [ "$1" == "install" -o "$1" == "i" ]; then
  if [ "$2" == "" ]; then
    echo "USAGE: install path/to/site-config"
    exit 0
  fi

  if [ ! -f "$2" ]; then
    echo "Not Found config '$2'"
    exit 1
  fi
  NAME=$(basename "$2")
  if [ "$NAME" == "" ]; then
    echo "ERROR: empty path"
    exit 1
  fi
  if [ -f "$AVAILABLE/$NAME" ]; then
    echo "[WARNING] Config File $NAME Already exists"
    read -p "Replace $AVAILABLE/$NAME by $2 ? (y/N): "
    if [ $REPLY != "y" ]; then
      echo "Installing the $NAME Canceled"
      exit 0
    fi
  fi
  sudo cp "$2" "$AVAILABLE/$NAME" && \
    echo "$NAME Installed to $AVAILABLE/$NAME" || echo "Error"
  exit 0
fi

function fix_perms() {
  # find . -type f -exec chmod 644 {} \;
  echo chown $WS_USER:$WS_USER -R "$1" '&&' chmod $WS_PERMS -R "$1"
  sudo chown $WS_USER:$WS_USER -R "$1" && sudo chmod $WS_PERMS -R "$1"
  if sudo [ -d "$1" ]; then
    echo sudo find "$1" -type d -exec chmod 755 {} \;
    sudo find "$1" -type d -exec chmod 755 {} \;
  fi
  sudo stat --format "Access: (%a/%A) [%U:%G] %n" "$1"
}

if [ "$1" == "fix-perms" -o "$1" == "fp" ]; then
  if sudo [ ! -f "$2" -a ! -d "$2" ]; then
    echo "Not Found: '$2'"
    exit 1
  fi
  fix_perms "$2"
  exit 0
fi

if [ "$1" == "show" -o "$1" == "s" ]; then
  shift
  CNT="$2"
  CNT="${CNT:=2}"
  if [ "$1" == "access.log" -o "$1" == "a" ]; then
    echo tail -n $CNT "$LOGDIR/access.log"
    sudo tail -n $CNT "$LOGDIR/access.log"
  elif [ "$1" == "error.log" -o "$1" == "e" ]; then
    echo tail -n $CNT "$LOGDIR/error.log"
    sudo tail -n $CNT "$LOGDIR/error.log"
  else
    echo "<e|error.log or a|access.log>"
    exit 1
  fi
  exit 0
fi

# change value in the nginx-config file
function change_site_root() {
  local SITE_CONFIG="$1"
  local DIR="$2"
  echo [Update Nginx-Config]: setup value for line: 'set $site_root <value>;'
  echo "set $site_root $DIR; into $SITE_CONFIG"
  sudo sed -i "s|^\s*set\s\$site_root.*|  set \$site_root $DIR;|" $SITE_CONFIG
}

# for local dev set value of $site_root variable
# $1 - path to config #2 - site root(path) by default use . cwd
function set_site_root() {
  SITE_CONFIG="$1"
  DIR="$2"
  if [ -z "$DIR" -o "$DIR" == '.' ]; then
    DIR=$(pwd)
  fi
  change_site_root "$SITE_CONFIG" "$DIR"
}


# Fast local deployment of the site from within the server
# See Help block
if [ "$1" == "deploy" -o "$1" == "d" ]; then
  shift
  if [ "$1" == "--help" -o "$1" == "-h" ]; then
    echo "Fast local deployment of your site. Just copy and setup your site"
    echo " What's will be done:"
    echo ' - copy directory with your site into WWW/SUB_DIR '
    echo ' - update value of $site_root in the specified nginx-site-config'
    echo '   (the nginx-site-config must be already exists. Use cmd:install)'
    # TODO check and copy nginx-site-config from deployment site into available
    echo ' - auto link site-config file into enabled (if needed)'
    echo "Usage Example:"
    echo "  WWW=/var/www/html SUB_DIR= SITE_CONFIG=my-site $CMD deploy ."
    echo "Here:"
    echo "  WWW          - Destination path. Where we deploy"
    echo '  SUB_DIR      - for specify sub destination dir: WWW/SUBDIR'
    echo '                 (may be empty)'
    echo "  SITE_CONFIG  - nginx config file name at $AVAILABLE "
    echo "                 can be just base filename or full path to conf file"
    echo "  .            - path to the directory with your site"
    exit 0
  fi
  [ ! -d "$WWW" ] && echo "directory WWW='$WWW' is not exists" && exit 1
  [ -z "$SITE_CONFIG" ] && echo "SITE_CONFIG Not Defined!" && exit 1
  [ ! -f "$SITE_CONFIG" ] && echo "Not found file SITE_CONFIG=$SITE_CONFIG" && exit 1

  DIR="$WWW"
  if [ "$SUB_DIR" != "" -a "$SUB_DIR" != "/" ]; then
    DIR="$DIR/$SUB_DIR"
  fi
  if [ ! -d "$DIR" ]; then
    echo "Create Directory: $DIR ..."
    if [ -d "$WWW" ]; then
      echo mkdir -p "$DIR" '&&' fix_perms "$DIR"
      sudo mkdir -p "$DIR" && fix_perms "$DIR"
    fi
    if sudo [ ! -d "$DIR" ]; then
      echo "Not Found Site Directory '$DIR'"
      sudo stat $WWW
      CMD=$(basename "$0")
      echo "You can specify it via WWW=/var/www/my-path SUB_DIR=sub $CMD .."
      exit 1
    fi
  fi
  echo "Deploy into $DIR"
  LDIR="$1"
  if [ "$LDIR" == "." ]; then
    LDIR="$(pwd)"
  fi
  if [ -d "$LDIR" ]; then
    echo cp -r "$LDIR/*" "$DIR" '&&' fix_perms "$DIR"
    sudo cp -r "$LDIR/"* "$DIR" && fix_perms "$DIR"
  else
    echo "Not Found Directory '$LDIR'"
  fi
  ENABLED_CONFIG=""
  # get full path to config file
  local SITE_CONFIG_NAME=$SITE_CONFIG_NAME
  if [[ "$SITE_CONFIG" != *"/"* ]]; then
    ENABLED_CONFIG="$ENABLED/$SITE_CONFIG"
    SITE_CONFIG="$AVAILABLE/$SITE_CONFIG"
  fi
  echo "Ngix-Site-Config: $SITE_CONFIG"
  if [ ! -f "$SITE_CONFIG" ]; then
    # TODO copy config from given place or generate in interact mode
    echo "Not Found ConfigFile: '$SITE_CONFIG'"
    echo "use the command '$CMD install $SITE_CONFIG_NAME' before deploy"
    exit 1
  fi
  # Update site_root at nginx config
  change_site_root "$SITE_CONFIG" "$DIR"
  if [ ! -f "$ENABLED_CONFIG" ]; then
    echo "[WARN] $SITE_CONFIG Not linked into $ENABLED"
    # Interactive Ask for link
    read -p "Add this site into Enabled (y/N)?: "
    if [ "$REPLY" == "y" -o "$REPLY" == "Y" ]; then
      echo ln -s $SITE_CONFIG $ENABLED_CONFIG
      sudo ln -s $SITE_CONFIG $ENABLED_CONFIG
    fi
  fi
  echo "sudo nginx -t && sudo nginx -s reload"
  sudo nginx -t && sudo nginx -s reload # check config  and reload
  exit 0
fi

# $1 fullpath to config
# $2 lastModified time before edit
# $3 Optional flags [-u|--update] or [-q|--no-ask]
function update_config_if_needed() {
  local CONF="$1"  # "$AVAILABLE/$F"
  local LM_OLD="$2"
  local LM_NOW=$(sudo stat --format "%Z" "$CONF")
  if [ -f "$CONF" -a "$LM_OLD" != "$LM_NOW" ]; then
    if [ "$3" == "--update" -o "$3" == "-u" ]; then
      sudo nginx -t && sudo nginx -s reload
    elif [ "$3" != "--no-ask" -a "$3" != "-q" ]; then
      read -p "Reload config changes by Nginx (y/N)?: "
      if [ "$REPLY" == "y" -o "$REPLY" == "Y" ]; then
        sudo nginx -t && sudo nginx -s reload
      fi
    fi
  fi
}


# Config File Name
F="$1"

# pick config file name by index (starts from 1 not from 0)
if [ -n "$F" ] && [ "$F" -eq "$F" ] 2>/dev/null; then # is_number
  LIST=($(ls $ENABLED/))
  INDEX=$1
  let INDEX="$INDEX-1"
  F="${LIST[$INDEX]}"
  echo $F
fi

if [ "$2" == "st" ]; then
  echo "available:" `[ -f "$AVAILABLE/$F" ] && echo "true" || echo "false"`
  echo "enabled:  " `[ -f "$ENABLED/$F" ] && echo "true" || echo "false"`
  exit 0
fi

if [ ! -f "$AVAILABLE/$F" ]; then
  echo "Not Found $AVAILABLE/$F"
  exit 1
fi

if [ "$2" == "on" ]; then
  if [ -f "$ENABLED/$F" ]; then
    echo "Already exists $ENABLED/$F"
    exit 1
  fi
  echo ln -s $AVAILABLE/$F $ENABLED/$F
  sudo ln -s $AVAILABLE/$F $ENABLED/$F

elif [ "$2" == "off" ]; then
  if [ ! -f "$ENABLED/$F" ]; then
    echo "Not Found $ENABLED/$F"
    exit 1
  fi
  echo unlink $ENABLED/$F
  sudo unlink $ENABLED/$F

elif [ "$2" == "path" -o "$2" == "p" ]; then
  echo $AVAILABLE/$F

elif [ "$2" == "less" ]; then
  echo "$AVAILABLE/$F"
  sudo less "$AVAILABLE/$F"

elif [ "$2" == "cat" ]; then
  echo "$AVAILABLE/$F"
  sudo cat "$AVAILABLE/$F"

elif [ "$2" == "grep" -o "$2" == "g" ]; then
  echo "$AVAILABLE/$F"
  sudo grep "$3" "$AVAILABLE/$F"

elif [ "$2" == "set-site-root" -o "$2" == "ssr" ]; then
  LM_OLD=$(sudo stat --format "%Z" "$AVAILABLE/$F")
  set_site_root "$AVAILABLE/$F" "$3"
  [[ "$3" != "-"* ]] && shift # if directory passed into set_site_root
  update_config_if_needed "$AVAILABLE/$F" "$LM_OLD" "$3"

elif [ "$2" == "edit" -o "$2" == "e" ]; then
  LM_OLD=$(sudo stat --format "%Z" "$AVAILABLE/$F")
  sudo $EDITOR "$AVAILABLE/$F"
  update_config_if_needed "$AVAILABLE/$F" $LM_OLD "$3"

# Create config backup
elif [ "$2" == "bak" ]; then
  if [ -f "$AVAILABLE/$F.bak" ]; then
    read -p "Backup '$AVAILABLE/$F.bak' Already Exists! Replace (y/N)?: "
    if [ "$REPLY" != "y" -a "$REPLY" != "Y" ]; then
      echo "Canceled"
      exit 0
    fi
    echo "Replace old backup..."
  fi
  echo cp "$AVAILABLE/$F" "$AVAILABLE/$F.bak"
  sudo cp "$AVAILABLE/$F" "$AVAILABLE/$F.bak"
  ls -lh "$AVAILABLE/$F.bak"

else
  echo "Uknonw command [$2]"
  echo "Supported: $USAGE_FOR_CONFNAME"
  exit 1
fi

