#!/bin/bash

# source this_file
# source "$HOME/tools/atoolsbashlib.sh"
RAMDISK="/tmp/ramdisk"

function isDEBUG() {
  [ ! -z "$DEBUG" ];
  return "$?"
}

function echo0() {
  while [ "$1" != "" ]
  do
    #print f "%s " "$1"
    printf "%q " "$1"
    shift
  done
  printf "\n"
}

function run0() {
  echo0 "$@"
  "$@"
}

function debug_echo() {
  if [ "$DEBUG" == "-DEBUG" ]; then
    local _DEBUG_OUT_="[DEBUG] "
    while [ "$1" != "" ]
    do
      _DEBUG_OUT_="$_DEBUG_OUT_"$(printf "%s " "$1")
      #printf "%q " "$1"
      shift
    done
    echo $_DEBUG_OUT_ >&2
  fi
}

function is_number() {
  if [ -n "$1" ] && [ "$1" -eq "$1" ] 2>/dev/null
  then
    #number
    return 0
  else
    #not a number
    return 1;
  fi
}

#  CMD4BASH
function isHelpOrNoArgs() {
  if [ "$1" == "" -o "$1" == "help" -o "$1" == "h" -o "$1" == "?" ]; then
    # true
    return 0
  else
    return 1
  fi
}
# isArg actual expectedFullName expectedShortName
function isArg() {
  if [ "$1" == "$2"  -o  "$3" != "" -a "$1" == "$3" ]; then
    return 0 #true
  else
    return 1
  fi
}


# Example: How to use
# if [[ "no" == $(askYesOrNo "Are you sure?") ]]; then
#     echo "Skipped."
#     exit 0
# fi
function askYesOrNo() {
  if [ "$1" != "" ]; then
    read -p "$1 (y/N): "
    case $(echo $REPLY | tr '[A-Z]' '[a-z]') in
        y|yes) echo "yes" ;;
        *)     echo "no" ;;
    esac

    return 0
  fi
  return 1
}

function check_file_exists_fatal() {
  if [ "$1" == "" -o ! -f "$1" ]; then
    echo "Not Found $1"
    exit 1
  fi
}

# Delete only exists file not dir! With check and echo
function delete_file() {
  local FILE="$1"
  if [ -f "$FILE" -a "$FILE" != "/" -a "$FILE" != "" ]; then
    echo "rm $FILE"
    rm "$FILE"
  fi
}

function delete_dir() {
  local DIR="$1"
  if [ -d "$DIR" -a "$DIR" != "/" -a "$DIR" != "" ]; then
    echo "rm -r $DIR"
    rm -r "$DIR"
  fi
}


# Protection against accidental deletion of system directories
# Usage Example: Allow to delete only directories of specified User
# $1 directory
# $2 expected owner
# $3 message on error
function check_dir_owner() {
  local DIR="$1"
  local EXPECTED_OWNER="$2"
  local MSG="$3"
  if [ ! -d "$DIR" ]; then
    echo "Not Found Directory: '$DIR'"
    return 1 # erro
  fi
  if [ "$DIR" == "/" ]; then
    echo "Root FS"
    exit 1
  fi
  local OWNER=$(sudo stat --format '%U' "$DIR")
  if [ "$OWNER" != "$EXPECTED_OWNER" ]; then
    [ "$MSG" != "" ] && echo "$3 $DIR"
    echo "[ERROR] Expected Owner: '$EXPECTED_OWNER' Actual Owner: $OWNER"
    exit 1
  fi
  return 0
}

function get_file_owner() {
  if [ -f "$1"]; then
    echo $(sudo stat --format '%U' "$FILE")
  fi
}

# Insert Lines from one file into another and print result into StdOut
# Lines will be inserted after the line containing the specified anhor
# $1 - file with lines (part) to insertion into dst file -- what we insert
# $2 - dst file  where we insert
# $3 - anhor after which we insert lines from $1
function ins2file_after_anhor() {
  local ins_file="$1"
  local dst_file="$2"
  local ANHOR="$3"
  if [ -z "$ANHOR" ]; then
    echo "[ERROR] empty ANHOR" >> /dev/stderr
    return 1;
  fi
  if [ ! -f  "$ins_file" ]; then
    echo "[ERROR] Not Found InsFile: '$ins_file' " >> /dev/stderr
    return 2;
  fi
  if [ ! -f "$dst_file" ]; then
    echo "[ERROR] Not Found DstFile: '$ins_file' " >> /dev/stderr
    return 3;
  fi

  IFS=$'\n' read -d '' -r -a INS_ARRAY < "$ins_file"
  IFS=$'\n' read -d '' -r -a DST_ARRAY < "$dst_file"

  PREV_IFS=$IFS
  IFS=$'\n'
  for line in ${DST_ARRAY[@]}; do
    echo $line
    if [[ $line == *"$ANHOR"* ]]; then
      for s in ${INS_ARRAY[@]}; do
        echo $s
      done
    fi
  done
  IFS=$PREV_IFS
  return 0; # success
}

# Example how to Use func ins2file_after_anhor():
# if [ "$1" == "test" ]; then
#   echo "# Test insert"
#   INS=test/ins.txt
#   DST=test/dst.txt
#   ANHOR='<!--anhor-->'
#   # ins2file_after_anhor $INS $DST $ANHOR
#   FUNC=$(declare -f ins2file_after_anhor)
#   sudo bash -c "$FUNC; ins2file_after_anhor $INS $DST '$ANHOR'" > /tmp/1.txt
#   cat /tmp/1.txt
#   exit 0
# fi

function sec_to_hh_mm_ss() {
  if is_number "$1"; then
    ((h=${1}/3600))
    ((m=(${1}%3600)/60))
    ((s=${1}%60))
    printf "%02d:%02d:%02d\n" $h $m $s
  else
    echo "00:00:00"
  fi
}

# SECONDS=0 in start point
function get_spent_time() {
  DUR_SEC=$SECONDS
  DUR_MIN="";
  if [ $DUR_SEC -ge "60" ]
  then
    DUR_MIN=$(sec_to_hh_mm_ss "$DUR_SEC")
  fi
  SPENT_TIME="$DUR_MIN ($DUR_SEC s)"
}

function has_ramdisk() {
  if [ -d "$RAMDISK" -a "$RAMDISK" != "" ]; then
    local line="$(mount | grep $RAMDISK)"
    if [ "$line" != "" ] && [[ $line == *"tmpfs"* ]]; then
      return 0
    fi
  fi
  #false
  return 1
}
