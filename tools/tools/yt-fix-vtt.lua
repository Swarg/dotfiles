#!/bin/lua

-- Goal: Fix broken vtt-file with subtitles downloaded from yt
-- Dependencies:
--  luarocks install lua_cliargs
-- Usage:
-- yt-fix-vtt file.vtt      -- fix and print all to StdOut
-- yt-fix-vtt file.vtt -r   -- fix and save to input(source) file
-- yt-fix-vtt file.vtt --output new-file.vtt  -- fix and save to specified file

local cli = require "cliargs"

-- this is called when the flag -v or --version is set
local function print_version()
  print(cli.name .. ": version 1.0.1")
  os.exit(0)
end

cli:set_name("yt-fix-vtt.lua")
cli:flag("-v, --version", "prints the program's version and exits", print_version)
cli:option("-o, --output FILE", "save output to specified file")
cli:flag("--force", "replace already exists file", false)
cli:flag("-r, --rewrite", "rewrite parsed output back to input(source) file", false)
cli:splat("INPUT", "FileName of Subtitles", "")
cli:flag("--debug", "", false)
-- remove a text of one subtitle by specified time range and
-- shift back all subsequet subtitles started from specified time-range
--cli:option("-m, --move-up MOVE_UP", "move all subtitles up from specified time-range", nil)

-- Parses from _G['arg']
local args, err = cli:parse()

-- something wrong happened, we print the error and exit
if not args then
  print(err)
  os.exit(1)
end

--busted -e package.path = package.path .. ';./src/?.lua;./lua/?.lua' /d/Dev/2023/lua/fix-yt-subtitles/test/yt-fix-vtt_spec.lua
local M = {}

function M.file_exists(file)
  local f = io.open(file, "rb")
  if f then f:close() end
  return f ~= nil
end

-- Get all lines from a file, or empty it file not exist
---@param path string
---@return table {string} all lines from file
function M.read_lines(path)
  local lines = {}
  if M.file_exists(path) then
    for line in io.lines(path) do
      lines[#lines + 1] = line
    end
  end
  return lines
end

function M.get_time_line(s)
  local p = "^%d%d:%d%d:%d%d%.%d%d%d %-%-> %d%d:%d%d:%d%d%.%d%d%d*"
  return s and string.match(s, p)
end

---@return table { h = 0, m = 0, s = 0, n = 0 }
function M.new_time_point()
  -- 00:00:42.310
  -- hh:mm:ss.nnn
  return { h = 0, m = 0, s = 0, n = 0 }
end

function M.new_time_range()
  -- "00:00:42.310 --> 00:00:42.320"
  --  hh:mm:ss.nnn --> hh:mm:ss.nnn
  return { t1 = M.new_time_point(), t2 = M.new_time_point() }
end

---@param line string "00:00:42.310 --> 00:00:42.320"
function M.get_time_start_s(line)
  local p = "^(%d%d:%d%d:%d%d%.%d%d%d) %-%-> %d%d:%d%d:%d%d%.%d%d%d*"
  return line and string.match(line, p)
end

---@param line string "00:00:42.310 --> 00:00:42.320"
function M.get_time_end_s(line)
  local p = "^%d%d:%d%d:%d%d%.%d%d%d %-%-> (%d%d:%d%d:%d%d%.%d%d%d) *"
  return line and string.match(line, p)
end

---@param stime string "00:00:42.310 --> 00:00:42.320"
---@param tp table time-range
function M.parse_time_point(stime, tp)
  tp.h = tonumber(string.match(stime, "^(%d%d):%d%d:%d%d%.%d%d%d"))
  tp.m = tonumber(string.match(stime, "^%d%d:(%d%d):%d%d%.%d%d%d"))
  tp.s = tonumber(string.match(stime, "^%d%d:%d%d:(%d%d)%.%d%d%d"))
  tp.n = tonumber(string.match(stime, "^%d%d:%d%d:%d%d%.(%d%d%d)"))
  return tp
end

---@param t1 table { h = 0, m = 0, s = 0, n = 0 }
---@param t2 table { h = 0, m = 0, s = 0, n = 0 }
function M.get_time_diff(t1, t2)
  local n1 = t1.n + t1.s * 1000 + t1.m * 60000 + t1.h * 3600000
  local n2 = t2.n + t2.s * 1000 + t2.m * 60000 + t2.h * 3600000
  return n2 - n1
end

---@param lines table {string}
---@return string?
function M.get_big_line(lines)
  local state = 0
  if lines then
    for _, line in pairs(lines) do
      if state == 2 and #line > 1 then     -- not empty
        return line
      elseif state == 1 and #line < 2 then -- empty line not work!
        state = 2
      elseif state == 0 and M.get_time_line(line) ~= nil then
        state = 1
      end
    end
  end
end

---@param s string?
---@return table
function M.parse_big_line(s)
  local lines = {}
  if s then
    s = string.gsub(s, "  ", "\n")
    for line in string.gmatch(s, "([^\n]+)\n?") do
      lines[#lines + 1] = line
      -- print(s)--DEBUG
    end
  end
  return lines
end

-- Parse broken vtt with big_line with all subtitles in one time-range
-- and then distribute the splited lines of subtitles across time-ranges
---@param lines table - raw-lines readed from vtt-file
---@return table
function M.parse_big_line_and_substitute_subtitles(lines)
  local bl = M.get_big_line(lines)
  -- print('[DEBUG] bigline is') print(bl) os.exit(0)
  local subs = M.parse_big_line(bl)
  local head = true
  local n = 1
  local res = {}
  if lines then
    local t1 = M.new_time_point()
    local t2 = M.new_time_point()

    for _, line in pairs(lines) do
      local time = M.get_time_line(line)
      if time then
        head = false
        M.parse_time_point(M.get_time_start_s(line), t1)
        M.parse_time_point(M.get_time_end_s(line), t2)
        -- ignore too shotly time ranges
        local time_diff = M.get_time_diff(t1, t2)

        if time_diff > 500 or (time_diff > 100 and subs[n] and #subs[n] < 12) then
          res[#res + 1] = time
          if n <= #subs then
            res[#res + 1] = subs[n]
            n = n + 1
          end
          res[#res + 1] = ""
        else
          if args.debug then
            print('DEBUG time-diff: ' .. tostring(time_diff) .. ' ' .. line ..
              ' subs: ' .. tostring(subs[n]))
          end
        end
      elseif head then
        res[#res + 1] = line
      end
    end
  end
  return res
end

---@param filename string
---@return table?
local function parse(filename)
  -- print(filename) -- DEBUG
  if not M.file_exists(filename) then
    print("File Not Found " .. tostring(filename))
    return nil
  end
  local raw_lines = M.read_lines(filename)
  local res = M.parse_big_line_and_substitute_subtitles(raw_lines)
  return res
end


if args.debug then print('[DEBUG] parse ' .. tostring(args.INPUT)) end

local lines = parse(args.INPUT)
if not lines or lines == {} then
  os.exit(1)
end

local filename

if args.rewrite then
  filename = args.INPUT
elseif args.o and args.o ~= "" then
  filename = args.o
  if M.file_exists(args.o) and not args.force then
    print("Cannot rewrite already exists file '" .. tostring(args.o)
      .. "' Use --force")
    os.exit(2)
  end
end

-- save or print
if filename and filename ~= "" then
  -- save to file
  print('Save output to file: ' .. tostring(filename))
  local file, erro = io.open(filename, "w")
  if file then
    local text = ""
    for _, s in ipairs(lines) do
      text = text .. s .. '\n'
    end
    file:write(text)
    file:close()
    return true
  else
    error(erro)
  end
else
  -- print output to StdOut
  for _, s in ipairs(lines) do
    print(s)
  end
end

return M;
