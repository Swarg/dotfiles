#!/usr/bin/env python3

# File: mic2vosk.py
# Based on "$HOME/vosk/vosk-api/python/example/test_microphone.py"
# files with voice models stored at ~/.cache/vosk/
# Goal: Show transcription in multiple lines in history like mode

# prerequisites: as described in https://alphacephei.com/vosk/install and also
# python module `sounddevice` (simply run command `pip install sounddevice`)
# Example usage using Dutch (nl) recognition model: `python test_microphone.py -m nl`
# For more help run: `python test_microphone.py -h`

import argparse
import queue
import sys
import sounddevice as sd

import json
import curses
import time

from vosk import Model, KaldiRecognizer

q = queue.Queue()

def int_or_str(text):
    """Helper function for argument parsing."""
    try:
        return int(text)
    except ValueError:
        return text

pause=False

def callback(indata, frames, time, status):
    """This is called (from a separate thread) for each audio block."""
    if status:
        print(status, file=sys.stderr)
    if not pause:
        q.put(bytes(indata))


parser = argparse.ArgumentParser(add_help=False)
parser.add_argument(
    "-l", "--list-devices", action="store_true",
    help="show list of audio devices and exit")
args, remaining = parser.parse_known_args()
if args.list_devices:
    print(sd.query_devices())
    parser.exit(0)
parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter,
    parents=[parser])
parser.add_argument(
    "-f", "--filename", type=str, metavar="FILENAME",
    help="audio file to store recording to")
parser.add_argument(
    "-d", "--device", type=int_or_str,
    help="input device (numeric ID or substring)")
parser.add_argument(
    "-r", "--samplerate", type=int, help="sampling rate")
parser.add_argument(
    "-m", "--model", type=str, help="language model; e.g. en-us, fr, nl; default is en-us")
args = parser.parse_args(remaining)

try:

    if args.samplerate is None:
        device_info = sd.query_devices(args.device, "input")
        # soundfile expects an int, sounddevice provides a float:
        args.samplerate = int(device_info["default_samplerate"])

    if args.model is None:
        model = Model(lang="en-us")
    else:
        model = Model(lang=args.model)

    if args.filename:
        dump_fn = open(args.filename, "wb")
    else:
        dump_fn = None

    with sd.RawInputStream(samplerate=args.samplerate, blocksize = 4000,
            device=args.device, dtype="int16", channels=1, callback=callback):
        #print("#" * 80)
        #print("Press Ctrl+C to stop the recording")
        #print("#" * 80)

        rec = KaldiRecognizer(model, args.samplerate)
        # ----      ----
        # do not wait for input when calling getch
        stdscr = curses.initscr()
        stdscr.nodelay(1)
        curses.noecho()
        # Non-blocking or cbreak mode... do not wait for Enter key to be pressed.
        curses.cbreak()
        height,width = stdscr.getmaxyx()
        s0 = " " * width
        #num = min(height,width)
        #for x in range(num):
        #    stdscr.addch(x,x,'X')
        # ----      ----
        stdscr.addstr(height - 1, 0, "Press Ctrl+C or q to stop the recording")
        last_partial = ''
        prev_text1 = ''
        prev_text2 = ''
        prev_text3 = ''
        text=''
        while True:
            # get keyboard input, returns -1 if none available
            c = stdscr.getch()
            if c != -1:
                if c == 113:#q
                    break
                if c == 32:#space
                    pause = not pause
                    if pause == True:
                        stdscr.move(0,0)
                        stdscr.addstr(0, 0, s0)
                        stdscr.addstr(0, 0, "(Pause)")
                        stdscr.refresh()
                    else:
                        stdscr.move(0,0)
                        stdscr.addstr(0, 0, s0)
                        stdscr.refresh()

                #print numeric value
                stdscr.addstr(str(c) + ' ')
                stdscr.refresh()
                stdscr.move(0,0)
            if pause:
                time.sleep(1)
                continue

            data = q.get()
            if rec.AcceptWaveform(data):
                """print(rec.Result())"""
                res = json.loads(rec.Result())
                text=res["text"]
                #print(res["text"])
                stdscr.move(4,0)
                stdscr.addstr(4, 0, s0)
                stdscr.addstr(5, 0, s0)
                stdscr.addstr(6, 0, s0)
                stdscr.addstr(4, 0, text)

                stdscr.addstr(8, 0, s0)
                stdscr.addstr(9, 0, s0)
                stdscr.addstr(10, 0, s0)
                stdscr.addstr(11, 0, s0)
                stdscr.addstr(8, 0, prev_text1)

                stdscr.addstr(12, 0, s0)
                stdscr.addstr(13, 0, s0)
                stdscr.addstr(14, 0, s0)
                stdscr.addstr(12, 0, prev_text2)

                stdscr.addstr(15, 0, s0)
                stdscr.addstr(16, 0, s0)
                stdscr.addstr(17, 0, s0)
                stdscr.addstr(15, 0, prev_text3)

                stdscr.refresh()
                if not text == "":
                    prev_text3 = prev_text2
                    prev_text2 = prev_text1
                    prev_text1 = text
            else:
                #print(rec.PartialResult())
                res = json.loads(rec.PartialResult())
                partial = res["partial"]
                if partial and partial != last_partial:
                    stdscr.move(1,0)
                    stdscr.addstr(1, 0, s0)
                    stdscr.addstr(2, 0, s0)
                    stdscr.addstr(3, 0, s0)
                    stdscr.addstr(1, 0, partial)
                    stdscr.refresh()
                    #print("::"+partial)
                    last_partial=partial

            if dump_fn is not None:
                dump_fn.write(data)

except KeyboardInterrupt:
    print("\nDone")
    parser.exit(0)
except Exception as e:
    parser.exit(type(e).__name__ + ": " + str(e))
finally:
    curses.nocbreak()
    curses.echo()
    curses.endwin()
