-- 02-12-2024 @author Swarg
-- Goal: mute one audio track
--
-- Refs:
--  - https://mpv.io/manual/master/
--  - Real-time multi-track volume control via lua
--  - https://github.com/mpv-player/mpv/issues/12828
--
--  mp.module_paths, mp.get_script_file()

local mp = require 'mp'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

--
---@param sfmt string
local function ui_msgf(sfmt, ...)
  local msg = fmt(sfmt, ...)
  mp.osd_message(msg)
end

--
-- to emulate the audio track moving in the video timeline
--
---@param delay number
local function set_audio_delay(delay)
  mp.set_property("audio-delay", delay)
  mp.osd_message("set A-V delay: " .. delay .. " s", 2)
end


local callback = {}
---@param filename string
---@param offset string|number in secods
callback.add_audio_with_delay = function(_, filename, offset)
  local av_delay = tonumber(offset) --
  if not filename or not av_delay then
    ui_msgf('audio_tracks_add_audio: file:' .. v2s(filename) .. ' off:' .. v2s(offset))
    return
  end

  mp.commandv('audio-add', filename)
  set_audio_delay(av_delay)
  ui_msgf('audio-add: %s', filename)
end

--
-- distaptch my own subcommands incoming via ips
--
local function command_dispatcher(...)
  local cnt = select('#', ...)
  if cnt > 0 then
    local cmd = select(1, ...)
    local handler = callback[cmd]
    if type(handler) == 'function' then
      handler(...)
    else
      ui_msgf('unknown command: "%s"', v2s(cmd))
    end
  end
end

--[[
 Example of how to send custom event to trigger callback.add_audio_with_delay
  echo '{ "command": ["script-message", "script_audio_tracks",
            "add_audio_with_delay", "./audio.mp3", "4"] }' | \
    socat - /run/user/1000/mpvctl.0
]]
mp.register_script_message("script_audio_tracks", command_dispatcher)

