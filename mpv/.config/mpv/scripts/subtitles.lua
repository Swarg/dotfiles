-- 22-02-2024 @author Swarg
--
-- Goals:
--   - autoload subtitles from subs dir in the same directory as video file
--
-- Docs:
--   - https://github.com/mpv-player/mpv/blob/master/DOCS/man/lua.rst
--   - https://mpv.io/manual/master/#lua-scripting-utils
--

local mp = require 'mp'
local utils = require 'mp.utils'

local SUBS_DIR = 'subs'


-- just experimental to figure out how to bind own function to key and
-- see it in the UI of the mpv
local function hdl_show_sub_file_path()
  print("stdout: the key was pressed")
  mp.osd_message('ui: the key was pressed')

  local paths = mp.get_property_native("sub-file-paths");
  if type(paths) == 'table' then
    if not next(paths) then
      print('empty sub-file-paths')
    else
      for _, v in ipairs(paths) do
        print(v)
      end
    end
  end
end

-- bind own function to key
mp.add_key_binding("x", "show_sub_file_paths", hdl_show_sub_file_path)




local function get_vid_from_filename(fn)
  local vid = fn:sub(-11, -1)
  --._c32_ab48k
  if string.match(vid, "^._c%d%d_ab%d+k$") then
    vid = fn:sub(-21, -11)
  end
  -- print('##|' .. tostring(vid) .. '|##')
  return vid
end

local function find_and_add()
  local path = mp.get_property('path', '')
  local fn_no_ext = mp.get_property('filename/no-ext')
  local dir, fn = utils.split_path(path)
  local sd = utils.join_path(dir, SUBS_DIR)
  print(dir, fn, sd)
  local vid = get_vid_from_filename(fn_no_ext)
  if not vid then
    print('Cannot get yt-vid for ' .. tostring(fn))
    return
  end

  local fn0 = sd .. '/' .. vid .. '.vtt'
  local stat = utils.file_info(fn0)
  if stat and stat.is_file then
    print('found ', fn0)
    mp.commandv('sub-add', fn0, 'select')
  end
end

mp.register_event('file-loaded', find_and_add)



--[[ Snippets of how to interact with the mpv API available for lua

  local video_path = mp.get_property("path")
  local fn = mp.get_property("filename/no-ext") -- video file name

  mp.set_property_native("sub-file-paths", new_paths)
  mp.msg.info('msg:info Message for you!') -- stdout

  local stat = utils.file_info(fn)
  print(fn, utils.to_string(stat))  -- aka inspect:
  {"is_dir" = false, "mtime" = 1708617199, "is_file" = true, "size" = 38905,
    "mode" = 33276, "atime" = 1708617199, "ctime" = 1708617214}


  local list = utils.readdir(sd, 'files')

  sd = utils.join_path(sd, fn_no_ext)
  list = utils.readdir(sd, 'files')

  mp.commandv('sub-add', utils.join_path(sd, sub), 'select')


  sub_paths = mp.get_property_native('sub-file-paths')

  mp.add_hook('on_load', 10, function ()
      sub_paths[#sub_paths+1]='Subs/' .. mp.get_property('filename/no-ext')
      mp.set_property_native('sub-file-paths', sub_paths)
      sub_paths[#sub_paths]=nil
  end)

  local utils = require 'mp.utils'
  mp.add_periodic_timer(30, function()
      utils.subprocess({args={"xdg-screensaver", "reset"}})
  end)
]]

--[[ How to see all available function without docs:
mp.utils:
for func_name, val in pairs(utils) do print(func_name, type(val)) end :
  shared_script_property_set
  shared_script_property_get
  shared_script_property_observe
  subprocess_detached
  to_string
  format_json
  format_table
  parse_json
  format_bytes_humanized
  subprocess
  getpid
  getcwd
  readdir
  join_path
  split_path
  file_info
]]
