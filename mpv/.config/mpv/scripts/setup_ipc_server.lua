-- 24-05-2024 @author Swarg
-- Goals:
--  - Creates a socket for mpv instance by pressing specified keybinding.
--
-- The sockets are named "/tmp/mpvctl_" followed by a number, i.e. mpvctl_5.
-- The highest number is the most recent instance, the lowest is the oldest.
--
-- https://mpv.io/manual/master/#properties
-- https://github.com/mpv-player/mpv/blob/master/DOCS/man/ipc.rst
-- https://github.com/mpv-player/mpv/blob/master/DOCS/man/lua.rst
-- https://github.com/mpv-player/mpv/blob/master/DOCS/man/console.rst
-- https://github.com/mpv-player/mpv/blob/master/DOCS/man/input.rst#list-of-input-commands
--
-- For example, this request:
-- { "command": ["get_property", "time-pos"], "request_id": 100 }
--
-- Would generate this response:
-- { "error": "success", "data": 1.468135, "request_id": 100 }
--
-- Socat example

-- You can use the socat cli-tool to send commands (and receive replies)
-- from the shell:
--
--   mpv file.mkv --input-ipc-server=/tmp/mpvsocket
--
-- Then you can control it using socat:
--
--   echo '{ "command": ["get_property", "playback-time"] }' | \
--   socat - /tmp/mpvsocket
--
-- {"data":190.482000,"error":"success"}
--
-- interactive way:
-- socat - /tmp/mpvctl_0
--
-- Events
-- mpv will also send events to clients with JSON messages of the following form:

-- { "event": "event_name" }
-- See List of events
-- https://github.com/mpv-player/mpv/blob/master/DOCS/man/input.rst#list-of-events

local mp = require 'mp'

local SOCKET_NAME = 'mpvctl'
local UID = tostring(os.getenv('UID'))
local RUNTIME_DIR = os.getenv("XDG_RUNTIME_DIR") or ("/run/user/" .. UID)

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

--
---@param sfmt string
local function ui_msgf(sfmt, ...)
  local msg = fmt(sfmt, ...)
  mp.osd_message(msg)
end


---@param offset number?
local function next_free_socket_name(offset)
  local i = offset or 0
  local filename
  while true do
    filename = RUNTIME_DIR .. '/' .. SOCKET_NAME .. '.' .. i
    local file, _, err = io.open(filename)
    if file == nil and err ~= 6 then -- 6 = socket (maybe not portable?)
      break
    else
      if file then
        io.close(file)
      end
      i = i + 1
    end
  end
  return filename
end

-- setup ipc on next free socket
---@param offset number?
local function setup_ipc_server(offset)
  local ipc_server = mp.get_property("input-ipc-server")
  ui_msgf('ui: try to setup input-ipc-server prev:"%s"', v2s(ipc_server))

  if ipc_server == '' then
    local filename = next_free_socket_name(offset)

    mp.set_property("input-ipc-server", filename)
    mp.register_event("shutdown", function() os.remove(filename) end)

    ui_msgf('ipc-server listen socket: "%s"', v2s(filename))
  end
end

mp.add_key_binding("ctrl+alt+i", "setup_ipc_server", setup_ipc_server)
