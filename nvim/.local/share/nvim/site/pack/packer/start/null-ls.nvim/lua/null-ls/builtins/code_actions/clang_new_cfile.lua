local h = require("null-ls.helpers")
local methods = require("null-ls.methods")
local log = require("null-ls.logger")
local u = require("null-ls.utils")
local utils = u.make_conditional_utils()

local CODE_ACTION = methods.internal.CODE_ACTION

-- Generate *.c *.h empty files for given name
-- 12.04:22 hard-impl: 6:30-9:18

-- local null_ls = require("null-ls")
return h.make_builtin({
  name = "clang-genfile",
  method = CODE_ACTION,
  filetypes = { "c", "h" },
  meta = {
    description = "Injects actions to generate c and h files.",
    notes = {
    },
    usage = "local sources = { null_ls.builtins.code_actions.clang_new_cfile}",
  },
  generator = {
    fn = function(context)
      --print(vim.inspect(context))
      return {
        {
          title = "Generate new c-file with header-file",
          action = function()
            local create_new_file = function(name, body)
              if not utils.has_file({ name }) then
                local file = io.open(name, 'w')
                if file then
                  file:write(body)
                  file:close()
                  vim.cmd(":e " .. name)
                  return true
                end
              else
                vim.notify("Already exists " .. name, vim.log.levels.INFO)
              end
              return false
            end
            local gen_files = function(dir, name)
              -- current file path
              local cfile_path = dir .. '/' .. name .. '.c' --vim.loop.fs_realpath(name .. '.c')
              local hfile_path = dir .. '/' .. name .. '.h'
              return
                  create_new_file(hfile_path,
                    '#ifndef ' .. string.upper(name) .. '_H_SENTRY\n' ..
                    '#define ' .. string.upper(name) .. '_H_SENTRY\n' ..
                    '\n\n\n' ..
                    '#endif\n'
                  ) and
                  create_new_file(cfile_path,
                    '#include "' .. name .. '.h"\n\n\n'
                  )
            end
            vim.ui.input({ prompt = "Enter c-file name: " }, function(name)
              if not name then
                return
              end
              if name == "" then
                log:error("File name cannot be empty")
                return
              end
              -- 0 for buffer id means "the current buffer"
              local dir = vim.fs.dirname(vim.api.nvim_buf_get_name(0))
              --local dir = vim.fs.dirname(context.lsp_params.textDocument.uri)
              if gen_files(dir, name) then
                vim.notify("Created a new files " .. name .. ".[c|h] at " .. dir, vim.log.levels.INFO)
              end
            end)
          end
        }
      }
    end
  },
})

-- References:
-- /home/swarg/.local/share/nvim/site/pack/packer/start/null-ls.nvim/lua/null-ls/builtins/code_actions/impl.lua
-- local lines = { frozen_string_literal_comment, "", first_line }
-- /home/swarg/.local/share/nvim/site/pack/packer/start/null-ls.nvim/lua/null-ls/utils/init.lua
-- /usr/share/nvim/runtime/lua/vim/lsp/rpc.lua    fd: write tcp connect
-- /usr/share/nvim/runtime/lua/vim/lsp/util.lua
--
--  create new file and write json to it
-- /home/swarg/.local/share/nvim/site/pack/packer/start/null-ls.nvim/lua/null-ls/builtins/code_actions/cspell.lua
-- vim.api.nvim_buf_set_lines(context.bufnr, 0, 1, false, lines)
-- output = output .. vim.fn.system(cmd_args2)
-- if vim.v.shell_error ~= 0 then
--   log:error(output)
--   return
-- end
-- return output
