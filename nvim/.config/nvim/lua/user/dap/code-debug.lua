-- Configuration for Debugging C/C++ via vscode extension Native Debugging
-- WebFreak001/code-debug

local dap_status_ok, dap = pcall(require, "dap")
if not dap_status_ok then
  return
end

-- C/C++ Configuration >>
local mason_path = vim.fn.glob(vim.fn.stdpath("data")) .. "/mason/"
local code_debug_path = mason_path .. "packages/code-debug/" --"extension/"

local find_pid = function(name)
  local s = vim.fn.system("ps -C prog -o pid h");
  local n = tonumber(s);
  print( n )
end


dap.adapters.gdb = {
  type = "executable",
  command = code_debug_path .. "code_debug.sh", --"node",
  args = { "gdb" }, --./out/src/gdb.js"},
  cwd = code_debug_path
}

dap.configurations.c = {
  --setmetatable(
  {
    name = "GDB: Launch File",
    type = "gdb", -- Must match name of DAP adapter defined above
    request = "launch",
    cwd = "${workspaceFolder}",
    target = -- (in vscode-cpptools it - program)
    function()
      return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
    end,
    -- setupCommands = {..} -- specific for ms vscode-cpptools
    -- Native Debug has its own version of 'setupCommands' called autorun
    -- (which is an array of commands to run after startup before execution)
    valuesFormatting = "prettyPrinters", -- "parseText", "disabled"
    -- gdbpath instead miDebuggerPath (for vscode-cpptools)
    -- Check the package.json for the full list of available options
    --
    -- Launch over SSH
    -- ssh = {
    --   host = "<remote host>",
    --   user = "<user name>",
    --   cwd = "<cwd on remote machine>",
    --   keyfile = "<location of keyfile>",
    --   forwardX11 = false
    -- }
  },

  -- the program would have to be compiled with -g)
  -- $ ps -C prog -o pid h
  -- pid
  -- $ gdb prog -p pid
  -- (gdb) shell ps -C prog -o pid h
  -- (gdb) attach pid
  {
    name = 'Attach to Already Running Process',
    type = "gdb",
    request = 'attach',
    cwd = '${workspaceFolder}',
    executable = function() -- executable with debug symbols for debugger
      return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
    end,
    target = function()  --vscode-cpptools use here'program'
      -- TODO how get name from executable function?
      local pname = vim.fn.input('Process Name: ', 'prog')
      return vim.fn.system("ps -C " .. pname ..  " -o pid h")
    end,
    valuesFormatting = "prettyPrinters", -- "parseText", "disabled"
    remote = false,
    -- or remote = true, target = "localhost:2345"
    -- autorun: ["cmd1", "cmd2" ]
    -- stopAtConnect = true,
    stopAtEntry = true,

    -- Debugging: NativeDebagginClient <-(DAP)-> GDB
    -- printCalls = true,
    -- showDevDebugOutput = true,
  }
  --)
}

dap.configurations.cpp = dap.configurations.c
dap.configurations.rust = dap.configurations.c

-- This file contains all available fields-params of config and his descriptions
-- ~/.local/share/nvim/mason/packages/code-debug/extension/package.json

-- Defining configuration using launch.json file
-- require('dap.ext.vscode').load_launchjs(nil, {})
-- The first parameter is the path where you store the file, by default is
-- .vscode/launch.json, but you can change it to whatever you want
--
-- issue: Failed to get Stack Trace: Selected thread is running.



