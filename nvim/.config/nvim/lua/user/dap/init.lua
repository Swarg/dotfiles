local dap_status_ok, dap = pcall(require, "dap")
if not dap_status_ok then
  return
end

-- packages installed manualy (for lua, jdtls?)
local locshare = os.getenv("HOME") .. '/.local/share/'
local vscode_extensions = os.getenv("HOME") .. '/.vscode/extensions/'

-- packages installed by :Mason
local mpackages = vim.fn.stdpath("data") .. '/mason/packages/'

-- Lua
dap.adapters['local-lua'] = {
  type = 'executable',
  -- command = "node",
  command = "lua",
  args = {
    -- /home/swarg/.local/share/local-lua-debugger-vscode/extension/debugAdapter.ts
    -- locshare .."local-lua-debugger-vscode/extension/debugAdapter.ts"
    vscode_extensions .. "tomblind.local-lua-debugger-vscode-0.3.3/extension/debugAdapter.lua"
  },
  enrich_config = function(config, on_config)
    if not config["extensionPath"] then
      local c = vim.deepcopy(config)
      --  If this is missing or wrong you'll see
      -- "module 'lldebugger' not found" errors in the dap-repl when trying to launch a debug session
      c.extensionPath = --"/absolute/path/to/local-lua-debugger-vscode/"
      vscode_extensions .. "tomblind.local-lua-debugger-vscode-0.3.3/"
      on_config(c)
    else
      on_config(config)
    end
  end,
}

-- see also :help dap-launch.json
-- https://zignar.net/2023/06/10/debugging-lua-in-neovim/
dap.configurations.lua = {
  {
    type = 'local-lua',
    name = 'Current file (local-lua-dbg, lua)',
    request = 'launch',
    cwd = '${workspaceFolder}',
    program = {
      lua = 'lua5.1',
      file = '${file}',
    },
    args = {},
  }
}

-- Bash
dap.adapters.bashdb = {
  type = 'executable';
  name = 'bashdb';
  command = mpackages .. 'bash-debug-adapter/bash-debug-adapter';
}
dap.configurations.sh = {
  {
    type = 'bashdb',
    request = 'launch',
    name = "Launch file",
    showDebugOutput = true;
    pathBashdb = mpackages .. 'bash-debug-adapter/extension/bashdb_dir/bashdb',
    pathBashdbLib = mpackages .. 'bash-debug-adapter/extension/bashdb_dir',
    trace = true,
    file = "${file}",
    program = "${file}",
    cwd = '${workspaceFolder}',
    pathCat = "cat",
    pathBash = "/bin/bash",
    pathMkfifo = "mkfifo",
    pathPkill = "pkill",
    args = {},
    env = {},
    terminalKind = "integrated",
  }
}

-- JAVA
dap.configurations.java = {
  {
    type = 'java';
    request = 'attach';
    name = "Debug (Attach) - localhost:5005";
    hostName = "127.0.0.1";
    port = 5005;
  },
}

local dap_install_status_ok, dap_install = pcall(require, "dap-install")
if dap_install_status_ok then
  dap_install.setup {}

  dap_install.config("python", {})
  -- add other configs here
end

-- C/C++/Rust
-- setup for Debugging C/C++ via vscode extension "Native Debugging"
local _, _ = pcall(require, "user.dap.code-debug")
-- experimental: VSCode extension for C/C++ with telemetry
--local _, _ = pcall(require, "user.dap.vscode-ccptools")


-- PHP
-- setup for Debugging PHP via vscode-php-debug (php-debug-adapter)
-- nvim + nvim-dap + vscode-php-debug + xdebug
local _, _ = pcall(require, "user.dap.vscode-php-debug")

-- works
-- Lua
-- TODO https://github.com/mfussenegger/nvim-dap/wiki/Debug-Adapter-installation#lua


local dap_vt_status_ok, dap_vt = pcall(require, 'nvim-dap-virtual-text')
if dap_vt_status_ok then
  dap_vt.setup()
end

local dap_ui_status_ok, dapui = pcall(require, "dapui")
if not dap_ui_status_ok then
  return
end

dapui.setup {
  expand_lines = true,
  icons = { expanded = "", collapsed = "", circular = "" },
  mappings = {
    -- Use a table to apply multiple mappings
    expand = { "<CR>", "<2-LeftMouse>" },
    open = "o",
    remove = "d",
    edit = "e",
    repl = "r",
    toggle = "t",
  },
  element_mappings = {
    stacks = {
      open = {"o", "<CR>", "o", "<2-LeftMouse>"},
      -- expand = "o",
    }
  },
  layouts = {
    {
      elements = {
        { id = "scopes", size = 0.33 },
        { id = "breakpoints", size = 0.17 },
        { id = "stacks", size = 0.25 },
        { id = "watches", size = 0.25 },
      },
      size = 0.33,
      position = "right",
    },
    {
      elements = {
        { id = "repl", size = 0.45 },
        { id = "console", size = 0.55 },
      },
      size = 0.27,
      position = "bottom",
    },
  },
  floating = {
    max_height = 0.9,
    max_width = 0.5, -- Floats will be treated as percentage of your screen.
    border = vim.g.border_chars, -- Border style. Can be 'single', 'double' or 'rounded'
    mappings = {
      close = { "q", "<Esc>" },
    },
  },
}

vim.fn.sign_define("DapBreakpoint", { text = "", texthl = "DiagnosticSignError", linehl = "", numhl = "" })

-- dap.listeners.after.event_initialized["dapui_config"] = function()
--   dapui.open()
-- end
-- dap.listeners.before.event_terminated["dapui_config"] = function()
--   dapui.close()
-- end
-- dap.listeners.before.event_exited["dapui_config"] = function()
--   dapui.close()
-- end

-- Open dapui in separated new tab(new Buffer) instead to splits in the current tab
-- and autoclose dapui tab on Stop Debugging
-- https://github.com/rcarriga/nvim-dap-ui/issues/122#issuecomment-1206389311
local debug_win = nil
local debug_tab = nil
local debug_tabnr = nil

local function open_in_tab()
  if debug_win and vim.api.nvim_win_is_valid(debug_win) then
    vim.api.nvim_set_current_win(debug_win)
    return
  end

  vim.cmd('tabedit %')
  debug_win = vim.fn.win_getid()
  debug_tab = vim.api.nvim_win_get_tabpage(debug_win)
  debug_tabnr = vim.api.nvim_tabpage_get_number(debug_tab)

  dapui.open()
end

local function close_tab()
  dapui.close()

  if debug_tab and vim.api.nvim_tabpage_is_valid(debug_tab) then
    vim.api.nvim_exec('tabclose ' .. debug_tabnr, false)
  end

  debug_win = nil
  debug_tab = nil
  debug_tabnr = nil
end

-- Attach DAP UI to DAP events
dap.listeners.after.event_initialized['dapui_config'] = function()
  open_in_tab()
end
dap.listeners.before.event_terminated['dapui_config'] = function()
  close_tab()
end
dap.listeners.before.event_exited['dapui_config'] = function()
  close_tab()
end
dap.listeners.before.disconnect["dapui_config"] = function()
  close_tab()
end
