-- Alternative Configuration for Debugging via ms vscode-cpptools extension
-- [WARN] vscode-cpptools collect telemetry

local dap_status_ok, dap = pcall(require, "dap")
if not dap_status_ok then
  return
end

-- C/C++ Configuration >>
local mason_path = vim.fn.glob(vim.fn.stdpath("data")) .. "/mason/"
-- https://code.visualstudio.com/docs/cpp/launch-json-reference
local cpptools_debugAdapter_path = mason_path ..
  "packages/cpptools/extension/debugAdapters/bin/OpenDebugAD7"

dap.adapters.cppdbg = {
  id = 'cppdbg',
  type = 'executable',
  command = cpptools_debugAdapter_path,
}
dap.configurations.cpp = {
  {
    name = "Launch file",
    type = 'cppdbg', -- using GDB or LLDB Linux
    --type = 'cppvsdbg' -- using Visual Studio Windows debugger
    request = "launch",
    program = function()
      return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
    end,
    --symbolSearchPath = "path/to/Symbols:p"
    cwd = '${workspaceFolder}',
    stopAtEntry = true,
    setupCommands = {
      {
         text = '-enable-pretty-printing',
         description =  'enable pretty printing',
         ignoreFailures = false
      },
    },
  },
  {
    name = 'Attach to gdbserver :1234',
    type = "cppdbg",
    request = 'launch',
    MIMode = 'gdb',
    miDebuggerServerAddress = 'localhost:1234',
    miDebuggerPath = '/usr/bin/gdb',
    cwd = '${workspaceFolder}',
    program = function()
      return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
    end,
    setupCommands = {
      {
         text = '-enable-pretty-printing',
         description =  'enable pretty printing',
         ignoreFailures = false
      },
    },
  },
}
dap.configurations.c = dap.configurations.cpp
dap.configurations.rust = dap.configurations.cpp
