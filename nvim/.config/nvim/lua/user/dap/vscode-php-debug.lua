-- Configuration for Debugging PHP via vscode-php-debug
-- nvim + nvim-dap + vscode-php-debug + xdebug
-- Refs:
-- https://github.com/xdebug/vscode-php-debug
-- https://github.com/mfussenegger/nvim-dap/wiki/Debug-Adapter-installation#PHP
--
-- https://xdebug.org/docs/step_debug#activate_debugger
-- https://xdebug.org/docs/all_settings#start_with_request
--
-- installed via :Mason DAP(3) php-debug-adapter  - the method used here
--
-- Second installation method is:
--  Install vscode-php-debug manually directly from repo:
--    git clone https://github.com/xdebug/vscode-php-debug.git
--    cd vscode-php-debug
--    npm install && npm run build
--
--    os.getenv("HOME") .. "/build/vscode-php-debug/out/phpDebug.js"
--

local dap_status_ok, dap = pcall(require, "dap")
if not dap_status_ok then
  return
end
local mason_path         = vim.fn.glob(vim.fn.stdpath("data")) .. "/mason/"
local php_dap_path       = mason_path .. "packages/php-debug-adapter/"
-- ~/.local/share/nvim/mason/packages/php-debug-adapter/

dap.adapters.php         = {
  type = 'executable',
  command = 'node',
  args = { php_dap_path .. '/extension/out/phpDebug.js' }
}

local xdebug_port        = os.getenv('NVIM_XDEBUG_PORT') or 9003
local xdebug_path_server = os.getenv('NVIM_XDEBUG_PATH_SERVER') or '/app/'
local xdebug_path_local  = os.getenv('NVIM_XDEBUG_PATH_LOCAL') or vim.fn.getcwd()

-- print('NVIM_XDEBUG_PATH_LOCAL:', xdebug_path_local)

dap.configurations.php   = {
  {
    name = 'Start Listening for Xdebug', -- php-fpm
    type = 'php',
    request = 'launch',
    port = xdebug_port,

    localSourceRoot = xdebug_path_local,

    pathMappings = xdebug_path_local and xdebug_path_server
        --       in container     -> in local machine (project with sources)
        and { [xdebug_path_server] = xdebug_path_local, } or nil,
    -- pathMappings = {
    --   -- ['/app/'] = "${workspaceFolder}",
    -- }
  },
  {
    name = 'Run current script in console', -- php-cli
    type = 'php',
    request = 'launch',
    port = 9003,
    cwd = "${fileDirname}",
    program = "${file}",
    runtimeExecutable = "php",
    externalConsole = false,
  },
  -- {
  --   name = 'Launch built-in server and debug',
  --   type = 'php',
  --   request = 'launch',
  --   runtimeArgs = {
  --     "-S", "localhost:8000", "-t", "."
  --   },
  --   port = 9000,
  --   serverReadyAction = {
  --     action = "openExternally",
  --   },
  -- },
}

-- TODO learn how to use the command DapLoadLaunchJSON + .vscode/launch.json


-- https://github.com/nanotee/nvim-lua-guide#learning-lua
