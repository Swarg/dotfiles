local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  }
  print "Installing packer close and reopen Neovim..."
  vim.cmd [[packadd packer.nvim]]
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
-- vim.cmd [[
--   augroup packer_user_config
--     autocmd!
--     autocmd BufWritePost plugins.lua source <afile> | PackerSync
--   augroup end
-- ]]

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end

-- Have packer use a popup window
packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float { border = "rounded" }
    end,
  },
  git = {
    clone_timeout = 300, -- Timeout, in seconds, for git clones
  },
}

-- Install your plugins here
return packer.startup(function(use)
  -- My plugins here
  use { "wbthomason/packer.nvim" } -- Have packer manage itself
  use { "nvim-lua/plenary.nvim" }  -- Useful lua functions used by lots of plugins
  --Opti use { "windwp/nvim-autopairs" } -- Autopairs, integrates with both cmp and treesitter
  use { "numToStr/Comment.nvim" }
  --Opti use { "JoosepAlviste/nvim-ts-context-commentstring" } -- advansed for numToStr/Comment.nvim
  use { "nvim-tree/nvim-web-devicons" }
  use { 'nvim-tree/nvim-tree.lua', requires = { 'nvim-tree/nvim-web-devicons', }, tag = 'nightly' }
  use { "akinsho/bufferline.nvim" }
  -- Delete buffers and close files in Vim without closing your windows or messing up your layout.
  use { "moll/vim-bbye" }
  use { "nvim-lualine/lualine.nvim" }
  use { "akinsho/toggleterm.nvim" }
  use { "ahmedkhalf/project.nvim" }
  -- Speed up loading Lua modules in Neovim to improve startup time
  use { "lewis6991/impatient.nvim" }
  use { "lukas-reineke/indent-blankline.nvim" }
  use { "goolord/alpha-nvim" }
  use { 'phaazon/hop.nvim', branch = 'v2' }
  use { "norcalli/nvim-colorizer.lua" }
  use { "folke/which-key.nvim" }
  use { "simrat39/symbols-outline.nvim" }

  -- Colorschemes
  use { "svvarg/darkai.nvim" }
  -- use 'tanvirtin/monokai.nvim'
  -- use 'ofirgall/ofirkai.nvim'
  -- use { "folke/tokyonight.nvim" }
  -- use { "lunarvim/darkplus.nvim" }

  -- cmp plugins
  use { "hrsh7th/nvim-cmp" }     -- The completion plugin
  use { "hrsh7th/cmp-buffer" }   -- buffer completions
  use { "hrsh7th/cmp-path" }     -- path completions
  use { "hrsh7th/cmp-nvim-lsp" }
  use { "hrsh7th/cmp-nvim-lua" } -- complete neovim's Lua rt API: vim.lsp.*. vim.lsp.util.*
  use { "hrsh7th/cmp-nvim-lsp-signature-help" } -- alternative ray-x/lsp_signature.nvim
  use { "saadparwaiz1/cmp_luasnip" } -- snippet completions

  -- snippets
  use { "L3MON4D3/LuaSnip" }  --snippet engine
  use { "rafamadriz/friendly-snippets"}  -- a bunch of snippets to use

  -- LSP
  use { "neovim/nvim-lspconfig" } -- enable LSP
  use { "williamboman/mason.nvim" } -- lsp, dap, linters installer
  use { "williamboman/mason-lspconfig.nvim" }
  use { "jose-elias-alvarez/null-ls.nvim" } -- for formatters and linters
  -- use { "RRethy/vim-illuminate" } -- auto highlighting some words  SWARG: 21-04-23 OFF to figureout is it haning nvim
  -- use { "ray-x/lsp_signature.nvim" } -- alternative  "hrsh7th/cmp-nvim-lsp-signature-help"

  -- markdown-previewer use :MarkdownPreview on any md file to see it in a browser
  -- install manually, fix relative links here: https://github.com/jannis-baum/markdown-preview.nvim
  use { 'iamcco/markdown-preview.nvim',
      --run = function() vim.fn["mkdp#util#install"]() end,
    --Dependencies: nodejs yarn
  }
  -- paste image to markdown cmd: PasteImg
  use 'ekickx/clipboard-image.nvim'

  -- Telescope
  use { "nvim-telescope/telescope.nvim", tag = '0.1.2' } -- nvim 0.8.3

  -- Treesitter
  use {
    "nvim-treesitter/nvim-treesitter", commit = "63260da1", -- before bump to 0.9.1
    run = function() --run = ':TSUpdate'
        local ts_update = require('nvim-treesitter.install').update({ with_sync = true })
        ts_update()
    end,
  }
  -- [NOTE]: SLOWDOWN:
  -- Input lag on cursor movement and edits when a buffer has a lot of injections (like comments) #1267
  -- Java Multiline Commenting Slow Drawing Speed If All Parsers Installed #3582
  -- To Solve Use: :TSUninstall comment
  -- use { "nvim-treesitter/playground" } -- use for get highlighting group under cursor throught :TSHighlightCapturesUnderCursor

  -- Git
  use { "lewis6991/gitsigns.nvim", tag = 'release' }

  -- DAP
  use { "mfussenegger/nvim-dap" }
  use { "rcarriga/nvim-dap-ui", requires = {"mfussenegger/nvim-dap"} }
  use { "ravenxrz/DAPInstall.nvim" } -- DIInstall DIList
  -- use { "theHamsta/nvim-dap-virtual-text" }
  -- use { "nvim-telescope/telescope-dap.nvim" }

  -- Java
  use 'mfussenegger/nvim-jdtls'
  -- TODO dgileadi/vscode-java-decompiler  dgileadi/dg.jdt.ls.decompiler

  -- Rust
  use 'simrat39/rust-tools.nvim'
  -- C#
  -- for work with omnisharp_mono (lsp) for decompile stdlib and extended libs by gd
  -- (GoToDefinition for look sourcecode from compiled C#-code) need to be installed
  -- omnisharp_mono installed through :Mason plugin
  -- Dependency: ~/omnisharp/omnisharp.json - global config file
  -- (for Full Decompiling bodies of methods)
  use 'Hoffs/omnisharp-extended-lsp.nvim' -- Lsp: nvim-lspconfig>mason.nvim
  -- https://github.com/williamboman/mason-lspconfig.nvim/blob/2ca0caadbe9117cb295edda6aa3c03e354f2c3c1/lua/mason-lspconfig/server_configurations/omnisharp/README.md
  -- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#omnisharp

  use {
    'https://gitlab.com/Swarg/nvim-env',
    -- luarocks dependencies for this plugin
    rocks = {
      { 'alogger', version = '0.5'},
      { 'dpring', version = '0.4'},
      { 'cmd4lua', version = '0.8'},
      { 'oop', version = '0.5'},
      { 'clieos', version = '0.6' },
      { 'db-env', version = '0.1'}, -- luasql.postgres luasql.mysql
      { 'ex-parser', version = '0.3'}, -- use lpeg
      { 'www-auth', version = '0.1' },
      { 'lua-cjson', version = '2.1.0'},
      { 'lyaml', version = '6.2'},
      { 'xml2lua', version = '1.5'},
      { 'luasocket'},
      { 'luasec' },
      -- { 'stub', version = '0.10' }, -- testing_dep
    }
  }

  use {
    'https://gitlab.com/Swarg/nvim-dsi',
  }
  use {
    'https://gitlab.com/Swarg/nvim-gpt',
  }

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end)
