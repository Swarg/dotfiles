local cmp_status_ok, hop = pcall(require, "hop")
if not cmp_status_ok then
  return
end

hop.setup {
  keys = 'etovxqpdygfblzhckisuran'
}

local directions = require('hop.hint').HintDirection

local after_cursor = function()
  hop.hint_char1({
    direction = directions.AFTER_CURSOR,
    current_line_only = true,
  })
end
local before_cursor = function()
  hop.hint_char1({
    direction = directions.BEFORE_CURSOR,
    current_line_only = true,
  })
end

local after_cursor2 = function()
  hop.hint_char1({
    direction = directions.AFTER_CURSOR,
    current_line_only = true,
    hint_offset = -1
  })
end
local before_cursor2 = function()
  hop.hint_char1({
    direction = directions.BEFORE_CURSOR,
    current_line_only = true,
    hint_offset = 1,
  })
end

local keymap = vim.keymap.set
-- Hop doesn’t set any keybindings; you will have to define them by yourself.
keymap('', 'f', after_cursor, {remap=true})
keymap('', 'F', before_cursor, {remap=true})
keymap('', 't', after_cursor2, {remap=true})
keymap('', 'T', before_cursor2, {remap=true})
