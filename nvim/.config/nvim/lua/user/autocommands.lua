local U = require 'user.xdebug_util'
local isBigMinifiedFile = U.isBigMinifiedFile

vim.api.nvim_create_autocmd({ "FileType" }, {
  pattern = { "qf", "help", "man", "lspinfo", "spectre_panel" },
  callback = function()
    vim.cmd [[
      nnoremap <silent> <buffer> q :close<CR>
      set nobuflisted
    ]]
  end,
})

vim.api.nvim_create_autocmd({ "FileType" }, {
  pattern = { "gitcommit", "markdown" },
  -- t { buf = 1, event = "FileType", file = "filename.md", match = "markdown" }
  callback = function(t)
    vim.opt_local.wrap = true
    vim.opt_local.spell = true -- :set spell | :set nospell
    if t.match == 'markdown' then
      vim.opt_local.spell = false
      -- local line = (vim.api.nvim_buf_get_lines(t.buf, 1, 1, true)or {})[1]
      -- setlocal spell spelllang=ru
    end
  end,
})

-- Indentation setup
local indentationGroup = vim.api.nvim_create_augroup("IndentationSetup", { clear = true })
-- intends
vim.api.nvim_create_autocmd({ "FileType" }, {
  pattern = { "c", "cpp", "java" },
  command = [[setlocal shiftwidth=4 tabstop=4 softtabstop=4]],
  group = indentationGroup,
})
-- END Indentation

vim.cmd "autocmd BufEnter * ++nested if winnr('$') == 1 && bufname() == 'NvimTree_' . tabpagenr() | quit | endif"

vim.api.nvim_create_autocmd({ "VimResized" }, {
  callback = function()
    vim.cmd "tabdo wincmd ="
  end,
})

vim.api.nvim_create_autocmd({ "CmdWinEnter" }, {
  callback = function()
    vim.cmd "quit"
  end,
})

vim.api.nvim_create_autocmd({ "TextYankPost" }, {
  callback = function()
    vim.highlight.on_yank { higroup = "Visual", timeout = 200 }
  end,
})

vim.api.nvim_create_autocmd({ "BufWritePost" }, {
  pattern = { "*.java" },
  callback = function()
    vim.lsp.codelens.refresh()
  end,
})

--[[vim.api.nvim_create_autocmd({ "VimEnter" }, {
  callback = function()
    vim.cmd "hi link illuminatedWord LspReferenceText"
  end,
})]]

--OFF for Opti 22-04-23
-- vim.api.nvim_create_autocmd({ "BufWinEnter" }, {
--   callback = function()
--     local line_count = vim.api.nvim_buf_line_count(0)
--     if line_count >= 5000 then
--       vim.cmd "IlluminatePauseBuf"
--     end
--   end,
-- })

------
-- Swarg experimental highlight trailing whitespace in eol
--vim.api.nvim_create_autocmd({ "BufWinEnter", "InsertEnter", "InsertLeave" }, {
-- FileType? "InsertLeave"
vim.api.nvim_create_autocmd({ "BufWinEnter", }, {
  callback = function()
    local bt = vim.bo.buftype;
    local ft = vim.bo.filetype;
    if bt == "" and ft == "" then -- fix toggleterm
      return
    end
    -- buftype
    local exclude = { "help", "nofile", "prompt", "quickfix", "terminal",
      "alpha", "aerial", "startify", "dashboard", "packer", "neogitstatus",
      "NvimTree", "neo-tree", "Trouble", }
    -- filetype
    local exclude2 = { "toggleterm", "alpha", "checkhealth", "cmp_menu", "diff",
      "lspinfo", "man", "mason", "TelescopePrompt", "Trouble", "WhichKey",
      "xcanvas_editor",
    }
    --local buftype = vim.api.nvim_get_option_value("buftype", {})
    --local ignore_filetypes = { "gitcommit", "gitrebase", "svn", "hgcommit", "toggleterm" }
    if vim.tbl_contains(exclude, bt) or vim.tbl_contains(exclude2, ft) then
      return
    end
    --vim.cmd 'lua print("bt:[".. vim.bo.buftype .."] ft:[".. vim.bo.filetype .."]")'
    vim.cmd "match NvimInternalError /\\s\\+$/"
  end,
})

vim.api.nvim_create_autocmd({"BufWinLeave" }, {
  callback = function()
    vim.cmd "call clearmatches()"
  end,
})

vim.api.nvim_create_user_command('TrimWhiteSpace', function()
    vim.cmd "%s/\\s\\+$//"
end, { desc = "Trim trailing whitespace", nargs = 0, })

vim.api.nvim_create_user_command('ShowWhiteSpace', function()
    vim.cmd "match NvimInternalError /\\s\\+$/"
end, { desc = "Show trailing whitespace", nargs = 0, })

vim.api.nvim_create_user_command('HideWhiteSpace', function()
    vim.cmd "call clearmatches()"
end, { desc = "Hide trailing whitespace", nargs = 0, })

-- auto remove extra white spaces on save
vim.cmd([[
  function! TrimWhiteSpace()
    %s/\s*$//
    ''
  endfunction
  autocmd FileWritePre * call TrimWhiteSpace()
  autocmd FileAppendPre * call TrimWhiteSpace()
  autocmd FilterWritePre * call TrimWhiteSpace()
  autocmd BufWritePre * call TrimWhiteSpace()
 ]])

-- Highlight trailing whitespace in red while in normal mode
-- vim.cmd('highlight ExtraWhitespace ctermbg=red guibg=#5f0000')
-- vim.cmd([[
--   match ExtraWhitespace /\s\+$/
--   autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
--   autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
--   autocmd InsertLeave * match ExtraWhitespace /\s\+$/
--   autocmd BufWinLeave * call clearmatches()
-- ]])
------

-- add new `jq` filetype into vim.bo.filetype
-- to add support for inserting comments via the Comment.nvim
-- whithout it vim.bo.filetype for *.jq files returns ''
vim.api.nvim_command('au BufRead,BufNewFile *.jq set filetype=jq')

-- cs - C# .csx - dotnet scipt for execution via dotne-script
vim.api.nvim_command('au BufRead,BufNewFile *.csx set filetype=cs')


-- Disable features for big files
local aug = vim.api.nvim_create_augroup("buf_large", { clear = true })


vim.api.nvim_create_autocmd({ "BufReadPre" }, {
  callback = function()
    local path = vim.api.nvim_buf_get_name(vim.api.nvim_get_current_buf())
    local ok, stats = pcall(vim.loop.fs_stat, path)
    local sz = ok == true and stats.size or -1
    if ok and ((sz > 1000000) or isBigMinifiedFile(path, 4096, sz)) then
      vim.b.large_buf = true
      vim.cmd("syntax off")
      --vim.cmd("IlluminatePauseBuf") -- disable vim-illuminate
      vim.cmd("IndentBlanklineDisable") -- disable indent-blankline.nvim
      vim.opt_local.foldmethod = "manual"
      vim.opt_local.spell = false
    else
      vim.b.large_buf = false
    end
  end,
  group = aug,
  pattern = "*",
})

-- to pass password into sudo you need pass-helper:
-- apt install ssh-askpass
-- echo "export SUDO_ASKPASS='/usr/lib/ssh/x11-ssh-askpass'" >> ~/.bashrc
vim.api.nvim_create_user_command('R', function() -- W used in wichkey
    vim.cmd 'w !sudo tee >/dev/null %:p:S'
end, { desc = "Save as root", nargs = 0, })
-- com -bar W exe 'w !sudo tee >/dev/null %:p:S' | setl nomod


-- Lsp for java
vim.api.nvim_create_user_command('Jdtls', function(opts)
  local jdtls_module = require("user.lsp.settings.jdtls")
  jdtls_module.cmd_jdtls((opts or {}).fargs)
end, { desc = "Lsp for current buffer", nargs = '*', })


-- how to auto-format code before save (see :h vim.lsp
-- autocmd BufWritePre *.rs lua vim.lsp.buf.format({ async = false })
