local status_ok, alpha = pcall(require, "alpha")
if not status_ok then
  return
end

--                               

local dashboard = require "alpha.themes.dashboard"
-- [[dashboard.section.header.val = {
dashboard.section.header.val = {
	[[     _/      _/                      _/      _/  _/]],
	[[    _/_/    _/    _/_/      _/_/    _/      _/      _/_/_/  _/_/]],
	[[   _/  _/  _/  _/_/_/_/  _/    _/  _/      _/  _/  _/    _/    _/]],
	[[  _/    _/_/  _/        _/    _/    _/  _/    _/  _/    _/    _/]],
	[[ _/      _/    _/_/_/    _/_/        _/      _/  _/    _/    _/]],
}
local keymaps_file = '~/.config/nvim/lua/user/keymaps.lua'
local jdtls_file = '~/.config/nvim/lua/user/lsp/settings/jdtls.lua'

local btn = dashboard.button

dashboard.section.buttons.val = {
  btn("f", " " .. " Find file", ":Telescope find_files <CR>"),
  btn("e", " " .. " New file", ":ene <BAR> startinsert <CR>"),
  btn("P", " " .. " New Project", ":EnvProject new <CR>"),
  btn("x", " " .. " New XCanvas", ":EnvXC canvas new<CR>"),
  btn("p", " " .. " Find project", ":lua require('telescope').extensions.projects.projects()<CR>"),
  btn("r", " " .. " Recent files", ":Telescope oldfiles <CR>"),
  btn("g", " " .. " Live Grep", ":Telescope live_grep <CR>"),
  btn("c", " " .. " Config", ":e $MYVIMRC <CR>"),
  btn("k", " " .. " KeyMapsConfig", ":e ".. keymaps_file.. "<CR>"),
  btn("J", " " .. " JdtlsConfig", ":e ".. jdtls_file .. "<CR>"),
  btn("b", " " .. " FindKeyBindings", ":Telescope keymaps <CR>"),
  btn("l", " " .. " Open nvim-env logs", ":EnvLog open <CR>"),
  btn("L", " " .. " Open last saved file names", ":EnvFileNamesOpenSaved last<CR>"),
  btn("m", " " .. " Connect to mpv", ":EnvMpv subs open -c<CR>"),
  btn("G", " " .. " Open Chat GPT window", ":GptWnd<CR>"),
  btn("D", " " .. " ALOGGER_DEBUG=1 NVIM_CONF_DEBUG=1 nvim", ""),
  btn("q", " " .. " Quit", ":qa<CR>"),
}
local function footer()
  return ""
end

dashboard.section.footer.val = footer()

dashboard.section.header.opts.hl = "Function"
dashboard.section.buttons.opts.hl = "Character"
dashboard.section.footer.opts.hl = "Type"

dashboard.opts.opts.noautocmd = true
alpha.setup(dashboard.opts)
