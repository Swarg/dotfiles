local ok_env, env = pcall(require, "env")
if not ok_env then
  print('[ERROR] not found plugin nvim-env')
  print(env)
end

env.setup({ logger = { save = true, level = vim.log.levels.INFO } })
