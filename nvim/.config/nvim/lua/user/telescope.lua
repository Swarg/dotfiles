local status_ok, telescope = pcall(require, "telescope")
if not status_ok then
  return
end

local actions = require "telescope.actions"

telescope.setup {
  defaults = {

    prompt_prefix = " ",
    selection_caret = " ",
    path_display = { "smart" },
    file_ignore_patterns = { ".git/", "node_modules" },

    --layout_strategy = 'vertical',
    --layout_config = { height = 0.95 },
    border = true,
    layout_strategy = "horizontal", --center",
    --results_title = false,
    --sorting_strategy = "ascending",
    layout_config = {
      horizontal = {
        width = 0.99,
        height = 0.9,
        preview_cutoff = 100,
      },
    },

    mappings = {
      i = {
        ["<Down>"] = actions.cycle_history_next,
        ["<Up>"]  = actions.cycle_history_prev,
        ["<C-j>"] = actions.move_selection_next,
        ["<C-k>"] = actions.move_selection_previous,
        -- map actions.which_key to <C-h> (default: <C-/>)
        ["<C-h>"] = actions.which_key,
        ["<C-o>"] = actions.select_default,
      },
    },
  },
}
