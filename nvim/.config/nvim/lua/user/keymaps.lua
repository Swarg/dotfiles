-- for lsp look to lua/user/lsp/handlers.lua
--
-- TODO idea how to make hotreloading keybingins
-- vim.api.nvim_get_keymap('n') - get all bindings
-- it has field rhs with '<cmd> .. <cr>' you can remember it to list and
-- Replace/Change on Reload Action
--
-- Shorten function name
local keymap = vim.keymap.set
-- Silent keymap option
local opts = { silent = true }

--Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- Normal --
-- Better window navigation
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-l>", "<C-w>l", opts)

-- signature help is "Ctrl-Alt-K" see user/lsp/handlers.lua:lsp_keymaps

--vim-illuminate
--by default alt+[n|p] - jump by variable name to next|prev
-- External cmds
-- Replace selected text by his translation
keymap("v", "<Leader>`", ":'<,'>!translate1 -i<CR>", opts)

-- Resize with arrows
keymap("n", "<C-Up>", ":resize -2<CR>", opts)
keymap("n", "<C-Down>", ":resize +2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

-- Navigate buffers
keymap("n", "<S-l>", ":bnext<CR>", opts)
keymap("n", "<S-h>", ":bprevious<CR>", opts)

-- Clear highlights
keymap("n", "<leader>hi", "<cmd>nohlsearch<CR>", opts)

-- Swarg's play
-- It will show same that nvim-treesitter/playground by cmd
-- :TSHighlightCapturesUnderCursor
--function my_own_show_synstack()
--  local result = vim.treesitter.get_captures_at_cursor(0)
--  print(vim.inspect(result))
--end
--keymap("n", "<leader>z", "<cmd>lua my_own_show_synstack()<CR>", opts)
-- OFF for opti 22-04-23
-- keymap("n", "<leader>z", ":TSHighlightCapturesUnderCursor<CR>", opts)

--:ColorizerToggle

--lua object tabel to string
function My_own_show_synstack2()
  local groups = " "
  local syn0 = vim.fn.synstack(vim.fn.line("."), vim.fn.col("."))
  for _, id in pairs(syn0) do
    groups = groups .. vim.fn.synIDattr(id, "name") .. " "
  end
  print("HiGr:" .. groups .. " | " .. vim.inspect(syn0))
end

keymap("n", "<leader>1", ":lua My_own_show_synstack2()<CR>", opts)
function My_own_get_bufftype()
  local bt = vim.bo.buftype
  local ft = vim.bo.filetype
  print("BuffType: [" .. bt .. "] FileType: [" .. ft .. "]")
end

keymap("n", "<leader>3", ":lua My_own_get_bufftype()<CR>", opts)

function Set_indent_width(i)
  if type(i) == 'number' then
    vim.cmd("setlocal shiftwidth=" .. i .. " tabstop=" .. i .. " softtabstop=" .. i)
  end
end

keymap("n", "<leader>2", ":lua Set_indent_width(2)<CR>", opts)
keymap("n", "<leader>4", ":lua Set_indent_width(4)<CR>", opts)

-- Close buffers
keymap("n", "<S-q>", "<cmd>Bdelete!<CR>", opts)
--Select One Buffer-"Tab" from all opened (plug:Bufferline)
keymap("n", "<leader>w", ":BufferLinePick<CR>", opts)

-- Copy absolute file name to sys Clipboard
keymap("n", "<leader>m",
  ':let @+=expand("%:p")<CR> :let @*=expand("%:p")<CR>:echo @*<CR>');

-- nvim-colorizer.lua
keymap("n", "<leader>ct", ":ColorizerToggle<CR>", opts)
keymap("n", "<leader>ct", ":ColorizerDetachFromBuffer<CR>", opts)


-- Better paste
keymap("v", "p", '"_dP', opts)

-- Insert --
-- Press jk fast to enter
--keymap("i", "jk", "<ESC>", opts)

-- Visual --
-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- Plugins --

-- NvimTree
keymap("n", "<leader>e", ":NvimTreeToggle<CR>", opts)

-- Hop (EasyMotion-like) -- jump to location in file
keymap("n", "<leader>j", ":HopWord<CR>", opts)
-- hop in current line fF and tT, look to hop.lua

-- Telescope
keymap("n", "<leader>ff", ":Telescope find_files<CR>", opts)
-- keymap("n", "<leader>fg", ":Telescope live_grep<CR>", opts) --ft
-- experimental: run rg(ripgrep) with one thread: slower but less memory consuming
keymap("n", "<leader>fg", '<cmd>lua require("telescope.builtin").live_grep({ additional_args = { "-j1" }})<CR>', opts)
keymap("n", "<leader>gr", ':EnvGrepCurrentWord <CR>', opts)
--keymap("n", "<leader>fg", "<cmd>lua require('telescope.builtin').grep_string{ only_sort_text = true, search = vim.fn.input(\"Grep For >\") }<CR>", opts)
keymap("n", "<leader>fb", ":Telescope buffers<CR>", opts)
keymap("n", "<leader>fp", ":Telescope projects<CR>", opts)
keymap("n", "<leader>fh", ":Telescope help_tags<CR>", opts)

-- builtin.lsp_workspace_symbols look to lsp/handlers.lua
keymap("n", "<leader>fr", ":Telescope lsp_references<CR>", opts) -- <leader>gr
keymap("n", "<leader>fs", ":Telescope lsp_document_symbols<CR>", opts)
keymap("n", "<leader>fw", ":Telescope lsp_workspace_symbols<CR>", opts)
keymap("n", "<leader>fo", ":Telescope lsp_dynamic_workspace_symbols<CR>", opts) --Dynamically Lists LSP for all workspace symbols
keymap("n", "<leader>fc", ":Telescope git_commits<CR>", opts)
keymap("n", "<leader>fk", ":Telescope keymaps<CR>", opts)
-- Scroll inside telescope previed  <C-d> <C-u>

-- Git  via plugin toggleterm
keymap("n", "<leader>gg", "<cmd>lua _LAZYGIT_TOGGLE()<CR>", opts)
-- add specific hunk(piece of code) to index for commit
-- original cli command for this: 'git add -patch <file>' cli-interactive mode
keymap('v', '<leader>hs', '<cmd>Gitsigns stage_hunk<CR>')
keymap('v', '<leader>hr', '<cmd>Gitsigns reset_hunk<CR>')
keymap('n', '<leader>hs', '<cmd>Gitsigns stage_hunk<CR>')
keymap('n', '<leader>hr', '<cmd>Gitsigns reset_hunk<CR>')
keymap('n', '<leader>hS', '<cmd>Gitsigns stage_buffer<CR>')
keymap('n', '<leader>hu', '<cmd>Gitsigns undo_stage_hunk<CR>')
keymap('n', '<leader>hR', '<cmd>Gitsigns reset_buffer<CR>')
keymap('n', ']c',         '<cmd>Gitsigns next_hunk<CR>')
keymap('n', '[c',         '<cmd>Gitsigns prev_hunk<CR>')
keymap('n', "<leader>hp", "<cmd>Gitsigns preview_hunk<CR>")
keymap('n', "<leader>hb", '<cmd>lua require"gitsigns".blame_line{full=true}<CR>')
keymap('n', "<leader>tb", "<cmd>Gitsigns toggle_current_line_blame<CR>")
keymap('n', "<leader>hd", "<cmd>Gitsigns diffthis<CR>")
keymap('n', '<leader>hD', '<cmd>lua require"gitsigns".diffthis("~")<CR>')
keymap('n', '<leader>td', "<cmd>Gitsigns toggle_deleted<CR>")

-- Comment
keymap("n", "<leader>/", "<cmd>lua require('Comment.api').toggle.linewise.current()<CR>", opts)
keymap("x", "<leader>/", '<ESC><CMD>lua require("Comment.api").toggle.linewise(vim.fn.visualmode())<CR>')

-- DAP (Debug Adapter Protocol) based nvim-dap (DAP-client for nvim)
keymap("n", "<leader>db", "<cmd>lua require'dap'.toggle_breakpoint()<cr>", opts)
keymap("n", "<leader>de", "<cmd>EnvBreakPoint edit<cr>", opts)
keymap("n", "<f42>", "<cmd>lua require'dap'.toggle_breakpoint()<cr>", opts) -- Ctrl + F8
keymap("n", "<f5>", "<cmd>lua require'dap'.continue()<cr>", opts)           -- F5 <leader>dc
keymap("n", "<f7>", "<cmd>lua require'dap'.step_into()<cr>", opts)          -- F7 <leader>di
keymap("n", "<f8>", "<cmd>lua require'dap'.step_over()<cr>", opts)          -- f8 <leader>do
keymap("n", "<f31>", "<cmd>lua require'dap'.step_out()<cr>", opts)          -- Ctrl+F7 <leader>dO
keymap("n", "<f17>", "<cmd>lua require'dap'.terminate()<cr>", opts)         -- Shift+F5 <leader>dt
keymap("n", "<leader>du", "<cmd>lua require'dapui'.toggle()<cr>", opts)
keymap("n", "<leader>dk", "<cmd>lua require'dap.ui.widgets'.hover()<cr>", opts)
keymap("n", "<f12>", "<cmd>lua require'dap.ui.widgets'.hover()<cr>", opts)  -- F12 <leader>dk
-- how to edit var value :h dapui *dapui.elements.watches.edit()*
keymap("n", "<leader>dc", "<cmd>lua require'dap'.continue()<cr>", opts)
keymap("n", "<leader>di", "<cmd>lua require'dap'.step_into()<cr>", opts)
keymap("n", "<leader>do", "<cmd>lua require'dap'.step_over()<cr>", opts)
keymap("n", "<leader>dO", "<cmd>lua require'dap'.step_out()<cr>", opts)
keymap("n", "<leader>dr", "<cmd>lua require'dap'.repl.toggle()<cr>", opts)
keymap("n", "<leader>dl", "<cmd>lua require'dap'.run_last()<cr>", opts)
keymap("n", "<leader>dt", "<cmd>lua require'dap'.terminate()<cr>", opts)
-- keymap("n", "<leader>du", "<cmd>lua require'dapui'.toggle()<cr>", opts)
keymap("n", "<F18>", "<cmd>EnvRunMainInFile <cr>", opts)      -- Shift-F6
keymap("n", "<F30>", "<cmd>EnvRunTestFile <cr>", opts)        -- Ctrl-F6
keymap("n", "<F42>", "<cmd>EnvDebugTestFile <cr>", opts)      -- Ctrl-Shift-F6
keymap("n", "<leader>gt", "<cmd>EnvToggleTestSourceJump <cr>", opts)
keymap("n", "<leader>da", "<cmd>EnvAttachToDebugger <cr>", opts)
keymap("n", "<F48>", "<cmd>lua require'env'.research()<cr>", opts) -- Ctrl+Shift+F12
keymap("n", "<F6>",  "<cmd>EnvProject run <cr>", opts)         -- F6
keymap("n", "<F29>", "<cmd>EnvDebugProject <cr>", opts)        -- Ctrl+F5
keymap("n", "<F47>", "<cmd>EnvFileNamesSaveOpened <cr>", opts) -- Ctrl+Shift+F11
keymap("n", "<F45>", "<cmd>EnvFileNamesOpenSaved <cr>", opts)  -- Ctrl+Shift+F9
keymap("n", "<F35>", "<cmd>EnvProject build <cr>", opts)       -- Ctrl+F11
keymap("n", "<F23>", "<cmd>EnvProject build --clean <cr>", opts)-- Shift+F11
keymap("n", "<F10>", "<cmd>EnvProject hotswap <cr>", opts)      -- F10
-- test is here difference in keybinding ++
-- keymap("n", "<f4>", "<cmd>Env1<cr>", opts) -- F4
-- keymap("n", "<f16>", "<cmd>Env2<cr>", opts) -- Shift+F4

-- Execute line as vim command (Picked from cursor pos, already picked to mem)
-- See :EnvLineExec help
keymap("n", "<leader>kr", "<CMD>EnvReload --clear <CR>", opts)
keymap("n", "<leader>kn", "<CMD>EnvNew <CR>", opts)
keymap("n", "<leader>kpr", "<CMD>EnvLsp refresh-project<CR>", opts)
-- for run functions via comments in source file:  -- lua require('sdf').func()
keymap("n", "<leader>kl", "<CMD>EnvLineExec current <CR>", opts)
keymap("n", "<leader>klm", "<CMD>EnvLineExec remember<CR>", opts)
keymap("n", "<leader>kll", "<CMD>EnvLineExec memorized<CR>", opts)
keymap("n", "<leader>klf", "<CMD>EnvLineExec forget <CR>", opts)

keymap("n", "<leader>ke", "<CMD>EnvCallFunc run <CR>", opts)
keymap("n", "<leader>kw", "<CMD>EnvCallFunc verbose <CR>", opts) -- with prints
keymap("n", "<leader>kmm", "<CMD>EnvCallFunc mem <CR>", opts)    -- mem for call
keymap("n", "<leader>kmx", "<CMD>EnvCallFunc clean <CR>", opts)  -- clear memed
keymap("n", "<leader>kmi", "<CMD>EnvCallFunc status <CR>", opts)
keymap("n", "<leader>ka", "<cmd>EnvRunTestFile <cr>", opts)      -- Ctrl-F6

-- Quick jump to some source(file) defined in current line
-- behavior like 'gd' in lua vim.lsp.buf.definition  ('gd'  ./lsp/handlers.lua)
keymap("n", "gs", "<CMD>EnvGotoSource <CR>", opts)
keymap("n", "<leader>ga", "<CMD>EnvGotoAttachedBuf<CR>", opts)
keymap("n", "<leader>p",  "<CMD>EnvHUsePassedIn<CR>", opts)
keymap("n", "gl", "<cmd>lua vim.diagnostic.open_float()<CR>", opts)

-- aka code-actions without any lsp Now it used to quick pase lua snippets
keymap("n", "<leader>i", "<CMD>EnvLineInsert <CR>", opts)
keymap("v", "<leader>i", "<CMD>EnvLineInsert <CR>", opts)
keymap("n", "<leader>ш", "<CMD>EnvLineInsert <CR>", opts)
keymap("v", "<leader>ш", "<CMD>EnvLineInsert <CR>", opts)
keymap("n", "<leader>[", "<CMD>EnvLinesModify<CR>", opts)
keymap("v", "<leader>[", "<CMD>EnvLinesModify<CR>", opts)
keymap("n", "<leader>lb", "<CMD>EnvLine fold <CR>", opts)    -- Note must '<leader>l*'-used for lsp
keymap("n", "<leader>lc", "<CMD>EnvLine concat <CR>", opts)  -- Note must '<leader>l*'-used for lsp

keymap("v", "<leader>z", "<CMD>EnvTranslate selected-text <CR>", opts)
keymap("n", "<leader>kz", "<CMD>EnvTranslate current-line<CR>", opts)
keymap("n", "<leader>ля", "<CMD>EnvTranslate current-line <CR>", opts)
keymap("n", "<leader>ktw", "<CMD>EnvTranslate current-word --offline<CR>", opts)


-- quickfix list See :h quickfix.txt
keymap("n", "<leader>kqo", "<cmd>:copen<cr>", opts)
keymap("n", "<leader>kqx", "<cmd>:cclose<cr>", opts)
keymap("n", "<leader>kqn", "<cmd>:cnext<cr>", opts)
keymap("n", "<leader>kqp", "<cmd>:cprev<cr>", opts)
-- location list (aka quickfix2?)
keymap("n", "<leader>klo", "<cmd>:lopen<cr>", opts)
keymap("n", "<leader>klx", "<cmd>:lclose<cr>", opts)
keymap("n", "<leader>kln", "<cmd>:lnext<cr>", opts)
keymap("n", "<leader>klp", "<cmd>:lprev<cr>", opts)

keymap("n", "<leader>kcv", "<cmd>:EnvXC source-code view <CR>", opts)
keymap("n", "<leader>kcc", "<cmd>:EnvXC source-code code <CR>", opts)


keymap("n", "<leader>kdc", "<CMD>EnvDB connect <CR>", opts)
keymap("n", "<leader>kds", "<CMD>EnvDB send <CR>", opts)
keymap("v", "<leader>kds", "<CMD>EnvDB send <CR>", opts)
keymap("n", "<leader>kdd", "<CMD>EnvDB disconnect <CR>", opts)
keymap("n", "<leader>kdt", "<CMD>EnvDB table show <CR>", opts)
keymap("v", "<leader>kdt", "<CMD>EnvDB table show <CR>", opts)
keymap("n", "<leader>kdf", "<CMD>EnvDB sql format <CR>", opts)
keymap("v", "<leader>kdf", "<CMD>EnvDB sql format <CR>", opts)

-- mpvctl
-- to see keybinding in vtt buffer in nvim use: EnvMpv kb

-- Start debugging the main project   Ctrl + F5
-- Start debugging the current file   Ctrl + Shift + F5
-- Start debugging test for file      Ctrl + Shift + F6
-- Stop debugging session             Shift + F5
-- Continue debugging session	        F5
-- Stop debugging session 	          Shift + F5
-- Toggle breakpoint 	                Ctrl + F8
-- New breakpoint                     Ctrl + Shift + F8
-- New watch                          Ctrl + Shift + F7
-- Go to the Unit test 	              Ctrl + Shift + T      <leader>gt
-- Go to Source 	                    Ctrl + Shift + B      <leader>gs
-- Next usage or compile error 	      Ctrl + .
-- Compile package or file            F9
-- Build the main project             F11
-- Create Unit test                   Ctrl + Shift + U
-- Run Unit test on file              Ctrl + F6
-- Run Unit tests on the project      Alt + F6
-- Run the main project               F6
-- Run the main file                  Shift + F6

-- See Lsp mappings in lua/user/lsp/handlers.lua (For Any Lsp)
-- gD  vim.lsp.buf.declaration(), gd  vim.lsp.buf.definition() ... etc
-- <leader>ls  :SymbolsOutline
--
-- See jdtls test_class and test_nearest_method at lua/user/lsp/settings/jdtls.lua
-- <leader>df  jdtls.test_class(),  <leader>dn  jdtls.test_nearest_method() ...

-- Clipboard Copy Selected Text to system Clipboard
-- set mouse=a
keymap("v", "<LeftRelease>", '"+ygv')
keymap("v", "<LeftRelease>", '"*ygv')  -- TODO in toggleterm move fr VISUAL to TERMINAL

local function escape(str)
  -- These characters must be escaped if they appear in the langmap
  local escape_chars = [[;,."|\]]
  return vim.fn.escape(str, escape_chars)
end

-- Character sets entered with the Shift key pressed
local en_shift = [[~QWERTYUIOP{}ASDFGHJKL:"ZXCVBNM<>]]
local ru_shift = [[ËЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ]]
-- Character sets entered as is
-- Here I do not add ',.' и 'бю', so that later there is no recursive call of the command
local en = [[`qwertyuiop[]asdfghjkl;'zxcvbnm]]
local ru = [[ёйцукенгшщзхъфывапролджэячсмить]]
vim.opt.langmap = vim.fn.join({
                   --  ; - delimiter not to be escaped
  escape(ru_shift) .. ';' .. escape(en_shift),
  escape(ru) .. ';' .. escape(en),
}, ',')
-- function Shell_pwd()
--   -- $ ps -C prog -o pid h
--   -- local s = io.popen("ps -C prog -o pid h");
--   local s = vim.fn.system("ps -C prog -o pid h");
--   local n = tonumber(s);
--   print( n )
-- end
-- keymap("n", "<leader>5", ":lua Shell_pwd() <CR>", opts)
