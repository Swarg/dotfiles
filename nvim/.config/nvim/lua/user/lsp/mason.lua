-- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#clangd

local xdu = require 'user.xdebug_util'

local log_debug = xdu.log_debug

-- List of all installed and used LSP-servers
local servers = {
  "lua_ls", --	"sumneko_lua",
  --jdtls installed manualy to ~/.local/share/jdtsl look to ftpluging/java.lua
  "cssls",
  "html",
  "tsserver",
  "pyright",
  "bashls",
  "jsonls",
  "yamlls",
  "clangd",         -- filetypes: { "c", "cpp", "objc", "objcpp", "cuda", "proto" }
  --"omnisharp", -- C# using MS DotNet 6.0+   .cs
  "omnisharp_mono", -- C# work via Mono -- OpenSource Impl of DotNet  .cs
  -- Dependency: Mono 6.12+ with MSBuild
  --"csharp_ls",
  "kotlin_language_server",
  -- "rust-analyzer",
  --"intelephense" -- php lsp on node.js
  "phpactor" -- php lsp on php       https://github.com/phpactor/phpactor
}

-- things to ask whether to run lsp under a new project being opened or not
local triggers_via_ftplugin = {
  jdtls = 1,  -- jdtls triggers from ftplugin/java.lua, not here!
  lua_ls = 1, -- see ftplugin/lua_.lua
}

-- to disable automatic launch of lua_ls
-- this can be useful, for example, for opening the sources of large projects
-- where all the capabilities of the LSP server are not needed, but response speed
-- and minimal memory consumption are needed
if os.getenv("luals") == "0" or os.getenv("lua_ls") == "0" then
  print('[WARN] Nvim starts to work without lua_ls (No LSP server setup for lua-sources)')
  triggers_via_ftplugin.lua_ls = 1
end

local settings = {
  ui = {
    border = "none",
    icons = {
      package_installed = "◍",
      package_pending = "◍",
      package_uninstalled = "◍",
    },
  },
  log_level = vim.log.levels.INFO,
  max_concurrent_installers = 4,
}

require("mason").setup(settings)
require("mason-lspconfig").setup({
  ensure_installed = servers,
  automatic_installation = true,
})

local lspconfig_status_ok, lspconfig = pcall(require, "lspconfig")
if not lspconfig_status_ok then
  return
end

local handlers_ok, handlers = pcall(require, "user.lsp.handlers")
if not handlers_ok then
  return print('Handlers not found!')
end

local setup_lsp_server = handlers.setup_lsp_server

-- local opts = {}
-- Set up options for each specific lsp-server
for _, server in pairs(servers) do
  server = vim.split(server, "@")[1]    -- pull settings for specific lsp from:

  if triggers_via_ftplugin[server] then -- if server triggers via ftplugin by ext
    log_debug('triggers via ftplugin', server)
    goto continue
  end

  setup_lsp_server(lspconfig, server)

  ::continue::
end


--[[ loop above is eqvivalent to this setup:
require'lspconfig'.sumneko_lua.setup {
    -- ... other configs
    settings = {
        Lua = {
            diagnostics = {
                globals = { 'vim' }
            }
        }
    }
}
settings for each specific server in the servers-list  at
~/.config/nvim/lua/user/lsp/settings/ server-name + .lua

--]]
