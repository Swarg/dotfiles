-- https://github.com/ray-x/lsp_signature.nvim
-- off: look at lsp/init.lua
local null_ls_status_ok, lsp_signature = pcall(require, "lsp_signature")
if not null_ls_status_ok then
  return
end

lsp_signature.setup({
  bind = true, -- This is mandatory, otherwise border config won't get registered.
  doc_lines = 0,
  max_height = 16,
  floating_window = true,
  floating_window_above_cur_line = false,
  floating_window_off_y = -2,
  hint_enable = false,
  hint_prefix = " ", -- remove 🐼  
  hint_scheme = 'LspSignatureHintVirtualText',
  handler_opts = {
    border = "none" --"rounded"
  },
  timer_interval = 500, -- default timer check interval
})

-- NOTE: 02-05-23 now use cmp-nvim-lsp-signature-help after look how it works
-- this plugin send too many requests to lsp -- on each cursor movement.
--
-- https://github.com/hrsh7th/cmp-nvim-lsp-signature-help
-- Setup here look to: lua/user/cmp.lua


