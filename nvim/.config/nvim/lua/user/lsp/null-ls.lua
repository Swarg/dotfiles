local null_ls_status_ok, null_ls = pcall(require, "null-ls")
if not null_ls_status_ok then
  return
end

-- require "user.lsp.null-ls-builtins.clang"

-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/formatting
local formatting = null_ls.builtins.formatting
-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/diagnostics
--local diagnostics = null_ls.builtins.diagnostics

-- https://github.com/prettier-solidity/prettier-plugin-solidity
null_ls.setup {
  debug = false,
  sources = {
    -- https://github.com/jose-elias-alvarez/null-ls.nvim/  .. /lua/null-ls/builtins/code_actions/gitsigns.lua
    -- null_ls.builtins.code_actions.gitsigns, -- trigger encogind conflict

    -- C/C++ CppCheck https://github.com/danmar/cppcheck
    null_ls.builtins.diagnostics.cppcheck.with({
      method = null_ls.methods.DIAGNOSTICS_ON_SAVE,
      to_temp_file = false, -- don't create temp file for each cheking
    }),

    formatting.prettier.with { -- for javascript (typescript)
      extra_filetypes = { "toml" },
      extra_args = { "--no-semi", "--single-quote", "--jsx-single-quote" },
    },
    formatting.black.with { extra_args = { "--fast" } }, -- python
    --formatting.google_java_format,
    --diagnostics.flake8, -- for python
    --diagnostics.gccdiag
    --diagnostics.gcc_ansi_c

    -- formatting.stylua, -- TODO cargo install stylua
    -- now use formatting from sumneko lua
    -- python

    -- my own builtins_clang for generate .c and .h files for given name
    null_ls.builtins.code_actions.clang_new_cfile,-- my own code actions for c-lang
    -- ~/.local/share/nvim/site/pack/packer/start/null-ls.nvim/lua/null-ls/builtins/code_actions/clang_new_cfile.lua

    -- php
    -- To See all rulles and descibsion: use cmd 'php-cs-fixer describe @PSR12'
    -- All rules here: https://github.com/PHP-CS-Fixer/PHP-CS-Fixer/blob/master/doc/list.rst
    null_ls.builtins.formatting.phpcsfixer.with({
      extra_args = {
        '--rules=@PSR12' .. ',@PHP81Migration'
           .. ',binary_operator_spaces'
           .. ',align_multiline_comment'
           .. ',ordered_imports,no_unused_imports'
           .. ',array_indentation'
           .. ',indentation_type'
           .. ',single_blank_line_at_eof' -- @PSR12  ?
          ,
          '--using-cache=no',  -- '--config=.php-cs-fixer.dist.php'
      },
    }),
    --  require_once replaced by include_once
    -- prefer_local = 'vendor/bin', only_local = 'vendor/bin',
    -- condition = function(utils)
    --   return utils.root_has_file('vendor/bin/php-cs-fixer')
    -- end

    -- Another one Code Beautify Formatter for php
    -- null_ls.builtins.formatting.phpcbf.with({ args= { '--standard=./phpcs.xml' } })
    --
    null_ls.builtins.diagnostics.psalm.with({
      method = null_ls.methods.DIAGNOSTICS_ON_SAVE,
      to_temp_file = false, -- don't create temp file for each cheking
    }),
  },
  should_attach = function(bufnr)
    --[[
    local name = vim.api.nvim_buf_get_name(bufnr)
    local dir = vim.fs.dirname(name)
    local filename = vim.fs.basename(name)
    local command = string.format('git -C %s check-ignore %s', dir, filename)
    local output = vim.fn.trim(vim.fn.system(command))
    return not (output == filename)]]
    return not vim.b.large_buf
  end,

  -- to solve 'warning: multiple different client offset_encodings detected for
  -- buffer, this is not supported yet'
  -- occurrs then define code_actions.gitsigns and clangd(lsp)
  -- https://github.com/jose-elias-alvarez/null-ls.nvim/issues/428
  on_init = function(new_client, _)
    new_client.offset_encoding = 'utf-8' --32
  end,
}
