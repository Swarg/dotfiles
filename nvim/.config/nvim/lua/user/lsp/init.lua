local status_ok, _ = pcall(require, "lspconfig")
if not status_ok then
  return
end

require "user.lsp.mason"
require("user.lsp.handlers").setup()
require "user.lsp.null-ls"
require "user.lsp.signature"
require "user.symbols-outline"

-- for manually allow statup lsp via user promt
vim.g.java_asked_project_names = {}
