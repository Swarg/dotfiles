-- this piece of code is used at lua/user/lsp/mason.lua
--
--[[ OmniSharp_mono
https://github.com/OmniSharp/omnisharp-roslyn/
https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#omnisharp
https://github.com/OmniSharp/omnisharp-vscode/issues/5120  (MSBuild for OmniSharp_Mono)

Note: The omnisharp_mono server configuration doesn't exist in lspconfig
but is provided by mason-lspconfig. This is done in order to separate the
.NET and Mono variants, making both easily accessible.
https://github.com/williamboman/mason-lspconfig.nvim/blob/2ca0caadbe9117cb295edda6aa3c03e354f2c3c1/lua/mason-lspconfig/server_configurations/omnisharp/README.md
local pid = vim.fn.getpid()
local omnisharp_bin = vim.fn.stdpath os.getenv("HOME") ..
"/.local/share/nvim/mason/bin/omnisharp-mono"
local omnisharp_bin = vim.fn.stdpath "data" .. "/mason/bin/omnisharp-mono"
lspconfig.omnisharp_mono.setup {
  cmd = { omnisharp_bin, "--languageserver" , "--hostPID", tostring(pid) };
  on_attach = require("user.lsp.handlers").on_attach,
  capabilities = require("user.lsp.handlers").capabilities,
  root_dir = lspconfig.util.root_pattern("*.sln", "*.csproj", ".git")
}
TODO mnisharp_useGlobalMono
 https://github.com/kabouzeid/nvim-lspinstall/issues/127
TODO
 Map the .csx extension to the cs file type
https://rudism.com/coding-csharp-in-neovim/
]]
local omni_ok, omnisharp_extended = pcall(require, 'omnisharp_extended')
if not omni_ok then
  print('Failed to load omnisharp_extended')
  return
end

return {
  --cmd = { "dotnet", "omnisharp_mono" },
  settings = { omnisharp = { useGlobalMono = "always" } }, --?
  -- Enables support for reading code style, naming convention and analyzer
  -- settings from .editorconfig.
  enable_editorconfig_support = true,
  -- If true, MSBuild project system will only load projects for files that
  -- were opened in the editor. This setting is useful for big C# codebases
  -- and allows for faster initialization of code navigation features only
  -- for projects that are relevant to code that is being edited. With this
  -- setting enabled OmniSharp may load fewer projects and may thus display
  -- incomplete reference lists for symbols.
  enable_ms_build_load_projects_on_demand = false,
  -- Enables support for roslyn analyzers, code fixes and rulesets.
  enable_roslyn_analyzers = true, --false,
  -- Specifies whether 'using' directives should be grouped and sorted during
  -- document formatting.
  organize_imports_on_format = false,
  -- Enables support for showing unimported types and unimported extension
  -- methods in completion lists. When committed, the appropriate using
  -- directive will be added at the top of the current file. This option can
  -- have a negative impact on initial completion responsiveness,
  -- particularly for the first few completion sessions after opening a
  -- solution.
  enable_import_completion = false,
  -- Specifies whether to include preview versions of the .NET SDK when
  -- determining which version to use for project loading.
  sdk_include_prereleases = true,
  -- Only run analyzers against open files when 'enableRoslynAnalyzers' is
  -- true
  analyze_open_documents_only = false,

  -- gd (GoToDefinition > Get Decompiled sources of Extended Libs) [checked++]
  handlers = {
    ['textDocument/definition'] = omnisharp_extended.handler,
  },
}
