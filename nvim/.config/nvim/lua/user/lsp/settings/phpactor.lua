-- LSP for php. See: https://github.com/phpactor/phpactor
-- https://phpactor.readthedocs.io/en/master/usage/configuration.html
-- https://phpactor.readthedocs.io/en/master/reference/configuration.html
-- phpactor config:dump
-- ~/.config/phpactor/phpactor.json
-- index stored at: ~/.cache/phpactor/
-- How to dump rpc query from phpactor-lsp See: doc/development/debugging.rst
-- How to improve perfomance:
-- ~/.local/share/nvim/mason/packages/phpactor/doc/tips/performance.rst
return {
-- ~/.config/phpactor/phpactor.json
}
--run-main-in-file,  run-test-file,   goto-test-or-source
