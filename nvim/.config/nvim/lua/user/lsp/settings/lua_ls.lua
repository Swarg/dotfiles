-- this piece of code used at ~/.config/nvim/lua/user/lsp/mason.lua
-- this table will be part of opts that goes to 'opts' and:
--
-- lspconfig['lua_ls'].setup(opts)
--
-- https://github.com/LuaLS/lua-language-server
-- https://github.com/LuaLS/lua-language-server/wiki/
-- https://github.com/LuaLS/lua-language-server/wiki/Configuration-File
-- https://luals.github.io/wiki/configuration/
--
-- old link:
-- https://github.com/sumneko/lua-language-server/wiki/Setting
--
-- current server-name is 'lua_ls', in old versions it was 'sumneko_lua'
--
-- https://github.com/LuaLS/lua-language-server/blob/ce964793afc6251673238c256a25e06502e487b8/locale/en-us/setting.lua
--
-- This is a default options for every opened lua file
-- Determine it as lua-plugins for nvim, but not external stand-alone project
-- To Define project specific configuration put '.luarc.json' - file to your
-- lua-project_root
return {
  settings = {
    Lua = {
      runtime = {
        version = 'LuaJIT',
        -- path = runtime_path,
        --
        -- And here is an example that can be used when using Luarocks:
        -- path = {
        --   '?.lua',
        --   '?/init.lua',
        --   vim.fn.expand'~/.luarocks/share/lua/5.1/?.lua',
        --   vim.fn.expand'~/.luarocks/share/lua/5.1/?/init.lua',
        --   '/usr/share/5.1/?.lua',
        --   '/usr/share/lua/5.1/?/init.lua'
        -- }
      },
      completion = {
        callSnippet = 'Replace',
      },
      -- ["completion.enable"] = false
      diagnostics = {
        enable = true,
        globals = { "vim" }, -- 'use'

        -- https://github.com/LuaLS/lua-language-server/wiki/Settings#diagnosticsworkspaceevent
        workspaceEvent = 'OnSave', -- OnChange, OnSave, None
      },
      workspace = {
        checkThirdParty = false, -- Solve issue 'Do you need to conf.. luv ?'
        -- checkThirdParty = "Disable",
        -- library = vim.api.nvim_get_runtime_file('', true),
        library = {
          [vim.fn.expand "$VIMRUNTIME/lua"] = true,
          [vim.fn.stdpath "config" .. "/lua"] = true,
          ["${3rd}/busted/library"] = true,
          ["${3rd}/luassert/library"] = true,
          -- ['/usr/local/share/lua/5.1/'] = true,
        },
        --maxPreload = 5000,
        --preloadFileSize = 10000,
      },
      telemetry = {
        enable = false,
      },
    },
  },
}
--[[ this usage eqvivalent this code:
  require'lspconfig'.lua_ls.setup {
      -- ... other configs
      settings = {
          Lua = {
              diagnostics = {
                  globals = { 'vim' }
              }
          },
          workspace ..
          telemetry = ..
      }
  }
]]
-- Issue: Do you need to configure your work environment as `luv`?
-- See: https://github.com/LuaLS/lua-language-server/issues/679
-- See: https://github.com/LunarVim/LunarVim/issues/4049
-- .luarc.json - for project scope:
-- {
--     "Lua": {
--         "workspace.checkThirdParty": false
--     }
-- }
