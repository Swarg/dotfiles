-- this piece of code is used at ~/.config/nvim/ftplugin/java.lua
-- all other lsp setups from ~/.config/nvim/lua/user/lsp/mason.lua
-- Here:
--  Define cmd for launch jdtls process
--  Define project root for each opened java file
--  Add jdtls specific keymapping
--  Ask whether to start the jdtls-server for a new project
--  (confirm_start_for_project)

-- Config nvim-jdtls:
-- language server for java + debug app, debug junit tests
-- Dependencies:
--   Environment vars: JDK8 JDK17 JDK21
--   jdtls installed at "~/.local/share/jdtls/"
--   java.debug.plugin (DebugAdapter) at "~/.local/share/java-dap/"
--   vscode-java-test  (DebugJUnitTests) at ~/.local/share/java-dap/vscode-java-test/
--
--   lua/user/dap/init.lua   (dap.configurations.java) for Attach to launched process
--   lua/user/lsp/handlers.lua  on_attach  + commons lsp keymappings
-- Recomended Dependencies:
--   nvim-env (for projects_cache) reduce access to FS for each source file checking
--
-- Formatting Refs
--  - https://github.com/redhat-developer/vscode-java/wiki/Formatter-settings
--  - https://github.com/mfussenegger/nvim-jdtls/issues/203
--

local xdu = require 'user.xdebug_util'
local PM = require 'user.lsp.projects_management'

local M = {}

local log_debug = xdu.log_debug

-- use `ALOG_DEBUG=1 nvim src/Main.java` to enable debug log level
log_debug('jdtls.lua loading...')


local capabilities = vim.lsp.protocol.make_client_capabilities()

local status_cmp_ok, cmp_nvim_lsp = pcall(require, "cmp_nvim_lsp")
if not status_cmp_ok then
  return
end
capabilities.textDocument.completion.completionItem.snippetSupport = false
capabilities = cmp_nvim_lsp.default_capabilities(capabilities)

local status, jdtls = pcall(require, "jdtls")
if not status then
  return
end

local extendedClientCapabilities = jdtls.extendedClientCapabilities
extendedClientCapabilities.resolveAdditionalTextEditsSupport = true


-- from my nvim-env plugin
local eclipse_ok, eclipse = pcall(require, "env.bridges.eclipse")



local java = (os.getenv("JDK21") or os.getenv('JAVA_HOME') or '') .. '/bin/java'

-- Determine OS
local home = os.getenv "HOME"
local WORKSPACE_PATH

if vim.fn.has "mac" == 1 then
  WORKSPACE_PATH = home .. "/workspace/"
  CONFIG = "mac"
elseif vim.fn.has "unix" == 1 then
  WORKSPACE_PATH = home .. "/workspace/"
  CONFIG = "linux"
else
  print "Unsupported system"
end



-- java-debug plugin for jdt.ls
local java_dap_active = true
-- my own directory for store both java-debug-plugin and vscode-java-test
-- this jars are plugins for jdt.ls
-- Goal: don't keep it in jdtls_path/plugins/ for case then need to update jdtls
local java_dap_path = vim.fn.glob(home .. "/.local/share/java-dap/")
local bundles = {}
local plugins_to_bundles = {
  "com.microsoft.java.debug.plugin-*.jar",
  "/vscode-java-test/server/*.jar",
}
for _, p in pairs(plugins_to_bundles) do
  local path = vim.fn.glob(java_dap_path .. p, 1)
  path = vim.split(path, '\n', { trimempty = true })
  vim.list_extend(bundles, path, 1, #path)
end

-- TODO dgileadi/vscode-java-decompiler/server/*.jar
-- See: mfussenegger/dotfiles/ .. ftplugin/java.lua

-- To Check is glob got resolved correctly Loot to the bundles output:
-- :lua print(vim.inspect(vim.lsp.get_active_clients()))

-- Point to the eclipse.jdt.ls installation
-- If install jdtls via Mason path will be ~/.local/share/nvim/mason/packages/jdtls/
-- local jdtls_path = vim.fn.stdpath("data") .. "/mason/packages/jdtls"

local jdtls_path = vim.fn.glob(home .. "/.local/share/jdtls/")

--'/path/to/jdtls_install_location/plugins/org.eclipse.equinox.launcher_VER.jar'
local equinox_launcher_jar = vim.fn.glob(jdtls_path .. '/plugins/' ..
  'org.eclipse.equinox.launcher_*.jar');

-- Actualy jdtls works with gradle defined at config:
-- jdtls/config_linux/config.init: osgi.bundles  org.gradle.toolingapi_*.jar
-- local grandle_ver = "8.1.1" ( jdtls:1.37.0 )
-- to findout current version of jdtls:
-- ./plugins/org.eclipse.jdt.ls.core_1.37.0.202406271335.jar

-- See `:help vim.lsp.start_client` for an overview of the supported `config` options.
-- See https://github.com/mfussenegger/nvim-jdtls

-- local root_markers = {
--   ".git", "Makefile", "mvnw", "gradlew", "pom.xml", "build.gradle"
-- }


---@return table|nil { project_name project_root }
function M.mk_config(project_root, project_name)
  -- Workspace aka eclipse-workspace because it work through eclipse.jdt.ls
  local config = {
    --cmd = { -- Experimental
    --  '~/bin/lsp-tool', 'run', 'jdtls', '-p', project_root, '-mode', 'proxy'
    --},
    -- The command that starts the language server
    -- See: https://github.com/eclipse/eclipse.jdt.ls#running-from-the-command-line
    cmd = {
      java, --'/usr/lib/jvm/java21/bin/java',
      -- '-javaagent:' .. vim.fn.glob(home .. jdtls_path .. '/lombok/lombok*.jar'),
      -- '-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=1044',
      '-Declipse.application=org.eclipse.jdt.ls.core.id1',
      '-Dosgi.bundles.defaultStartLevel=4',
      '-Declipse.product=org.eclipse.jdt.ls.core.product',
      -- '-Djava.import.gradle.version='..grandle_ver,
      '-Dlog.protocol=true',
      '-Dlog.level=ALL',
      '-Xms1G', '-XX:+UseG1GC', '-XX:+UseStringDeduplication',
      '--add-modules=ALL-SYSTEM',
      '--add-opens', 'java.base/java.util=ALL-UNNAMED',
      '--add-opens', 'java.base/java.lang=ALL-UNNAMED',
      '-jar', equinox_launcher_jar,
      '-configuration', jdtls_path .. 'config_' .. CONFIG,
      -- See `data directory configuration` section in the README
      -- workspace_dir -- '/path/to/unique/per/project/workspace/folder'
      -- Workspace aka eclipse-workspace because it work through eclipse.jdt.ls
      '-data', WORKSPACE_PATH .. project_name

    },
    on_attach = require("user.lsp.handlers").on_attach, -- --> setup_on_attach
    capabilities = capabilities,
    root_dir = project_root,
    -- Here you can configure eclipse.jdt.ls specific settings
    -- See https://github.com/eclipse/eclipse.jdt.ls/wiki/Running-the-JAVA-LS-server-from-the-command-line#initialize-request
    -- for a list of options
    settings = {
      java = {
        --[[
        project = {
          referencedLibraries = {
            include =  {
              '/home/swarg/.m2/repository/junit/junit/4.13.2/junit-4.13.2.jar'
            },
            sources = {
              ['/home/swarg/.m2/repository/junit/junit/4.13.2/junit-4.13.2.jar'] =
                '/home/swarg/.m2/repository/junit/junit/4.13.2/junit-4.13.2-sources.jar',
            }
          }
        },
        ]]
        configuration = {
          runtimes = {
            {
              name = 'JavaSE-1.8',
              path = os.getenv("JDK8"), --'/usr/lib/jvm/java8/',
            },
            {
              name = 'JavaSE-17',
              path = os.getenv("JDK17"), -- /usr/lib/jvm/java17
              --default = true, -- directly affects which jvm the project will run on
            },
            {
              name = 'JavaSE-21',
              path = os.getenv("JDK21"), -- /usr/lib/jvm/java21
            }
          }
        },
        -- Way to Define java_home for Gradle. Gradle 7.4.2 cannot work with Java8
        -- Works for Gradle >= 4.7
        -- import = { gradle = { home = '/usr/lib/jvm/java17/', } },

        references = {
          includeDecompiledSources = true,
        },
        signatureHelp = { enabled = true },
        contentProvider = { preferred = "fernflower" },
        codeGeneration = {
          toString = {
            template = "${object.className}{${member.name()}=${member.value}, ${otherMembers}}",
          },
          useBlocks = true,
        },
        -- eclipse = {
        --   downloadSources = true,
        -- },
        -- maven = {
        --   downloadSources = true,
        --   updateSnapshots = true,
        -- },
        format = {
          settings = {
            url = home .."/.config/nvim/fmt-styles/eclipse-java-google-style.xml",
            -- profile = 'GoogleStyle',
            -- customized:
            --  - formatter.lineSplit from 100 to 80
            --  - formatter.join_wrapped_lines from true to false
            --    otherwise the chains of method calls will be glued together,
            --    for example for streams or for a spring config(builder)
            --
          }
        }
      }
    },
    -- Language server `initializationOptions`
    -- You need to extend the `bundles` with paths to jar files
    -- if you want to use additional eclipse.jdt.ls plugins.
    --
    -- See https://github.com/mfussenegger/nvim-jdtls#java-debug-installation
    --
    -- If you don't plan on using the debugger or other eclipse.jdt.ls plugins you can remove this
    init_options = {
      bundles = bundles -- ms java-debug + vscode-java-test
    },
  }
  return config
end

-- Mappings specified only for Jdtls (JAVA LSP)
local jdtls_keymaps = function(bufnr)
  local opts = { silent = true, buffer = bufnr }
  local keymap = vim.keymap.set
  -- LSP mapping for some extras provided by nvim-jdtls
  keymap('n', "<A-o>", "<Cmd>lua require('jdtls').organize_imports()<CR>", opts)
  keymap('n', "<A-c>", "<Cmd>EnvJdtlsGenConstructors <CR>", opts)
  keymap('n', "crv", "<Cmd>lua require('jdtls').extract_variable()<CR>", opts)
  keymap('v', "crv", "<Esc><Cmd>lua require('jdtls').extract_variable()<CR>", opts)
  keymap('n', "crc", "<Cmd>lua require('jdtls').extract_constant()<CR>", opts)
  keymap('v', "crc", "<Esc><Cmd>lua require('jdtls').extract_constant()<CR>", opts)
  keymap('v', "crm", "<Esc><Cmd>lua require('jdtls').extract_method(true)<CR>", opts)
  -- If using nvim-dap
  -- This requires java-debug and vscode-java-test bundles(plugins for jdtls)
  -- keymap('n', "<leader>df", "<Cmd>lua require'jdtls'.test_class()<CR>", opts)
  -- keymap('n', "<leader>dn", "<Cmd>lua require'jdtls'.test_nearest_method()<CR>", opts)
  keymap("n", "<leader>df", function()
    if vim.bo.modified then
      vim.cmd('w')
    end
    jdtls.test_class()
  end, opts)
  keymap("n", "<leader>dn", function()
    if vim.bo.modified then
      vim.cmd('w')
    end
    jdtls.test_nearest_method({
      config_overrides = {
        stepFilters = {
          skipClasses = { "$JDK", "junit.*" },
          skipSynthetics = true
        }
      }
    })
  end, opts)
  keymap("n", "<leader>dl", function()
    if vim.bo.modified then
      vim.cmd('w')
    end
    require("dap").run_last()
  end, opts)
end

-- add keymaps for jdts extras features and setup_dap
M.setup_on_attach = function(bufnr)
  jdtls_keymaps(bufnr)
  -- With `hotcodereplace = 'auto' the debug adapter will try to apply
  -- code changes you make during a debug session immediately.
  -- Remove the option if you do not want that.
  -- You can use the `JdtHotcodeReplace` command to trigger it manually
  if java_dap_active == true then
    jdtls.setup_dap({ hotcodereplace = "auto" })
    -- Add Usefull command like JdtHotcodeReplace JdtShell
    -- Add Usefull command like JdtUpdateHotcode JdtShell
    jdtls.setup.add_commands()
    require("jdtls.dap").setup_dap_main_class_configs() --JdtRefreshDebugConfig
  else
    print('java_dap_active: false')
  end
end


-- This starts a new client & server,
-- or attaches to an existing client & server depending on the `root_dir`.
-- require('jdtls').start_or_attach(config)

-- Swarg: Change autostart jdtls, ask for each project.
-- 'LspStart'

-- ask if user need to run a jdtls for this project
-- start jdtls only after user has given premission for it
---@param opts table?
---@return string? project_name
function M.confirm_start_for_project(opts)
  log_debug("confirm_start_for_project")
  local pname, project_root = PM.confirm_start_or_attach_for_project('jdtls', opts)

  if type(pname) == 'string' and #pname > 0 and project_root then
    log_debug('create configuration for root_dir:', project_root)
    -- the project_name is used to indicate in which workspace directory to
    -- store the jdtls(eclipse) files (like .project .classpath and .settings)
    -- generate config for nvim-jdtls
    local config = M.mk_config(project_root, pname)

    if eclipse_ok then
      eclipse.setup_jre_runtime(config)
    end

    jdtls.start_or_attach(config)

    return pname
  end

  return 'ignored'
end

-- :LspStart for java jdtls dont works with my configuration
-- TODO integrate this config with cmd :LspStart|:LspRestart
-- for possible run lsp for current project
-- then project selected as "not work with lsp"
function M.cmd_jdtls(args)
  if args then
    local cmd = args[1]
    local name = args[2]
    if cmd == "ls" then
      print(vim.inspect(vim.g.java_asked_project_names))
      --
    elseif (cmd == "st" or cmd == "status") and name then
      if vim.g.java_asked_project_names[name] then
        print(name, vim.inspect(vim.g.java_asked_project_names[name]))
      else
        print('not found ' .. name)
      end
      --
    elseif (cmd == "start" or cmd == "run") then
      name = name or M.get_project_root()
      if not name then
        print('Cannot find project root')
        return
      end
      print(name)
      local names = vim.g.java_asked_project_names
      names[name] = 'yes'
      vim.g.java_asked_project_names = names -- otherwise it will not changed!
      M.confirm_start_for_project()
      --
    elseif cmd == "rm" and name then
      if vim.g.java_asked_project_names[name] then
        local names = vim.g.java_asked_project_names
        names[name] = nil
        vim.g.java_asked_project_names = names
        print('removed ' .. name)
      else
        print('not found ' .. name)
      end
    else
      print('ls|status|run|rm')
    end
  end
end

if eclipse_ok then
  eclipse.bind_jdtls(M, WORKSPACE_PATH)
end

return M


-- Refs
-- https://github.com/mfussenegger/dotfiles/blob/master/vim/.config/nvim/ftplugin/java.lua

--[[
vim.ui.select({ 'yes', 'no' }, {
  prompt = 'Yes Start or Attach Lsp?',
  format_item = function(item)
    if item == 'yes' then
      return "Start Or Attach Lsp"
    else
      return "No! Not this time."
    end
  end,
}, function(choice)
   if choice == 'yes' then
      jdtls.start_or_attach(config)
   end
end)
]]
