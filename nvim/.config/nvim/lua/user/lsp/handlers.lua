local M = {}

local status_cmp_ok, cmp_nvim_lsp = pcall(require, "cmp_nvim_lsp")
if not status_cmp_ok then
	return
end

M.capabilities = vim.lsp.protocol.make_client_capabilities()
M.capabilities.textDocument.completion.completionItem.snippetSupport = true
M.capabilities = cmp_nvim_lsp.default_capabilities(M.capabilities)

M.setup = function()
	local signs = {

		{ name = "DiagnosticSignError", text = "" },--             
		{ name = "DiagnosticSignWarn", text = "" },
		{ name = "DiagnosticSignHint", text = "" },
		{ name = "DiagnosticSignInfo", text = "" },
	}

	for _, sign in ipairs(signs) do
		vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.text, numhl = "" })
	end

	local config = {
		virtual_text = false, -- disable virtual text
		signs = {
			active = signs, -- show signs
		},
		update_in_insert = false, --true,
		underline = true,
		severity_sort = true,
		float = {
			focusable = true,
			style = "minimal",
			border = "rounded",
			source = "always",
			header = "",
			prefix = "",
		},
	}

	vim.diagnostic.config(config)

	vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
		border = "rounded",
	})

	vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
		border = "rounded",
	})
end

-- base mappings for all LSP
local function lsp_keymaps(bufnr)
	local opts = { noremap = true, silent = true }
	local keymap = vim.api.nvim_buf_set_keymap
	keymap(bufnr, "n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
	keymap(bufnr, "n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", opts)
	keymap(bufnr, "n", "K", "<cmd>lua vim.lsp.buf.hover()<CR>", opts)
	keymap(bufnr, "n", "gm", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)
	keymap(bufnr, "n", "gr", "<cmd>lua vim.lsp.buf.references()<CR>", opts) -- show in quickfix window
	-- keymap(bufnr, "n", "gl", "<cmd>lua vim.diagnostic.open_float()<CR>", opts)
	keymap(bufnr, "n", "gh", ":ClangdSwitchSourceHeader<CR>", opts)
	keymap(bufnr, "n", "<leader>li", "<cmd>LspInfo<cr>", opts)
	keymap(bufnr, "n", "<leader>lI", "<cmd>Mason<cr>", opts)
	keymap(bufnr, "n", "<leader>ln", "<cmd>NullLsInfo<CR>", opts)

	keymap(bufnr, "n", "<leader>la", "<cmd>lua vim.lsp.buf.code_action()<cr>", opts)
	keymap(bufnr, "v", "<leader>la", "<cmd>lua vim.lsp.buf.code_action()<cr>", opts)
	keymap(bufnr, "x", "<leader>la", "<cmd>lua vim.lsp.buf.code_action()<cr>", opts)
	keymap(bufnr, "n", "<leader>lj", "<cmd>lua vim.diagnostic.goto_next({buffer=0})<cr>", opts)
	keymap(bufnr, "n", "<leader>lk", "<cmd>lua vim.diagnostic.goto_prev({buffer=0})<cr>", opts)
	keymap(bufnr, "n", "<leader>lr", "<cmd>lua vim.lsp.buf.rename()<cr>", opts)
	keymap(bufnr, "n", "<C-A-k>",    "<cmd>lua vim.lsp.buf.signature_help()<CR>", opts) -- <leader>ln
	keymap(bufnr, "n", "<leader>ls", "<cmd>SymbolsOutline<CR>", opts)
	keymap(bufnr, "n", "<leader>lt","<cmd>Telescope lsp_document_symbols<CR>", opts) -- more useful ls -- Symbols-Outline
	keymap(bufnr, "n", "<leader>lq", "<cmd>lua vim.diagnostic.setloclist()<CR>", opts)
  keymap(bufnr, "n", "<leader>lf", "<cmd>lua vim.lsp.buf.format{ async = true }<cr>", opts)
end


M.on_attach = function(client, bufnr)
  -- print ('on_attach ' .. client.name)

  -- nvim-env for update_lsp_settings
  local ok_env, env = pcall(require, 'env')
  if ok_env then env.update_lsp_settings_on_attach(client.name, client) end

  -- Java (base config in ftplugin/java.lua
  if client.name == "jdtls" or client.name == 'jdt.ls' then
		client.server_capabilities.documentFormattingProvider = true
    --vim.lsp.codelens.refresh()
    local status_ok, jdtls_module = pcall(require, "user.lsp.settings.jdtls")
    if status_ok then
      jdtls_module.setup_on_attach(bufnr)
    end
  end

  -- no stylua installed (formatting via null-ls) therefore use formatting in sumneko_lua
	if client.name == "lua_ls" then -- old:sumneko_lua
     client.server_capabilities.documentFormattingProvider = true --false
	end

  -- C#
  if client.name == "omnisharp_mono" then
    client.server_capabilities.documentFormattingProvider = true
    vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')
  end

  -- TypeScript
	if client.name == "tsserver" then
    -- work slowly than formationg via prettier
		client.server_capabilities.documentFormattingProvider = false
    client.server_capabilities.document_range_formatting = false
    -- Example hot to disable null-ls for this server
    -- require("null-ls").disable({"prettier"})
	end

	if client.name == "phpactor" then
    client.server_capabilities.documentFormattingProvider = false
    client.server_capabilities.document_range_formatting = false
	end

	lsp_keymaps(bufnr)
	local status_ok, illuminate = pcall(require, "illuminate")
	if status_ok then
	  illuminate.on_attach(client)
	end
end

--

--
-- setup an lsp server configuration for a specific programming language by
-- the name of this lsp-server
--
---@param srv_name string
function M.setup_lsp_server(lspconfig, srv_name)
  local opts = {
    on_attach = M.on_attach,
    capabilities = M.capabilities,
  }

  -- ~/config/nvim/lua/user/lsp/settings/ + server + .lua
  local require_ok, conf_opts = pcall(require, "user.lsp.settings." .. srv_name)
  if require_ok then
    opts = vim.tbl_deep_extend("force", conf_opts, opts)
  end

  lspconfig[srv_name].setup(opts)

  -- Not Start Server for very big files
  local conf = lspconfig[srv_name]
  local try_add = conf.manager.try_add

  conf.manager.try_add = function(bufnr)
    if not vim.b.large_buf then
      return try_add(bufnr)
    end
  end
end


return M


--[[
local file, err = io.open("/tmp/lsp.log", 'a')
if (file) then
  file:write('Client:' .. client.name .. '\n')
  file:write(debug.traceback())
  file:write('\n-------------------\n')
  file:close()
else
  print("error:", err)
end
]]
