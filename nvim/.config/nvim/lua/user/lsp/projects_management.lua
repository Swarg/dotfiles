-- 15-10-2024 @author Swarg
-- Goal: provide the ability to choose whether to run the lsp server or not

local xdu = require 'user.xdebug_util'
local pcache_ok, projects_cache = pcall(require, "env.cache.projects")

local home = os.getenv "HOME"
-- for jdtls
local LOCAL_MAVEN_REPO = home .. '/.m2/repository/'
-- to skip attempt to open projects from sources
local G_SOURCES = "/d/Dev/src/"
local GRADLE = home .. '/.gradle'


local M = {}


-- per project type(lang|lsp) markers
-- servername -> markers
local MARKERS = {
  common = { ".git", "Makefile" },
  jdtls = { ".git", "Makefile", "mvnw", "gradlew", "pom.xml", "build.gradle" },
  lua_ls = { ".git", "Makefile", "rockspec" },
}


local log_debug = xdu.log_debug
local starts_with = xdu.starts_with
local ensure_dir = xdu.ensure_dir
--
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

function M.has_pcache()
  return pcache_ok
end

function M.set_active_project(root_dir, value)
  -- this is the way that allows to update the field; others will not work
  if value and value ~= true then value = value == 'yes' end -- to boolean

  local java_asked_project_names = vim.g.java_asked_project_names or E

  java_asked_project_names[root_dir] = value
  vim.g.java_asked_project_names = java_asked_project_names
end

function M.is_active_project(root_dir)
  local v = (vim.g.java_asked_project_names or E)[root_dir or false]
  if v == nil or v == '' then -- not asked from user
    return nil
  end
  return v == true or v == 'yes'
end

--
--
---@param project_root string
function M.get_project_name(project_root)
  local project_name --= vim.fn.fnamemodify(project_root, ":p:h:t")
  -- local env_java_ok, env_java = pcall(require, "env.java")
  if pcache_ok then
    -- Check is Current Project the SubProject with Parent
    -- and if so then add the parent name to the full project name
    -- like: parent/child
    -- to place the subproject dir in workspace inside the parent project dir
    project_name = projects_cache.get_full_project_name(project_root)
  else
    project_name = vim.fn.fnamemodify(project_root, ":p:h:t")
  end
  log_debug("get_project_name ret: '%s' dir: %s", project_name, project_root)
  return project_name
end

function M.get_current_bufname()
  return vim.api.nvim_buf_get_name(vim.api.nvim_get_current_buf())
end

--
---@param bufname string
---@param root_markers table
---@return string
function M.get_project_root_from_pcache(bufname, root_markers)
  local project_root
  assert(type(root_markers) == 'table', 'root_markers')

  bufname = bufname or M.get_current_bufname()
  -- print('#1 find_root with cache for '.. tostring(bufname))
  local proto, path = string.match(bufname, "^([^:]+)://(.*)$")
  -- local is_jdt = starts_with(bufname, 'jdt://') -- opened then goto-definition
  log_debug('pcache_ok:%s proto:%s path:%s', pcache_ok, proto, path)

  -- for lsp.settings.jdtls
  if proto == 'jdt' then -- -"jdt://"
    local proj_name = projects_cache.get_project_name_from_jdt(bufname);
    project_root = projects_cache.get_root_by_name(proj_name, true)
    log_debug('proj_name from jdt:"%s" pcache say:%s', proj_name, project_root)
    --
  elseif proto == 'zipfile' then
    local zipfile, innerpath = string.match(bufname, "^zipfile://([^:]+)::(.*)$")
    assert(zipfile, 'zipfile')
    log_debug('zipfile:%s inner path:%s', zipfile, innerpath)
    project_root = projects_cache.find_root(root_markers, zipfile)
    --
  else
    project_root = projects_cache.find_root(root_markers, bufname)
    log_debug('project_root found by pcache:', project_root)
  end

  return project_root
end

---@return string
function M.ask_user_for_project_root(bufname)
  bufname = bufname or M.get_current_bufname()
  local root_dir
  -- simple root finder by path pattern only
  local root0 = string.match(bufname, "(.+)/src/main/.+")
  -- ask the user
  local message = fmt("Project Root[%s]:(buf:%s):", v2s(root0), v2s(bufname))
  vim.ui.input({ prompt = message }, function(input)
    root_dir = input or root0
  end)

  if root_dir == '.' then -- aliase for current dir
    root_dir = os.getenv('PWD')
    log_debug('project_root from CWD:', root_dir)
  end

  if not root_dir or root_dir == "" then
    error("Cannot find project root")
  end

  return root_dir
end

local showed_opened_from = {
  gradle = false,
  maven = false,
  gsources = false,
}

--
-- Get the project root(root_dir) for the currently open file
-- One dedicated LSP server & client will be started per unique root_dir
--
---@return string|nil config
function M.get_project_root(root_markers)
  log_debug("get_project_root")
  local project_root

  local bufname = M.get_current_bufname()

  -- attempt to open file from jar (project dependencies)
  -- so that you can open dependency(libraries) (its sources) in the context
  -- of the current project, for example for debugging (to setup brackpoints)
  if bufname and (starts_with(bufname, LOCAL_MAVEN_REPO) or
        string.find(bufname, ".jar!/", 1, true) ~= nil) then
    if not showed_opened_from.maven then
      print('detected file opening from the local maven repo')
      showed_opened_from.maven = true
    end
    project_root = vim.fn.getcwd() .. '/'
    return project_root
  end

  if bufname and starts_with(bufname, G_SOURCES) then
    if not showed_opened_from.gsources then
      print('detected file opening from the local sources')
      showed_opened_from.gsources= true
    end
    project_root = vim.fn.getcwd() .. '/'
    return project_root
  end

  if bufname and (starts_with(bufname, GRADLE) or starts_with(bufname, "~/.gradle")) then
    if not showed_opened_from.gradle then
      print('detected file opening from the ~/.gradle ')
      showed_opened_from.gradle = true
    end
    project_root = vim.fn.getcwd() .. '/'
    return project_root
  end


  if pcache_ok then
    -- searching via nvim-env plugin
    project_root = M.get_project_root_from_pcache(bufname, root_markers)
  else
    -- deprecated way to search root_dir via nvim-jdtls plugin
    project_root = require("jdtls.setup").find_root(root_markers, bufname)
    log_debug('jdtls.setup.find_root say:', project_root)
  end

  if not project_root or project_root == "" then
    project_root = M.ask_user_for_project_root(bufname)
  end

  return ensure_dir(project_root)
end

--
-- confirm start or attach for files of a project
--
---@param opts table?{force:boolean?, root_markers:table?}
---@return false|string  (project_name)
---@return string? project_root
function M.confirm_start_or_attach_for_project(lspname, opts)
  log_debug("confirm_start_or_attach_for_project lsp:%s opts:%s", lspname, opts)
  opts = opts or E

  local root_markers = opts.root_markers or MARKERS[lspname] or MARKERS.common
  assert(type(root_markers) == 'table', 'root_markers for lsp:' .. v2s(lspname))

  local project_root = M.get_project_root(root_markers)
  assert(project_root, "Has Project Root")

  local active = M.is_active_project(project_root)
  if active == false and opts.force then
    M.set_active_project(project_root, true)
    active = true
  end
  log_debug("project_root:%s active:%s", project_root, active)

  if (active == nil) then
    vim.ui.select({ 'yes', 'no' }, {
      prompt = 'Run LSP for project "' .. project_root .. '" ?',
      format_item = function(item)
        if item == 'yes' then
          return "Yes Start Or Attach Lsp"
        else
          return "No. Not this time."
        end
      end,
    }, function(choice)
      M.set_active_project(project_root, choice)
      active = M.is_active_project(project_root)
    end)
  end

  -- triggers when opening files from the same project for which confirmation
  -- to work already has
  if (M.is_active_project(project_root)) then
    local project_name = M.get_project_name(project_root)
    assert(project_name and project_name ~= '', 'Has Project Name')

    return project_name, project_root -- to start
  end

  return false, nil -- ignored
end

return M
