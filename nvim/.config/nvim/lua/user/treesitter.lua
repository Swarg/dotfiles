local status_ok, treesitter = pcall(require, "nvim-treesitter")
if not status_ok then
  return
end

local status2_ok, configs = pcall(require, "nvim-treesitter.configs")
if not status2_ok then
  return
end

configs.setup {
  ensure_installed = { -- put the language you want in this array
    "lua", "markdown", "markdown_inline", "bash", "python",
    "java", "c", "cpp", "c_sharp", "jq", "go", "rust", "elixir"
  },
  -- ensure_installed = "all", -- one of "all" or a list of languages
  ignore_install = { "" }, -- List of parsers to ignore installing
  sync_install = false, -- install languages synchronously (only applied to `ensure_installed`)

  highlight = {
    enable = true, -- false will disable the whole extension
    --disable = { "css" }, -- list of language that will be disabled
    disable = function()
      return vim.b.large_buf
    end,
  },
  autopairs = {
    enable = true,
  },
  indent = { enable = true, disable = { "python", "css" } },

  context_commentstring = {
    enable = true,
    enable_autocmd = false,
  },
}
