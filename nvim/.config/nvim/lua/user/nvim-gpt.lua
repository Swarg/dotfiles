local ok_gpt, nvim_gpt = pcall(require, "nvim-gpt")
if not ok_gpt then
  print('[ERROR] not found plugin nvim-gpt')
  print(mod)
end

nvim_gpt.setup({
  logger = { save = true, level = vim.log.levels.INFO },
  api_url = 'http://10.3.0.3:1337',
  provider = 'Blackbox',
  model = 'gpt-4o',
  -- maintain conversation context by sending the entire message history
  conversation_ctx = true,
  history_dir = '~/gpt_hist/',
  save_history = true,
})
