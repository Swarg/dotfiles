local status_ok, impatient = pcall(require, "impatient")
if not status_ok then
  return
end

--[[
lewis6991/impatient.nvim
Speed up loading Lua modules in Neovim to improve startup time.

Commands
:LuaCacheClear:
Remove the loaded cache and delete the cache file.
A new cache file will be created the next time you load Neovim.

:LuaCacheLog:
View log of impatient.

:LuaCacheProfile:
View profiling data. To enable, Impatient must be setup with:
lua require'impatient'.enable_profile()
]]

impatient.enable_profile()
