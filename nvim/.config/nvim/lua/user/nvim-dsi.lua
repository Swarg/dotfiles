local ok_dsi, dsi = pcall(require, "nvim-dsi")
if not ok_dsi then
  print('ERROR On load nvim-dsi: ', dsi)
  return
end

dsi.setup({ key = "value" })
