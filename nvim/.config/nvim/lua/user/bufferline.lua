local status_ok, bufferline = pcall(require, "bufferline")
if not status_ok then
  return
end

bufferline.setup {
  options = {
    indicator = {
      icon = '▎',
      style = 'icon',
    },
    close_command           = "Bdelete! %d", -- can be a string | function, see "Mouse actions"
    --right_mouse_command = "Bdelete! %d", -- can be a string | function, see "Mouse actions"
    offsets = { { filetype = "NvimTree", text = "File Explorer", padding = 1 } },
    themable = true,
    separator_style         = "thin", -- | "thick" | "thin" | { 'any', 'any' },
    diagnostics             = "nvim_lsp",
    show_buffer_icons       = false, -- hide filetype icons. for more space for names
    show_buffer_close_icons = true,
    persist_buffer_sort     = false,
    -- numbers = 'ordinal'
    -- max_name_length       = 40,
  },


  highlights = {
    fill = {
      fg = { attribute = "fg", highlight = "#ff0000" },
      bg = { attribute = "bg", highlight = "TabLine" },
    },

    background = {
      fg = { attribute = "fg", highlight = "TabLine" },
      bg = { attribute = "bg", highlight = "TabLine" },
    },

    buffer_visible = {
      fg = { attribute = "fg", highlight = "TabLine" },
      bg = { attribute = "bg", highlight = "TabLine" },
    },
    buffer_selected = {
      fg = { attribute = "fg", highlight = "Normal" },
      bg = { attribute = "bg", highlight = "Normal" },
      bold = true,
      italic = false,
    },
    close_button = {
      fg = { attribute = "fg", highlight = "TabLine" },
      bg = { attribute = "bg", highlight = "TabLine" },
    },
    close_button_visible = {
      fg = { attribute = "fg", highlight = "TabLine" },
      bg = { attribute = "bg", highlight = "TabLine" },
    },

    tab_selected = {
      fg = { attribute = "fg", highlight = "Normal" },
      bg = { attribute = "bg", highlight = "Normal" },
    },

    tab = {
      fg = { attribute = "fg", highlight = "TabLine" },
      bg = { attribute = "bg", highlight = "TabLine" },
    },

    tab_close = {
      -- fg = {attribute='fg',highlight='LspDiagnosticsDefaultError'},
      fg = { attribute = "fg", highlight = "TabLineSel" },
      bg = { attribute = "bg", highlight = "Normal" },
    },

    duplicate_selected = {
      fg = { attribute = "fg", highlight = "TabLineSel" },
      bg = { attribute = "bg", highlight = "TabLineSel" },
      italic = true,
    },

    duplicate_visible = {
      fg = { attribute = "fg", highlight = "TabLine" },
      bg = { attribute = "bg", highlight = "TabLine" },
      italic = true,
    },

    duplicate = {
      fg = { attribute = "fg", highlight = "TabLine" },
      bg = { attribute = "bg", highlight = "TabLine" },
      italic = true,
    },

    modified = {
      fg = { attribute = "fg", highlight = "TabLine" },
      bg = { attribute = "bg", highlight = "TabLine" },
    },

    modified_selected = {
      fg = { attribute = "fg", highlight = "Normal" },
      bg = { attribute = "bg", highlight = "Normal" },
    },

    modified_visible = {
      fg = { attribute = "fg", highlight = "TabLine" },
      bg = { attribute = "bg", highlight = "TabLine" },
    },

    separator = {
      fg = { attribute = "bg", highlight = "TabLine" },
      bg = { attribute = "bg", highlight = "TabLine" },
    },

    separator_selected = {
      fg = { attribute = "bg", highlight = "Normal" },
      bg = { attribute = "bg", highlight = "Normal" },
    },

    indicator_selected = {
      --fg = { attribute = "fg", highlight = "LspDiagnosticsDefaultHint" },
      fg = { attribute = "fg", highlight = "VertSplit" },
      bg = { attribute = "bg", highlight = "Normal" },
    },
    warning_selected = {
      bold = true,
      italic = false,
    },
    error_selected = {
      bold = true,
      italic = false,
    },
    diagnostic_selected = {
      bold = true,
      italic = false,
    },
    error_diagnostic_selected = {
      bold = true,
      italic = false,
    },
    hint_selected = {
      bold = true,
      italic = false,
    },
    hint_diagnostic_selected = {
      bold = true,
      italic = true,
    },
    info_selected = {
      bold = true,
      italic = false,
    },
    info_diagnostic_selected = {
      bold = true,
      italic = false,
    },
    warning_diagnostic_selected = {
      bold = true,
      italic = false,
    },
  },
}
