-- 08-07-2024 @author Swarg
-- Goals:
--  - Utils for self debugging
--
-- Optional dependencies:
--  - https://luarocks.org/modules/swarg/alogger

local M = {}

local pwd = os.getenv('PWD')

-- unix slashes or win
M.path_sep = (string.find(pwd or '/', '/', 1, true) ~= nil) and '/' or '\\'


-- inner logger ( by default is false)
-- use `ALOGGER_DEBUG=1 NVIM_CONF_DEBUG=1 nvim source.ext` to switch to debug mode
local _debug = (0 == 1) or os.getenv('NVIM_CONF_DEBUG') == "1" or
    os.getenv('NVIM_CONFIG_DEBUG') == "1"

local ok_logger, logger = pcall(require, "alogger")

M.log_debug = function(...) end -- stub if alogger not installed

if _debug and ok_logger then
  M.log_debug = logger.debug
end

local log_debug = M.log_debug

---@param str string
---@param start string ends
function M.starts_with(str, start)
  return str ~= nil and start ~= nil and str:sub(1, #start) == start
end

function M.dump2file(fn, s, mode)
  mode = mode or 'ab'
  local file, _ = io.open(fn or "/tmp/nvim_dump.log", mode or 'ab')
  if file then
    file:write(s + "\n")
    file:close()
  end
end

---@param fn string?
function M.is_file_exists(fn)
  if type(fn) == 'string' and fn ~= '' then
    local file, _ = io.open(fn, 'rb')
    if file then
      file:close()
      return true
    end
  end
  return false
end

--
-- add a slash to the end of the path if it is not already there
--
---@param path string
---@return string
function M.ensure_dir(path)
  if path and #path > 1 then
    if path:sub(-1, -1) ~= M.path_sep then
      path = path .. M.path_sep
    end
  end
  return path
end

-- filename without extension
---@param path string
function M.extract_extension(path)
  if path then
    return string.match(path, "^.*%.([^%.\\/]+)$") or ''
  end
  return ''
end

--
local minified_exts = {
  xml = 1, html = 1, htm = 1, css = 1, js = 1, json = 1,
}

--
-- checks if the specified path is any large minified web file
-- (one of html css json js)
--
-- works based on reading the first line and
-- comparing its length with the one that is considered large(big_sz)
--
---@param path string
---@param big_sz number?    line size above which the file is considered large
--                          default is 4096
---@param actual_sz number? known in advance the size of the entire file,
--                          so as not to check again if it is less than the
--                          size of one line(big_sz)
function M.isBigMinifiedFile(path, big_sz, actual_sz)
  big_sz = big_sz or 4096
  local ext = M.extract_extension(path)
  if minified_exts[ext or false] and (actual_sz == nil or actual_sz > big_sz) then
    local first_line_size = 0
    local h = io.open(path, 'rb')
    if h then
      local first_line = h:read("*L")
      first_line_size = #(first_line or '')
      h:close()
    end
    log_debug('file:%s fls:%s big_sz:%s ', path, first_line_size, big_sz)
    return first_line_size > big_sz
  end

  return false
end

return M
