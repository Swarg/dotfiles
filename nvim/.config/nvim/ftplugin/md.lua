-- https://neovim.io/doc/user/spell.html
-- see ~/.config/nvim/lua/user/autocommands.lua
--[[
:setlocal spell spelllang=en_us
:setlocal spell spelllang=ru

Finding suggestions for bad words:

z=
]]


-- in vim: autocmd Bufread *.md  setlocal textwidth=0

--[[
""----------------------------------------------
" folding
"-----------------------------------------------
set foldenable
set foldmethod=indent
set foldminlines=4
set foldnestmax=4
-- activate for md
let g:markdown_folding=1


-- Set spell check to Russian
autocmd FileType markdown setlocal spell spelllang=ru

-- You can enable or disable spell checking using the commands:
:set spell
:set nospell

http://ftp.vim.org/pub/vim/runtime/spell/
]]


