
vim.opt_local.shiftwidth = 2
vim.opt_local.tabstop = 2
vim.opt_local.cmdheight = 2 -- more space in the neovim command line for displaying messages

-- Ask whether to start the jdtls-server for a new project
-- And start the jdtls only then user has given premission for it
local jdtls_module = require("user.lsp.settings.jdtls")
jdtls_module.confirm_start_for_project()

-- for autorun jdtls for each project of opened java-file use:
-- local status, jdtls = pcall(require, "jdtls")
-- if status then
--   jdtls.start_or_attach(jdtls_module.config)
-- end

