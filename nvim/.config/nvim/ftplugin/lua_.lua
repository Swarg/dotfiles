-- lua.lua does not work use lua_ instead
vim.opt_local.shiftwidth = 2
vim.opt_local.tabstop = 2
vim.opt_local.cmdheight = 2

-- Goal: Ask whether to start the lua_ls for a new opened project

local xdu = require 'user.xdebug_util'
local log_debug = xdu.log_debug

local LUA_LS = 'lua_ls'

local PM = require 'user.lsp.projects_management'
log_debug('check start or attach for', LUA_LS)

local pname, project_root = PM.confirm_start_or_attach_for_project(LUA_LS, nil)
log_debug('project-name:', pname, 'root:', project_root)


if pname and project_root then
  local lspconfig_status_ok, lspconfig = pcall(require, "lspconfig")
  if not lspconfig_status_ok then
    print('Cannot setup ' .. LUA_LS .. ': no lspconfig (plugin)')
    return
  end

  local handlers_ok, handlers = pcall(require, "user.lsp.handlers")
  if not handlers_ok then
    print('Cannot setup ' .. LUA_LS .. ': not found user.lsp.handlers(module)')
    return
  end

  log_debug('setup_lsp_server ', LUA_LS)
  handlers.setup_lsp_server(lspconfig, LUA_LS)
end
